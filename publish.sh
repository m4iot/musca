#!/bin/sh

find www/ -type d -exec chmod a+rx {} \;
find www/ -type f -exec chmod a+r {} \;
find www/ -type d -exec chmod g+w {} \;
chmod g+s www
rsync -v -r -e ssh -z --delete -p www/. $MUSCAUSER@fusionforge.int-evry.fr:/musca/htdocs/.
