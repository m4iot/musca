#!/bin/sh

cd sources/musca
mvn clean install
cd ../..
rm -rf www/updatesite/*
cp -r sources/musca/musca.updatesite/target/repository/* www/updatesite/
