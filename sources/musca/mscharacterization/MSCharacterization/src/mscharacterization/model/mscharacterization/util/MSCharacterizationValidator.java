/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization.util;

import java.util.Map;

import mscharacterization.model.mscharacterization.*;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage
 * @generated
 */
public class MSCharacterizationValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final MSCharacterizationValidator INSTANCE = new MSCharacterizationValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "mscharacterization.model.mscharacterization";

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Value Type Is Comparable' of 'Measure'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int MEASURE__VALUE_TYPE_IS_COMPARABLE = 1;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Min Has Required Type' of 'Scale'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SCALE__MIN_HAS_REQUIRED_TYPE = 2;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Max Has Required Type' of 'Scale'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SCALE__MAX_HAS_REQUIRED_TYPE = 3;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 3;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSCharacterizationValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return MSCharacterizationPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case MSCharacterizationPackage.MS_CHARACTERIZATION:
				return validateMSCharacterization((MSCharacterization)value, diagnostics, context);
			case MSCharacterizationPackage.VIEWPOINT:
				return validateViewpoint((Viewpoint)value, diagnostics, context);
			case MSCharacterizationPackage.DIMENSION:
				return validateDimension((Dimension)value, diagnostics, context);
			case MSCharacterizationPackage.MEASURE:
				return validateMeasure((Measure)value, diagnostics, context);
			case MSCharacterizationPackage.IN_SCALE_RULE:
				return validateInScaleRule((InScaleRule)value, diagnostics, context);
			case MSCharacterizationPackage.SCALE_SET:
				return validateScaleSet((ScaleSet)value, diagnostics, context);
			case MSCharacterizationPackage.SCALE:
				return validateScale((Scale)value, diagnostics, context);
			case MSCharacterizationPackage.BASIC_PROBE:
				return validateBasicProbe((BasicProbe)value, diagnostics, context);
			case MSCharacterizationPackage.BASIC_PROBE_INFORMATION_PARAMETER:
				return validateBasicProbeInformationParameter((BasicProbeInformationParameter)value, diagnostics, context);
			case MSCharacterizationPackage.MEASURE_CONVERTER:
				return validateMeasureConverter((MeasureConverter)value, diagnostics, context);
			case MSCharacterizationPackage.MAVEN_ARTIFACT:
				return validateMavenArtifact((MavenArtifact)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMSCharacterization(MSCharacterization msCharacterization, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(msCharacterization, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateViewpoint(Viewpoint viewpoint, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(viewpoint, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDimension(Dimension dimension, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dimension, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMeasure(Measure measure, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(measure, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(measure, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(measure, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(measure, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(measure, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(measure, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(measure, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(measure, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(measure, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasure_valueTypeIsComparable(measure, diagnostics, context);
		return result;
	}

	/**
	 * Validates the valueTypeIsComparable constraint of '<em>Measure</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMeasure_valueTypeIsComparable(Measure measure, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return measure.valueTypeIsComparable(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateInScaleRule(InScaleRule inScaleRule, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(inScaleRule, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateScaleSet(ScaleSet scaleSet, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(scaleSet, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateScale(Scale scale, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(scale, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(scale, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(scale, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(scale, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(scale, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(scale, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(scale, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(scale, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(scale, diagnostics, context);
		if (result || diagnostics != null) result &= validateScale_minHasRequiredType(scale, diagnostics, context);
		if (result || diagnostics != null) result &= validateScale_maxHasRequiredType(scale, diagnostics, context);
		return result;
	}

	/**
	 * Validates the minHasRequiredType constraint of '<em>Scale</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateScale_minHasRequiredType(Scale scale, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return scale.minHasRequiredType(diagnostics, context);
	}

	/**
	 * Validates the maxHasRequiredType constraint of '<em>Scale</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateScale_maxHasRequiredType(Scale scale, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return scale.maxHasRequiredType(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBasicProbe(BasicProbe basicProbe, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(basicProbe, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBasicProbeInformationParameter(BasicProbeInformationParameter basicProbeInformationParameter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(basicProbeInformationParameter, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMeasureConverter(MeasureConverter measureConverter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(measureConverter, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMavenArtifact(MavenArtifact mavenArtifact, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(mavenArtifact, diagnostics, context);
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //MSCharacterizationValidator
