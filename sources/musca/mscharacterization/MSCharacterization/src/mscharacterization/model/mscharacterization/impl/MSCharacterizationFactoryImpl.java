/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization.impl;

import mscharacterization.model.mscharacterization.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MSCharacterizationFactoryImpl extends EFactoryImpl implements MSCharacterizationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MSCharacterizationFactory init() {
		try {
			MSCharacterizationFactory theMSCharacterizationFactory = (MSCharacterizationFactory)EPackage.Registry.INSTANCE.getEFactory(MSCharacterizationPackage.eNS_URI);
			if (theMSCharacterizationFactory != null) {
				return theMSCharacterizationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MSCharacterizationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSCharacterizationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case MSCharacterizationPackage.MS_CHARACTERIZATION: return createMSCharacterization();
			case MSCharacterizationPackage.VIEWPOINT: return createViewpoint();
			case MSCharacterizationPackage.DIMENSION: return createDimension();
			case MSCharacterizationPackage.MEASURE: return createMeasure();
			case MSCharacterizationPackage.IN_SCALE_RULE: return createInScaleRule();
			case MSCharacterizationPackage.SCALE_SET: return createScaleSet();
			case MSCharacterizationPackage.SCALE: return createScale();
			case MSCharacterizationPackage.BASIC_PROBE: return createBasicProbe();
			case MSCharacterizationPackage.BASIC_PROBE_INFORMATION_PARAMETER: return createBasicProbeInformationParameter();
			case MSCharacterizationPackage.MEASURE_CONVERTER: return createMeasureConverter();
			case MSCharacterizationPackage.MAVEN_ARTIFACT: return createMavenArtifact();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSCharacterization createMSCharacterization() {
		MSCharacterizationImpl msCharacterization = new MSCharacterizationImpl();
		return msCharacterization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Viewpoint createViewpoint() {
		ViewpointImpl viewpoint = new ViewpointImpl();
		return viewpoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dimension createDimension() {
		DimensionImpl dimension = new DimensionImpl();
		return dimension;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Measure createMeasure() {
		MeasureImpl measure = new MeasureImpl();
		return measure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InScaleRule createInScaleRule() {
		InScaleRuleImpl inScaleRule = new InScaleRuleImpl();
		return inScaleRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScaleSet createScaleSet() {
		ScaleSetImpl scaleSet = new ScaleSetImpl();
		return scaleSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scale createScale() {
		ScaleImpl scale = new ScaleImpl();
		return scale;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicProbe createBasicProbe() {
		BasicProbeImpl basicProbe = new BasicProbeImpl();
		return basicProbe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicProbeInformationParameter createBasicProbeInformationParameter() {
		BasicProbeInformationParameterImpl basicProbeInformationParameter = new BasicProbeInformationParameterImpl();
		return basicProbeInformationParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MeasureConverter createMeasureConverter() {
		MeasureConverterImpl measureConverter = new MeasureConverterImpl();
		return measureConverter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MavenArtifact createMavenArtifact() {
		MavenArtifactImpl mavenArtifact = new MavenArtifactImpl();
		return mavenArtifact;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSCharacterizationPackage getMSCharacterizationPackage() {
		return (MSCharacterizationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MSCharacterizationPackage getPackage() {
		return MSCharacterizationPackage.eINSTANCE;
	}

} //MSCharacterizationFactoryImpl
