/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dimension</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.Dimension#getName <em>Name</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.Dimension#getTreeDimensionName <em>Tree Dimension Name</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.Dimension#getDimensionIsContainedInViewpoint <em>Dimension Is Contained In Viewpoint</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.Dimension#getDimensionContainsScaleSets <em>Dimension Contains Scale Sets</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.Dimension#isGetScaleMethodHasParameters <em>Get Scale Method Has Parameters</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.Dimension#getDimensionHasBasicProbe <em>Dimension Has Basic Probe</em>}</li>
 * </ul>
 * </p>
 *
 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getDimension()
 * @model
 * @generated
 */
public interface Dimension extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getDimension_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.Dimension#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Tree Dimension Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tree Dimension Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tree Dimension Name</em>' attribute.
	 * @see #setTreeDimensionName(String)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getDimension_TreeDimensionName()
	 * @model
	 * @generated
	 */
	String getTreeDimensionName();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.Dimension#getTreeDimensionName <em>Tree Dimension Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tree Dimension Name</em>' attribute.
	 * @see #getTreeDimensionName()
	 * @generated
	 */
	void setTreeDimensionName(String value);

	/**
	 * Returns the value of the '<em><b>Dimension Is Contained In Viewpoint</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link mscharacterization.model.mscharacterization.Viewpoint#getViewpointContainsDimensions <em>Viewpoint Contains Dimensions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dimension Is Contained In Viewpoint</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dimension Is Contained In Viewpoint</em>' container reference.
	 * @see #setDimensionIsContainedInViewpoint(Viewpoint)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getDimension_DimensionIsContainedInViewpoint()
	 * @see mscharacterization.model.mscharacterization.Viewpoint#getViewpointContainsDimensions
	 * @model opposite="viewpointContainsDimensions" required="true" transient="false"
	 * @generated
	 */
	Viewpoint getDimensionIsContainedInViewpoint();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.Dimension#getDimensionIsContainedInViewpoint <em>Dimension Is Contained In Viewpoint</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dimension Is Contained In Viewpoint</em>' container reference.
	 * @see #getDimensionIsContainedInViewpoint()
	 * @generated
	 */
	void setDimensionIsContainedInViewpoint(Viewpoint value);

	/**
	 * Returns the value of the '<em><b>Dimension Contains Scale Sets</b></em>' containment reference list.
	 * The list contents are of type {@link mscharacterization.model.mscharacterization.ScaleSet}.
	 * It is bidirectional and its opposite is '{@link mscharacterization.model.mscharacterization.ScaleSet#getScaleSetIsContainedInDimension <em>Scale Set Is Contained In Dimension</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dimension Contains Scale Sets</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dimension Contains Scale Sets</em>' containment reference list.
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getDimension_DimensionContainsScaleSets()
	 * @see mscharacterization.model.mscharacterization.ScaleSet#getScaleSetIsContainedInDimension
	 * @model opposite="scaleSetIsContainedInDimension" containment="true"
	 * @generated
	 */
	EList<ScaleSet> getDimensionContainsScaleSets();

	/**
	 * Returns the value of the '<em><b>Get Scale Method Has Parameters</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get Scale Method Has Parameters</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get Scale Method Has Parameters</em>' attribute.
	 * @see #setGetScaleMethodHasParameters(boolean)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getDimension_GetScaleMethodHasParameters()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isGetScaleMethodHasParameters();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.Dimension#isGetScaleMethodHasParameters <em>Get Scale Method Has Parameters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get Scale Method Has Parameters</em>' attribute.
	 * @see #isGetScaleMethodHasParameters()
	 * @generated
	 */
	void setGetScaleMethodHasParameters(boolean value);

	/**
	 * Returns the value of the '<em><b>Dimension Has Basic Probe</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dimension Has Basic Probe</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dimension Has Basic Probe</em>' reference.
	 * @see #setDimensionHasBasicProbe(BasicProbe)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getDimension_DimensionHasBasicProbe()
	 * @model
	 * @generated
	 */
	BasicProbe getDimensionHasBasicProbe();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.Dimension#getDimensionHasBasicProbe <em>Dimension Has Basic Probe</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dimension Has Basic Probe</em>' reference.
	 * @see #getDimensionHasBasicProbe()
	 * @generated
	 */
	void setDimensionHasBasicProbe(BasicProbe value);

} // Dimension
