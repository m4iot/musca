/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization.impl;

import java.util.Collection;

import mscharacterization.model.mscharacterization.BasicProbe;
import mscharacterization.model.mscharacterization.BasicProbeInformationParameter;
import mscharacterization.model.mscharacterization.MSCharacterizationPackage;
import mscharacterization.model.mscharacterization.MavenArtifact;
import mscharacterization.model.mscharacterization.Measure;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Basic Probe</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.BasicProbeImpl#getMavenArtifact <em>Maven Artifact</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.BasicProbeImpl#getFullyQualifiedClassName <em>Fully Qualified Class Name</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.BasicProbeImpl#getInformation <em>Information</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.BasicProbeImpl#getTreeInformation <em>Tree Information</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.BasicProbeImpl#getBasicProbeHasMeasure <em>Basic Probe Has Measure</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.BasicProbeImpl#getParameter <em>Parameter</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.BasicProbeImpl#getConstructorProperties <em>Constructor Properties</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.BasicProbeImpl#getInitialization_PROBE <em>Initialization PROBE</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BasicProbeImpl extends MinimalEObjectImpl.Container implements BasicProbe {
	/**
	 * The cached value of the '{@link #getMavenArtifact() <em>Maven Artifact</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMavenArtifact()
	 * @generated
	 * @ordered
	 */
	protected MavenArtifact mavenArtifact;

	/**
	 * The default value of the '{@link #getFullyQualifiedClassName() <em>Fully Qualified Class Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFullyQualifiedClassName()
	 * @generated
	 * @ordered
	 */
	protected static final String FULLY_QUALIFIED_CLASS_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFullyQualifiedClassName() <em>Fully Qualified Class Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFullyQualifiedClassName()
	 * @generated
	 * @ordered
	 */
	protected String fullyQualifiedClassName = FULLY_QUALIFIED_CLASS_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getInformation() <em>Information</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInformation()
	 * @generated
	 * @ordered
	 */
	protected static final String INFORMATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInformation() <em>Information</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInformation()
	 * @generated
	 * @ordered
	 */
	protected String information = INFORMATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getTreeInformation() <em>Tree Information</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTreeInformation()
	 * @generated
	 * @ordered
	 */
	protected static final String TREE_INFORMATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTreeInformation() <em>Tree Information</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTreeInformation()
	 * @generated
	 * @ordered
	 */
	protected String treeInformation = TREE_INFORMATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBasicProbeHasMeasure() <em>Basic Probe Has Measure</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBasicProbeHasMeasure()
	 * @generated
	 * @ordered
	 */
	protected Measure basicProbeHasMeasure;

	/**
	 * The cached value of the '{@link #getParameter() <em>Parameter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameter()
	 * @generated
	 * @ordered
	 */
	protected BasicProbeInformationParameter parameter;

	/**
	 * The cached value of the '{@link #getConstructorProperties() <em>Constructor Properties</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstructorProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<String> constructorProperties;

	/**
	 * The default value of the '{@link #getInitialization_PROBE() <em>Initialization PROBE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialization_PROBE()
	 * @generated
	 * @ordered
	 */
	protected static final String INITIALIZATION_PROBE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInitialization_PROBE() <em>Initialization PROBE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialization_PROBE()
	 * @generated
	 * @ordered
	 */
	protected String initialization_PROBE = INITIALIZATION_PROBE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BasicProbeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MSCharacterizationPackage.Literals.BASIC_PROBE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MavenArtifact getMavenArtifact() {
		if (mavenArtifact != null && mavenArtifact.eIsProxy()) {
			InternalEObject oldMavenArtifact = (InternalEObject)mavenArtifact;
			mavenArtifact = (MavenArtifact)eResolveProxy(oldMavenArtifact);
			if (mavenArtifact != oldMavenArtifact) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MSCharacterizationPackage.BASIC_PROBE__MAVEN_ARTIFACT, oldMavenArtifact, mavenArtifact));
			}
		}
		return mavenArtifact;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MavenArtifact basicGetMavenArtifact() {
		return mavenArtifact;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMavenArtifact(MavenArtifact newMavenArtifact) {
		MavenArtifact oldMavenArtifact = mavenArtifact;
		mavenArtifact = newMavenArtifact;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.BASIC_PROBE__MAVEN_ARTIFACT, oldMavenArtifact, mavenArtifact));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFullyQualifiedClassName() {
		return fullyQualifiedClassName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFullyQualifiedClassName(String newFullyQualifiedClassName) {
		String oldFullyQualifiedClassName = fullyQualifiedClassName;
		fullyQualifiedClassName = newFullyQualifiedClassName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.BASIC_PROBE__FULLY_QUALIFIED_CLASS_NAME, oldFullyQualifiedClassName, fullyQualifiedClassName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInformation() {
		return information;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInformation(String newInformation) {
		String oldInformation = information;
		information = newInformation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.BASIC_PROBE__INFORMATION, oldInformation, information));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTreeInformation() {
		return treeInformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTreeInformation(String newTreeInformation) {
		String oldTreeInformation = treeInformation;
		treeInformation = newTreeInformation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.BASIC_PROBE__TREE_INFORMATION, oldTreeInformation, treeInformation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Measure getBasicProbeHasMeasure() {
		if (basicProbeHasMeasure != null && basicProbeHasMeasure.eIsProxy()) {
			InternalEObject oldBasicProbeHasMeasure = (InternalEObject)basicProbeHasMeasure;
			basicProbeHasMeasure = (Measure)eResolveProxy(oldBasicProbeHasMeasure);
			if (basicProbeHasMeasure != oldBasicProbeHasMeasure) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MSCharacterizationPackage.BASIC_PROBE__BASIC_PROBE_HAS_MEASURE, oldBasicProbeHasMeasure, basicProbeHasMeasure));
			}
		}
		return basicProbeHasMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Measure basicGetBasicProbeHasMeasure() {
		return basicProbeHasMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBasicProbeHasMeasure(Measure newBasicProbeHasMeasure) {
		Measure oldBasicProbeHasMeasure = basicProbeHasMeasure;
		basicProbeHasMeasure = newBasicProbeHasMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.BASIC_PROBE__BASIC_PROBE_HAS_MEASURE, oldBasicProbeHasMeasure, basicProbeHasMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicProbeInformationParameter getParameter() {
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameter(BasicProbeInformationParameter newParameter, NotificationChain msgs) {
		BasicProbeInformationParameter oldParameter = parameter;
		parameter = newParameter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.BASIC_PROBE__PARAMETER, oldParameter, newParameter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameter(BasicProbeInformationParameter newParameter) {
		if (newParameter != parameter) {
			NotificationChain msgs = null;
			if (parameter != null)
				msgs = ((InternalEObject)parameter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MSCharacterizationPackage.BASIC_PROBE__PARAMETER, null, msgs);
			if (newParameter != null)
				msgs = ((InternalEObject)newParameter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MSCharacterizationPackage.BASIC_PROBE__PARAMETER, null, msgs);
			msgs = basicSetParameter(newParameter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.BASIC_PROBE__PARAMETER, newParameter, newParameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getConstructorProperties() {
		if (constructorProperties == null) {
			constructorProperties = new EDataTypeUniqueEList<String>(String.class, this, MSCharacterizationPackage.BASIC_PROBE__CONSTRUCTOR_PROPERTIES);
		}
		return constructorProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInitialization_PROBE() {
		return initialization_PROBE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialization_PROBE(String newInitialization_PROBE) {
		String oldInitialization_PROBE = initialization_PROBE;
		initialization_PROBE = newInitialization_PROBE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.BASIC_PROBE__INITIALIZATION_PROBE, oldInitialization_PROBE, initialization_PROBE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MSCharacterizationPackage.BASIC_PROBE__PARAMETER:
				return basicSetParameter(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MSCharacterizationPackage.BASIC_PROBE__MAVEN_ARTIFACT:
				if (resolve) return getMavenArtifact();
				return basicGetMavenArtifact();
			case MSCharacterizationPackage.BASIC_PROBE__FULLY_QUALIFIED_CLASS_NAME:
				return getFullyQualifiedClassName();
			case MSCharacterizationPackage.BASIC_PROBE__INFORMATION:
				return getInformation();
			case MSCharacterizationPackage.BASIC_PROBE__TREE_INFORMATION:
				return getTreeInformation();
			case MSCharacterizationPackage.BASIC_PROBE__BASIC_PROBE_HAS_MEASURE:
				if (resolve) return getBasicProbeHasMeasure();
				return basicGetBasicProbeHasMeasure();
			case MSCharacterizationPackage.BASIC_PROBE__PARAMETER:
				return getParameter();
			case MSCharacterizationPackage.BASIC_PROBE__CONSTRUCTOR_PROPERTIES:
				return getConstructorProperties();
			case MSCharacterizationPackage.BASIC_PROBE__INITIALIZATION_PROBE:
				return getInitialization_PROBE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MSCharacterizationPackage.BASIC_PROBE__MAVEN_ARTIFACT:
				setMavenArtifact((MavenArtifact)newValue);
				return;
			case MSCharacterizationPackage.BASIC_PROBE__FULLY_QUALIFIED_CLASS_NAME:
				setFullyQualifiedClassName((String)newValue);
				return;
			case MSCharacterizationPackage.BASIC_PROBE__INFORMATION:
				setInformation((String)newValue);
				return;
			case MSCharacterizationPackage.BASIC_PROBE__TREE_INFORMATION:
				setTreeInformation((String)newValue);
				return;
			case MSCharacterizationPackage.BASIC_PROBE__BASIC_PROBE_HAS_MEASURE:
				setBasicProbeHasMeasure((Measure)newValue);
				return;
			case MSCharacterizationPackage.BASIC_PROBE__PARAMETER:
				setParameter((BasicProbeInformationParameter)newValue);
				return;
			case MSCharacterizationPackage.BASIC_PROBE__CONSTRUCTOR_PROPERTIES:
				getConstructorProperties().clear();
				getConstructorProperties().addAll((Collection<? extends String>)newValue);
				return;
			case MSCharacterizationPackage.BASIC_PROBE__INITIALIZATION_PROBE:
				setInitialization_PROBE((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.BASIC_PROBE__MAVEN_ARTIFACT:
				setMavenArtifact((MavenArtifact)null);
				return;
			case MSCharacterizationPackage.BASIC_PROBE__FULLY_QUALIFIED_CLASS_NAME:
				setFullyQualifiedClassName(FULLY_QUALIFIED_CLASS_NAME_EDEFAULT);
				return;
			case MSCharacterizationPackage.BASIC_PROBE__INFORMATION:
				setInformation(INFORMATION_EDEFAULT);
				return;
			case MSCharacterizationPackage.BASIC_PROBE__TREE_INFORMATION:
				setTreeInformation(TREE_INFORMATION_EDEFAULT);
				return;
			case MSCharacterizationPackage.BASIC_PROBE__BASIC_PROBE_HAS_MEASURE:
				setBasicProbeHasMeasure((Measure)null);
				return;
			case MSCharacterizationPackage.BASIC_PROBE__PARAMETER:
				setParameter((BasicProbeInformationParameter)null);
				return;
			case MSCharacterizationPackage.BASIC_PROBE__CONSTRUCTOR_PROPERTIES:
				getConstructorProperties().clear();
				return;
			case MSCharacterizationPackage.BASIC_PROBE__INITIALIZATION_PROBE:
				setInitialization_PROBE(INITIALIZATION_PROBE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.BASIC_PROBE__MAVEN_ARTIFACT:
				return mavenArtifact != null;
			case MSCharacterizationPackage.BASIC_PROBE__FULLY_QUALIFIED_CLASS_NAME:
				return FULLY_QUALIFIED_CLASS_NAME_EDEFAULT == null ? fullyQualifiedClassName != null : !FULLY_QUALIFIED_CLASS_NAME_EDEFAULT.equals(fullyQualifiedClassName);
			case MSCharacterizationPackage.BASIC_PROBE__INFORMATION:
				return INFORMATION_EDEFAULT == null ? information != null : !INFORMATION_EDEFAULT.equals(information);
			case MSCharacterizationPackage.BASIC_PROBE__TREE_INFORMATION:
				return TREE_INFORMATION_EDEFAULT == null ? treeInformation != null : !TREE_INFORMATION_EDEFAULT.equals(treeInformation);
			case MSCharacterizationPackage.BASIC_PROBE__BASIC_PROBE_HAS_MEASURE:
				return basicProbeHasMeasure != null;
			case MSCharacterizationPackage.BASIC_PROBE__PARAMETER:
				return parameter != null;
			case MSCharacterizationPackage.BASIC_PROBE__CONSTRUCTOR_PROPERTIES:
				return constructorProperties != null && !constructorProperties.isEmpty();
			case MSCharacterizationPackage.BASIC_PROBE__INITIALIZATION_PROBE:
				return INITIALIZATION_PROBE_EDEFAULT == null ? initialization_PROBE != null : !INITIALIZATION_PROBE_EDEFAULT.equals(initialization_PROBE);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (fullyQualifiedClassName: ");
		result.append(fullyQualifiedClassName);
		result.append(", information: ");
		result.append(information);
		result.append(", treeInformation: ");
		result.append(treeInformation);
		result.append(", constructorProperties: ");
		result.append(constructorProperties);
		result.append(", initialization_PROBE: ");
		result.append(initialization_PROBE);
		result.append(')');
		return result.toString();
	}

} //BasicProbeImpl
