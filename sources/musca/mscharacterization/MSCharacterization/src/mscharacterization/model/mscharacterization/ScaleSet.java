/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scale Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.ScaleSet#getScaleSetContainsScales <em>Scale Set Contains Scales</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.ScaleSet#getScaleSetIsContainedInDimension <em>Scale Set Is Contained In Dimension</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.ScaleSet#getScaleSetHasInScaleRule <em>Scale Set Has In Scale Rule</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.ScaleSet#getScaleSetHasMeasure <em>Scale Set Has Measure</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.ScaleSet#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getScaleSet()
 * @model
 * @generated
 */
public interface ScaleSet extends EObject {
	/**
	 * Returns the value of the '<em><b>Scale Set Contains Scales</b></em>' containment reference list.
	 * The list contents are of type {@link mscharacterization.model.mscharacterization.Scale}.
	 * It is bidirectional and its opposite is '{@link mscharacterization.model.mscharacterization.Scale#getScaleIsContainedInScaleSet <em>Scale Is Contained In Scale Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scale Set Contains Scales</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scale Set Contains Scales</em>' containment reference list.
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getScaleSet_ScaleSetContainsScales()
	 * @see mscharacterization.model.mscharacterization.Scale#getScaleIsContainedInScaleSet
	 * @model opposite="scaleIsContainedInScaleSet" containment="true"
	 * @generated
	 */
	EList<Scale> getScaleSetContainsScales();

	/**
	 * Returns the value of the '<em><b>Scale Set Is Contained In Dimension</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link mscharacterization.model.mscharacterization.Dimension#getDimensionContainsScaleSets <em>Dimension Contains Scale Sets</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scale Set Is Contained In Dimension</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scale Set Is Contained In Dimension</em>' container reference.
	 * @see #setScaleSetIsContainedInDimension(Dimension)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getScaleSet_ScaleSetIsContainedInDimension()
	 * @see mscharacterization.model.mscharacterization.Dimension#getDimensionContainsScaleSets
	 * @model opposite="dimensionContainsScaleSets" required="true" transient="false"
	 * @generated
	 */
	Dimension getScaleSetIsContainedInDimension();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.ScaleSet#getScaleSetIsContainedInDimension <em>Scale Set Is Contained In Dimension</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scale Set Is Contained In Dimension</em>' container reference.
	 * @see #getScaleSetIsContainedInDimension()
	 * @generated
	 */
	void setScaleSetIsContainedInDimension(Dimension value);

	/**
	 * Returns the value of the '<em><b>Scale Set Has In Scale Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scale Set Has In Scale Rule</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scale Set Has In Scale Rule</em>' reference.
	 * @see #setScaleSetHasInScaleRule(InScaleRule)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getScaleSet_ScaleSetHasInScaleRule()
	 * @model required="true"
	 * @generated
	 */
	InScaleRule getScaleSetHasInScaleRule();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.ScaleSet#getScaleSetHasInScaleRule <em>Scale Set Has In Scale Rule</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scale Set Has In Scale Rule</em>' reference.
	 * @see #getScaleSetHasInScaleRule()
	 * @generated
	 */
	void setScaleSetHasInScaleRule(InScaleRule value);

	/**
	 * Returns the value of the '<em><b>Scale Set Has Measure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scale Set Has Measure</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scale Set Has Measure</em>' reference.
	 * @see #setScaleSetHasMeasure(Measure)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getScaleSet_ScaleSetHasMeasure()
	 * @model required="true"
	 * @generated
	 */
	Measure getScaleSetHasMeasure();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.ScaleSet#getScaleSetHasMeasure <em>Scale Set Has Measure</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scale Set Has Measure</em>' reference.
	 * @see #getScaleSetHasMeasure()
	 * @generated
	 */
	void setScaleSetHasMeasure(Measure value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getScaleSet_Name()
	 * @model default="" required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getName();

} // ScaleSet
