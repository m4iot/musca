/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scale</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.Scale#getScaleIsContainedInScaleSet <em>Scale Is Contained In Scale Set</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.Scale#getName <em>Name</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.Scale#getMin <em>Min</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.Scale#getMax <em>Max</em>}</li>
 * </ul>
 * </p>
 *
 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getScale()
 * @model
 * @generated
 */
public interface Scale extends EObject {
	/**
	 * Returns the value of the '<em><b>Scale Is Contained In Scale Set</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link mscharacterization.model.mscharacterization.ScaleSet#getScaleSetContainsScales <em>Scale Set Contains Scales</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scale Is Contained In Scale Set</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scale Is Contained In Scale Set</em>' container reference.
	 * @see #setScaleIsContainedInScaleSet(ScaleSet)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getScale_ScaleIsContainedInScaleSet()
	 * @see mscharacterization.model.mscharacterization.ScaleSet#getScaleSetContainsScales
	 * @model opposite="scaleSetContainsScales" required="true" transient="false"
	 * @generated
	 */
	ScaleSet getScaleIsContainedInScaleSet();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.Scale#getScaleIsContainedInScaleSet <em>Scale Is Contained In Scale Set</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scale Is Contained In Scale Set</em>' container reference.
	 * @see #getScaleIsContainedInScaleSet()
	 * @generated
	 */
	void setScaleIsContainedInScaleSet(ScaleSet value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getScale_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.Scale#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min</em>' attribute.
	 * @see #setMin(String)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getScale_Min()
	 * @model
	 * @generated
	 */
	String getMin();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.Scale#getMin <em>Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min</em>' attribute.
	 * @see #getMin()
	 * @generated
	 */
	void setMin(String value);

	/**
	 * Returns the value of the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max</em>' attribute.
	 * @see #setMax(String)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getScale_Max()
	 * @model
	 * @generated
	 */
	String getMax();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.Scale#getMax <em>Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max</em>' attribute.
	 * @see #getMax()
	 * @generated
	 */
	void setMax(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean minHasRequiredType(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean maxHasRequiredType(DiagnosticChain diagnostics, Map<Object, Object> context);

} // Scale
