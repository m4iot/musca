/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Measure</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.Measure#getUnity <em>Unity</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.Measure#getValueType <em>Value Type</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.Measure#getCompareToMethodBody_THISVALUE_OTHERVALUE <em>Compare To Method Body THISVALUE OTHERVALUE</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.Measure#getFromMeasureConverters <em>From Measure Converters</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.Measure#getToMeasureConverters <em>To Measure Converters</em>}</li>
 * </ul>
 * </p>
 *
 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMeasure()
 * @model
 * @generated
 */
public interface Measure extends EObject {
	/**
	 * Returns the value of the '<em><b>Unity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unity</em>' attribute.
	 * @see #setUnity(String)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMeasure_Unity()
	 * @model required="true"
	 * @generated
	 */
	String getUnity();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.Measure#getUnity <em>Unity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unity</em>' attribute.
	 * @see #getUnity()
	 * @generated
	 */
	void setUnity(String value);

	/**
	 * Returns the value of the '<em><b>Value Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Type</em>' attribute.
	 * @see #setValueType(Class)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMeasure_ValueType()
	 * @model
	 * @generated
	 */
	Class<?> getValueType();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.Measure#getValueType <em>Value Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Type</em>' attribute.
	 * @see #getValueType()
	 * @generated
	 */
	void setValueType(Class<?> value);

	/**
	 * Returns the value of the '<em><b>Compare To Method Body THISVALUE OTHERVALUE</b></em>' attribute.
	 * The default value is <code>"THISVALUE.compareTo(OTHERVALUE)"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Compare To Method Body THISVALUE OTHERVALUE</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Compare To Method Body THISVALUE OTHERVALUE</em>' attribute.
	 * @see #setCompareToMethodBody_THISVALUE_OTHERVALUE(String)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMeasure_CompareToMethodBody_THISVALUE_OTHERVALUE()
	 * @model default="THISVALUE.compareTo(OTHERVALUE)"
	 * @generated
	 */
	String getCompareToMethodBody_THISVALUE_OTHERVALUE();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.Measure#getCompareToMethodBody_THISVALUE_OTHERVALUE <em>Compare To Method Body THISVALUE OTHERVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Compare To Method Body THISVALUE OTHERVALUE</em>' attribute.
	 * @see #getCompareToMethodBody_THISVALUE_OTHERVALUE()
	 * @generated
	 */
	void setCompareToMethodBody_THISVALUE_OTHERVALUE(String value);

	/**
	 * Returns the value of the '<em><b>From Measure Converters</b></em>' reference list.
	 * The list contents are of type {@link mscharacterization.model.mscharacterization.MeasureConverter}.
	 * It is bidirectional and its opposite is '{@link mscharacterization.model.mscharacterization.MeasureConverter#getFromMeasure <em>From Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From Measure Converters</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Measure Converters</em>' reference list.
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMeasure_FromMeasureConverters()
	 * @see mscharacterization.model.mscharacterization.MeasureConverter#getFromMeasure
	 * @model opposite="fromMeasure"
	 * @generated
	 */
	EList<MeasureConverter> getFromMeasureConverters();

	/**
	 * Returns the value of the '<em><b>To Measure Converters</b></em>' reference list.
	 * The list contents are of type {@link mscharacterization.model.mscharacterization.MeasureConverter}.
	 * It is bidirectional and its opposite is '{@link mscharacterization.model.mscharacterization.MeasureConverter#getToMeasure <em>To Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Measure Converters</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Measure Converters</em>' reference list.
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMeasure_ToMeasureConverters()
	 * @see mscharacterization.model.mscharacterization.MeasureConverter#getToMeasure
	 * @model opposite="toMeasure"
	 * @generated
	 */
	EList<MeasureConverter> getToMeasureConverters();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean valueTypeIsComparable(DiagnosticChain diagnostics, Map<Object, Object> context);

} // Measure
