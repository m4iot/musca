/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Basic Probe Information Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.BasicProbeInformationParameter#getParameterType <em>Parameter Type</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.BasicProbeInformationParameter#getValueFromProjectionField <em>Value From Projection Field</em>}</li>
 * </ul>
 * </p>
 *
 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getBasicProbeInformationParameter()
 * @model
 * @generated
 */
public interface BasicProbeInformationParameter extends EObject {
	/**
	 * Returns the value of the '<em><b>Parameter Type</b></em>' attribute.
	 * The default value is <code>"java.lang.String"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Type</em>' attribute.
	 * @see #setParameterType(Class)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getBasicProbeInformationParameter_ParameterType()
	 * @model default="java.lang.String" required="true"
	 * @generated
	 */
	Class<?> getParameterType();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.BasicProbeInformationParameter#getParameterType <em>Parameter Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Type</em>' attribute.
	 * @see #getParameterType()
	 * @generated
	 */
	void setParameterType(Class<?> value);

	/**
	 * Returns the value of the '<em><b>Value From Projection Field</b></em>' attribute.
	 * The default value is <code>"id"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value From Projection Field</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value From Projection Field</em>' attribute.
	 * @see #setValueFromProjectionField(String)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getBasicProbeInformationParameter_ValueFromProjectionField()
	 * @model default="id" required="true"
	 * @generated
	 */
	String getValueFromProjectionField();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.BasicProbeInformationParameter#getValueFromProjectionField <em>Value From Projection Field</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value From Projection Field</em>' attribute.
	 * @see #getValueFromProjectionField()
	 * @generated
	 */
	void setValueFromProjectionField(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='if (other == null) {\n\treturn false;\n}\nif (other == this) {\n\treturn true;\n}\nif (!(other instanceof BasicProbeInformationParameter)) {\n\treturn false;\n}\nBasicProbeInformationParameter o = (BasicProbeInformationParameter) other;\nif (o.getValueFromProjectionField() == this.getValueFromProjectionField()\n\t\t&& o.getParameterType() == this.getParameterType()) {\n\treturn true;\n}\nreturn false;'"
	 * @generated
	 */
	boolean equals(Object other);

} // BasicProbeInformationParameter
