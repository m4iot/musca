/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization.impl;

import java.util.Collection;

import mscharacterization.model.mscharacterization.Dimension;
import mscharacterization.model.mscharacterization.MSCharacterizationPackage;
import mscharacterization.model.mscharacterization.Viewpoint;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Viewpoint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.ViewpointImpl#getName <em>Name</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.ViewpointImpl#getViewpointContainsDimensions <em>Viewpoint Contains Dimensions</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.ViewpointImpl#getProjectionProperties <em>Projection Properties</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ViewpointImpl extends MinimalEObjectImpl.Container implements Viewpoint {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getViewpointContainsDimensions() <em>Viewpoint Contains Dimensions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getViewpointContainsDimensions()
	 * @generated
	 * @ordered
	 */
	protected EList<Dimension> viewpointContainsDimensions;

	/**
	 * The cached value of the '{@link #getProjectionProperties() <em>Projection Properties</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProjectionProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<String> projectionProperties;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ViewpointImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MSCharacterizationPackage.Literals.VIEWPOINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.VIEWPOINT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Dimension> getViewpointContainsDimensions() {
		if (viewpointContainsDimensions == null) {
			viewpointContainsDimensions = new EObjectContainmentWithInverseEList<Dimension>(Dimension.class, this, MSCharacterizationPackage.VIEWPOINT__VIEWPOINT_CONTAINS_DIMENSIONS, MSCharacterizationPackage.DIMENSION__DIMENSION_IS_CONTAINED_IN_VIEWPOINT);
		}
		return viewpointContainsDimensions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getProjectionProperties() {
		if (projectionProperties == null) {
			projectionProperties = new EDataTypeUniqueEList<String>(String.class, this, MSCharacterizationPackage.VIEWPOINT__PROJECTION_PROPERTIES);
		}
		return projectionProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MSCharacterizationPackage.VIEWPOINT__VIEWPOINT_CONTAINS_DIMENSIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getViewpointContainsDimensions()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MSCharacterizationPackage.VIEWPOINT__VIEWPOINT_CONTAINS_DIMENSIONS:
				return ((InternalEList<?>)getViewpointContainsDimensions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MSCharacterizationPackage.VIEWPOINT__NAME:
				return getName();
			case MSCharacterizationPackage.VIEWPOINT__VIEWPOINT_CONTAINS_DIMENSIONS:
				return getViewpointContainsDimensions();
			case MSCharacterizationPackage.VIEWPOINT__PROJECTION_PROPERTIES:
				return getProjectionProperties();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MSCharacterizationPackage.VIEWPOINT__NAME:
				setName((String)newValue);
				return;
			case MSCharacterizationPackage.VIEWPOINT__VIEWPOINT_CONTAINS_DIMENSIONS:
				getViewpointContainsDimensions().clear();
				getViewpointContainsDimensions().addAll((Collection<? extends Dimension>)newValue);
				return;
			case MSCharacterizationPackage.VIEWPOINT__PROJECTION_PROPERTIES:
				getProjectionProperties().clear();
				getProjectionProperties().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.VIEWPOINT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case MSCharacterizationPackage.VIEWPOINT__VIEWPOINT_CONTAINS_DIMENSIONS:
				getViewpointContainsDimensions().clear();
				return;
			case MSCharacterizationPackage.VIEWPOINT__PROJECTION_PROPERTIES:
				getProjectionProperties().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.VIEWPOINT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case MSCharacterizationPackage.VIEWPOINT__VIEWPOINT_CONTAINS_DIMENSIONS:
				return viewpointContainsDimensions != null && !viewpointContainsDimensions.isEmpty();
			case MSCharacterizationPackage.VIEWPOINT__PROJECTION_PROPERTIES:
				return projectionProperties != null && !projectionProperties.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", projectionProperties: ");
		result.append(projectionProperties);
		result.append(')');
		return result.toString();
	}

} //ViewpointImpl
