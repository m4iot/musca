/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Viewpoint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.Viewpoint#getName <em>Name</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.Viewpoint#getViewpointContainsDimensions <em>Viewpoint Contains Dimensions</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.Viewpoint#getProjectionProperties <em>Projection Properties</em>}</li>
 * </ul>
 * </p>
 *
 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getViewpoint()
 * @model
 * @generated
 */
public interface Viewpoint extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getViewpoint_Name()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.Viewpoint#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Viewpoint Contains Dimensions</b></em>' containment reference list.
	 * The list contents are of type {@link mscharacterization.model.mscharacterization.Dimension}.
	 * It is bidirectional and its opposite is '{@link mscharacterization.model.mscharacterization.Dimension#getDimensionIsContainedInViewpoint <em>Dimension Is Contained In Viewpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Viewpoint Contains Dimensions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Viewpoint Contains Dimensions</em>' containment reference list.
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getViewpoint_ViewpointContainsDimensions()
	 * @see mscharacterization.model.mscharacterization.Dimension#getDimensionIsContainedInViewpoint
	 * @model opposite="dimensionIsContainedInViewpoint" containment="true"
	 * @generated
	 */
	EList<Dimension> getViewpointContainsDimensions();

	/**
	 * Returns the value of the '<em><b>Projection Properties</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Projection Properties</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Projection Properties</em>' attribute list.
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getViewpoint_ProjectionProperties()
	 * @model
	 * @generated
	 */
	EList<String> getProjectionProperties();

} // Viewpoint
