/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization.impl;

import java.lang.reflect.InvocationTargetException;

import mscharacterization.model.mscharacterization.BasicProbeInformationParameter;
import mscharacterization.model.mscharacterization.MSCharacterizationPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Basic Probe Information Parameter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.BasicProbeInformationParameterImpl#getParameterType <em>Parameter Type</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.BasicProbeInformationParameterImpl#getValueFromProjectionField <em>Value From Projection Field</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BasicProbeInformationParameterImpl extends MinimalEObjectImpl.Container implements BasicProbeInformationParameter {
	/**
	 * The cached value of the '{@link #getParameterType() <em>Parameter Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterType()
	 * @generated
	 * @ordered
	 */
	protected Class<?> parameterType;

	/**
	 * The default value of the '{@link #getValueFromProjectionField() <em>Value From Projection Field</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueFromProjectionField()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_FROM_PROJECTION_FIELD_EDEFAULT = "id";

	/**
	 * The cached value of the '{@link #getValueFromProjectionField() <em>Value From Projection Field</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueFromProjectionField()
	 * @generated
	 * @ordered
	 */
	protected String valueFromProjectionField = VALUE_FROM_PROJECTION_FIELD_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BasicProbeInformationParameterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MSCharacterizationPackage.Literals.BASIC_PROBE_INFORMATION_PARAMETER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Class<?> getParameterType() {
		return parameterType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterType(Class<?> newParameterType) {
		Class<?> oldParameterType = parameterType;
		parameterType = newParameterType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.BASIC_PROBE_INFORMATION_PARAMETER__PARAMETER_TYPE, oldParameterType, parameterType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValueFromProjectionField() {
		return valueFromProjectionField;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueFromProjectionField(String newValueFromProjectionField) {
		String oldValueFromProjectionField = valueFromProjectionField;
		valueFromProjectionField = newValueFromProjectionField;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.BASIC_PROBE_INFORMATION_PARAMETER__VALUE_FROM_PROJECTION_FIELD, oldValueFromProjectionField, valueFromProjectionField));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean equals(final Object other) {
		if (other == null) {
			return false;
		}
		if (other == this) {
			return true;
		}
		if (!(other instanceof BasicProbeInformationParameter)) {
			return false;
		}
		BasicProbeInformationParameter o = (BasicProbeInformationParameter) other;
		if (o.getValueFromProjectionField() == this.getValueFromProjectionField()
				&& o.getParameterType() == this.getParameterType()) {
			return true;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MSCharacterizationPackage.BASIC_PROBE_INFORMATION_PARAMETER__PARAMETER_TYPE:
				return getParameterType();
			case MSCharacterizationPackage.BASIC_PROBE_INFORMATION_PARAMETER__VALUE_FROM_PROJECTION_FIELD:
				return getValueFromProjectionField();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MSCharacterizationPackage.BASIC_PROBE_INFORMATION_PARAMETER__PARAMETER_TYPE:
				setParameterType((Class<?>)newValue);
				return;
			case MSCharacterizationPackage.BASIC_PROBE_INFORMATION_PARAMETER__VALUE_FROM_PROJECTION_FIELD:
				setValueFromProjectionField((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.BASIC_PROBE_INFORMATION_PARAMETER__PARAMETER_TYPE:
				setParameterType((Class<?>)null);
				return;
			case MSCharacterizationPackage.BASIC_PROBE_INFORMATION_PARAMETER__VALUE_FROM_PROJECTION_FIELD:
				setValueFromProjectionField(VALUE_FROM_PROJECTION_FIELD_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.BASIC_PROBE_INFORMATION_PARAMETER__PARAMETER_TYPE:
				return parameterType != null;
			case MSCharacterizationPackage.BASIC_PROBE_INFORMATION_PARAMETER__VALUE_FROM_PROJECTION_FIELD:
				return VALUE_FROM_PROJECTION_FIELD_EDEFAULT == null ? valueFromProjectionField != null : !VALUE_FROM_PROJECTION_FIELD_EDEFAULT.equals(valueFromProjectionField);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case MSCharacterizationPackage.BASIC_PROBE_INFORMATION_PARAMETER___EQUALS__OBJECT:
				return equals(arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (parameterType: ");
		result.append(parameterType);
		result.append(", valueFromProjectionField: ");
		result.append(valueFromProjectionField);
		result.append(')');
		return result.toString();
	}

} //BasicProbeInformationParameterImpl
