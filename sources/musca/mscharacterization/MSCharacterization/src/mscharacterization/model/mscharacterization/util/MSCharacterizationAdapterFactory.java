/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization.util;

import mscharacterization.model.mscharacterization.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage
 * @generated
 */
public class MSCharacterizationAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MSCharacterizationPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSCharacterizationAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = MSCharacterizationPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MSCharacterizationSwitch<Adapter> modelSwitch =
		new MSCharacterizationSwitch<Adapter>() {
			@Override
			public Adapter caseMSCharacterization(MSCharacterization object) {
				return createMSCharacterizationAdapter();
			}
			@Override
			public Adapter caseViewpoint(Viewpoint object) {
				return createViewpointAdapter();
			}
			@Override
			public Adapter caseDimension(Dimension object) {
				return createDimensionAdapter();
			}
			@Override
			public Adapter caseMeasure(Measure object) {
				return createMeasureAdapter();
			}
			@Override
			public Adapter caseInScaleRule(InScaleRule object) {
				return createInScaleRuleAdapter();
			}
			@Override
			public Adapter caseScaleSet(ScaleSet object) {
				return createScaleSetAdapter();
			}
			@Override
			public Adapter caseScale(Scale object) {
				return createScaleAdapter();
			}
			@Override
			public Adapter caseBasicProbe(BasicProbe object) {
				return createBasicProbeAdapter();
			}
			@Override
			public Adapter caseBasicProbeInformationParameter(BasicProbeInformationParameter object) {
				return createBasicProbeInformationParameterAdapter();
			}
			@Override
			public Adapter caseMeasureConverter(MeasureConverter object) {
				return createMeasureConverterAdapter();
			}
			@Override
			public Adapter caseMavenArtifact(MavenArtifact object) {
				return createMavenArtifactAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link mscharacterization.model.mscharacterization.MSCharacterization <em>MS Characterization</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mscharacterization.model.mscharacterization.MSCharacterization
	 * @generated
	 */
	public Adapter createMSCharacterizationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mscharacterization.model.mscharacterization.Viewpoint <em>Viewpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mscharacterization.model.mscharacterization.Viewpoint
	 * @generated
	 */
	public Adapter createViewpointAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mscharacterization.model.mscharacterization.Dimension <em>Dimension</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mscharacterization.model.mscharacterization.Dimension
	 * @generated
	 */
	public Adapter createDimensionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mscharacterization.model.mscharacterization.Measure <em>Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mscharacterization.model.mscharacterization.Measure
	 * @generated
	 */
	public Adapter createMeasureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mscharacterization.model.mscharacterization.InScaleRule <em>In Scale Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mscharacterization.model.mscharacterization.InScaleRule
	 * @generated
	 */
	public Adapter createInScaleRuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mscharacterization.model.mscharacterization.ScaleSet <em>Scale Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mscharacterization.model.mscharacterization.ScaleSet
	 * @generated
	 */
	public Adapter createScaleSetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mscharacterization.model.mscharacterization.Scale <em>Scale</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mscharacterization.model.mscharacterization.Scale
	 * @generated
	 */
	public Adapter createScaleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mscharacterization.model.mscharacterization.BasicProbe <em>Basic Probe</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mscharacterization.model.mscharacterization.BasicProbe
	 * @generated
	 */
	public Adapter createBasicProbeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mscharacterization.model.mscharacterization.BasicProbeInformationParameter <em>Basic Probe Information Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mscharacterization.model.mscharacterization.BasicProbeInformationParameter
	 * @generated
	 */
	public Adapter createBasicProbeInformationParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mscharacterization.model.mscharacterization.MeasureConverter <em>Measure Converter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mscharacterization.model.mscharacterization.MeasureConverter
	 * @generated
	 */
	public Adapter createMeasureConverterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mscharacterization.model.mscharacterization.MavenArtifact <em>Maven Artifact</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mscharacterization.model.mscharacterization.MavenArtifact
	 * @generated
	 */
	public Adapter createMavenArtifactAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //MSCharacterizationAdapterFactory
