/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Measure Converter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.MeasureConverter#getFromMeasure <em>From Measure</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.MeasureConverter#getToMeasure <em>To Measure</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.MeasureConverter#getConversionFunction_VALUE <em>Conversion Function VALUE</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.MeasureConverter#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMeasureConverter()
 * @model
 * @generated
 */
public interface MeasureConverter extends EObject {
	/**
	 * Returns the value of the '<em><b>From Measure</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link mscharacterization.model.mscharacterization.Measure#getFromMeasureConverters <em>From Measure Converters</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From Measure</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Measure</em>' reference.
	 * @see #setFromMeasure(Measure)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMeasureConverter_FromMeasure()
	 * @see mscharacterization.model.mscharacterization.Measure#getFromMeasureConverters
	 * @model opposite="fromMeasureConverters" required="true"
	 * @generated
	 */
	Measure getFromMeasure();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.MeasureConverter#getFromMeasure <em>From Measure</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From Measure</em>' reference.
	 * @see #getFromMeasure()
	 * @generated
	 */
	void setFromMeasure(Measure value);

	/**
	 * Returns the value of the '<em><b>To Measure</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link mscharacterization.model.mscharacterization.Measure#getToMeasureConverters <em>To Measure Converters</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Measure</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Measure</em>' reference.
	 * @see #setToMeasure(Measure)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMeasureConverter_ToMeasure()
	 * @see mscharacterization.model.mscharacterization.Measure#getToMeasureConverters
	 * @model opposite="toMeasureConverters" required="true"
	 * @generated
	 */
	Measure getToMeasure();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.MeasureConverter#getToMeasure <em>To Measure</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To Measure</em>' reference.
	 * @see #getToMeasure()
	 * @generated
	 */
	void setToMeasure(Measure value);

	/**
	 * Returns the value of the '<em><b>Conversion Function VALUE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conversion Function VALUE</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conversion Function VALUE</em>' attribute.
	 * @see #setConversionFunction_VALUE(String)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMeasureConverter_ConversionFunction_VALUE()
	 * @model required="true"
	 * @generated
	 */
	String getConversionFunction_VALUE();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.MeasureConverter#getConversionFunction_VALUE <em>Conversion Function VALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Conversion Function VALUE</em>' attribute.
	 * @see #getConversionFunction_VALUE()
	 * @generated
	 */
	void setConversionFunction_VALUE(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMeasureConverter_Name()
	 * @model default="" required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getName();

} // MeasureConverter
