/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see mscharacterization.model.mscharacterization.MSCharacterizationFactory
 * @model kind="package"
 * @generated
 */
public interface MSCharacterizationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mscharacterization";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www-public.it-sudparis.eu/~rottenbe/mscharacterization/3.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mscharacterization";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MSCharacterizationPackage eINSTANCE = mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl.init();

	/**
	 * The meta object id for the '{@link mscharacterization.model.mscharacterization.impl.MSCharacterizationImpl <em>MS Characterization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationImpl
	 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getMSCharacterization()
	 * @generated
	 */
	int MS_CHARACTERIZATION = 0;

	/**
	 * The feature id for the '<em><b>Contains Measures</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MS_CHARACTERIZATION__CONTAINS_MEASURES = 0;

	/**
	 * The feature id for the '<em><b>Contains Viewpoints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MS_CHARACTERIZATION__CONTAINS_VIEWPOINTS = 1;

	/**
	 * The feature id for the '<em><b>Contains In Scale Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MS_CHARACTERIZATION__CONTAINS_IN_SCALE_RULES = 2;

	/**
	 * The feature id for the '<em><b>Contains Basic Probes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MS_CHARACTERIZATION__CONTAINS_BASIC_PROBES = 3;

	/**
	 * The feature id for the '<em><b>Contains Measure Converters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MS_CHARACTERIZATION__CONTAINS_MEASURE_CONVERTERS = 4;

	/**
	 * The feature id for the '<em><b>Contains Maven Artifacts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MS_CHARACTERIZATION__CONTAINS_MAVEN_ARTIFACTS = 5;

	/**
	 * The feature id for the '<em><b>Generated MS Probes Maven Artifact</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MS_CHARACTERIZATION__GENERATED_MS_PROBES_MAVEN_ARTIFACT = 6;

	/**
	 * The number of structural features of the '<em>MS Characterization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MS_CHARACTERIZATION_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>MS Characterization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MS_CHARACTERIZATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link mscharacterization.model.mscharacterization.impl.ViewpointImpl <em>Viewpoint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mscharacterization.model.mscharacterization.impl.ViewpointImpl
	 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getViewpoint()
	 * @generated
	 */
	int VIEWPOINT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEWPOINT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Viewpoint Contains Dimensions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEWPOINT__VIEWPOINT_CONTAINS_DIMENSIONS = 1;

	/**
	 * The feature id for the '<em><b>Projection Properties</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEWPOINT__PROJECTION_PROPERTIES = 2;

	/**
	 * The number of structural features of the '<em>Viewpoint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEWPOINT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Viewpoint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEWPOINT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link mscharacterization.model.mscharacterization.impl.DimensionImpl <em>Dimension</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mscharacterization.model.mscharacterization.impl.DimensionImpl
	 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getDimension()
	 * @generated
	 */
	int DIMENSION = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Tree Dimension Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSION__TREE_DIMENSION_NAME = 1;

	/**
	 * The feature id for the '<em><b>Dimension Is Contained In Viewpoint</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSION__DIMENSION_IS_CONTAINED_IN_VIEWPOINT = 2;

	/**
	 * The feature id for the '<em><b>Dimension Contains Scale Sets</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSION__DIMENSION_CONTAINS_SCALE_SETS = 3;

	/**
	 * The feature id for the '<em><b>Get Scale Method Has Parameters</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSION__GET_SCALE_METHOD_HAS_PARAMETERS = 4;

	/**
	 * The feature id for the '<em><b>Dimension Has Basic Probe</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSION__DIMENSION_HAS_BASIC_PROBE = 5;

	/**
	 * The number of structural features of the '<em>Dimension</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSION_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Dimension</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link mscharacterization.model.mscharacterization.impl.MeasureImpl <em>Measure</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mscharacterization.model.mscharacterization.impl.MeasureImpl
	 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getMeasure()
	 * @generated
	 */
	int MEASURE = 3;

	/**
	 * The feature id for the '<em><b>Unity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASURE__UNITY = 0;

	/**
	 * The feature id for the '<em><b>Value Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASURE__VALUE_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Compare To Method Body THISVALUE OTHERVALUE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASURE__COMPARE_TO_METHOD_BODY_THISVALUE_OTHERVALUE = 2;

	/**
	 * The feature id for the '<em><b>From Measure Converters</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASURE__FROM_MEASURE_CONVERTERS = 3;

	/**
	 * The feature id for the '<em><b>To Measure Converters</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASURE__TO_MEASURE_CONVERTERS = 4;

	/**
	 * The number of structural features of the '<em>Measure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASURE_FEATURE_COUNT = 5;

	/**
	 * The operation id for the '<em>Value Type Is Comparable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASURE___VALUE_TYPE_IS_COMPARABLE__DIAGNOSTICCHAIN_MAP = 0;

	/**
	 * The number of operations of the '<em>Measure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASURE_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link mscharacterization.model.mscharacterization.impl.InScaleRuleImpl <em>In Scale Rule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mscharacterization.model.mscharacterization.impl.InScaleRuleImpl
	 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getInScaleRule()
	 * @generated
	 */
	int IN_SCALE_RULE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_SCALE_RULE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Is In Scale Condition VALUE SCALE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_SCALE_RULE__IS_IN_SCALE_CONDITION_VALUE_SCALE = 1;

	/**
	 * The number of structural features of the '<em>In Scale Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_SCALE_RULE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>In Scale Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_SCALE_RULE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link mscharacterization.model.mscharacterization.impl.ScaleSetImpl <em>Scale Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mscharacterization.model.mscharacterization.impl.ScaleSetImpl
	 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getScaleSet()
	 * @generated
	 */
	int SCALE_SET = 5;

	/**
	 * The feature id for the '<em><b>Scale Set Contains Scales</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE_SET__SCALE_SET_CONTAINS_SCALES = 0;

	/**
	 * The feature id for the '<em><b>Scale Set Is Contained In Dimension</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE_SET__SCALE_SET_IS_CONTAINED_IN_DIMENSION = 1;

	/**
	 * The feature id for the '<em><b>Scale Set Has In Scale Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE_SET__SCALE_SET_HAS_IN_SCALE_RULE = 2;

	/**
	 * The feature id for the '<em><b>Scale Set Has Measure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE_SET__SCALE_SET_HAS_MEASURE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE_SET__NAME = 4;

	/**
	 * The number of structural features of the '<em>Scale Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE_SET_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Scale Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE_SET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link mscharacterization.model.mscharacterization.impl.ScaleImpl <em>Scale</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mscharacterization.model.mscharacterization.impl.ScaleImpl
	 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getScale()
	 * @generated
	 */
	int SCALE = 6;

	/**
	 * The feature id for the '<em><b>Scale Is Contained In Scale Set</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE__SCALE_IS_CONTAINED_IN_SCALE_SET = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE__MIN = 2;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE__MAX = 3;

	/**
	 * The number of structural features of the '<em>Scale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE_FEATURE_COUNT = 4;

	/**
	 * The operation id for the '<em>Min Has Required Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE___MIN_HAS_REQUIRED_TYPE__DIAGNOSTICCHAIN_MAP = 0;

	/**
	 * The operation id for the '<em>Max Has Required Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE___MAX_HAS_REQUIRED_TYPE__DIAGNOSTICCHAIN_MAP = 1;

	/**
	 * The number of operations of the '<em>Scale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link mscharacterization.model.mscharacterization.impl.BasicProbeImpl <em>Basic Probe</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mscharacterization.model.mscharacterization.impl.BasicProbeImpl
	 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getBasicProbe()
	 * @generated
	 */
	int BASIC_PROBE = 7;

	/**
	 * The feature id for the '<em><b>Maven Artifact</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PROBE__MAVEN_ARTIFACT = 0;

	/**
	 * The feature id for the '<em><b>Fully Qualified Class Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PROBE__FULLY_QUALIFIED_CLASS_NAME = 1;

	/**
	 * The feature id for the '<em><b>Information</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PROBE__INFORMATION = 2;

	/**
	 * The feature id for the '<em><b>Tree Information</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PROBE__TREE_INFORMATION = 3;

	/**
	 * The feature id for the '<em><b>Basic Probe Has Measure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PROBE__BASIC_PROBE_HAS_MEASURE = 4;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PROBE__PARAMETER = 5;

	/**
	 * The feature id for the '<em><b>Constructor Properties</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PROBE__CONSTRUCTOR_PROPERTIES = 6;

	/**
	 * The feature id for the '<em><b>Initialization PROBE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PROBE__INITIALIZATION_PROBE = 7;

	/**
	 * The number of structural features of the '<em>Basic Probe</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PROBE_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Basic Probe</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PROBE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link mscharacterization.model.mscharacterization.impl.BasicProbeInformationParameterImpl <em>Basic Probe Information Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mscharacterization.model.mscharacterization.impl.BasicProbeInformationParameterImpl
	 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getBasicProbeInformationParameter()
	 * @generated
	 */
	int BASIC_PROBE_INFORMATION_PARAMETER = 8;

	/**
	 * The feature id for the '<em><b>Parameter Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PROBE_INFORMATION_PARAMETER__PARAMETER_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Value From Projection Field</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PROBE_INFORMATION_PARAMETER__VALUE_FROM_PROJECTION_FIELD = 1;

	/**
	 * The number of structural features of the '<em>Basic Probe Information Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PROBE_INFORMATION_PARAMETER_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Equals</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PROBE_INFORMATION_PARAMETER___EQUALS__OBJECT = 0;

	/**
	 * The number of operations of the '<em>Basic Probe Information Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_PROBE_INFORMATION_PARAMETER_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link mscharacterization.model.mscharacterization.impl.MeasureConverterImpl <em>Measure Converter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mscharacterization.model.mscharacterization.impl.MeasureConverterImpl
	 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getMeasureConverter()
	 * @generated
	 */
	int MEASURE_CONVERTER = 9;

	/**
	 * The feature id for the '<em><b>From Measure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASURE_CONVERTER__FROM_MEASURE = 0;

	/**
	 * The feature id for the '<em><b>To Measure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASURE_CONVERTER__TO_MEASURE = 1;

	/**
	 * The feature id for the '<em><b>Conversion Function VALUE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASURE_CONVERTER__CONVERSION_FUNCTION_VALUE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASURE_CONVERTER__NAME = 3;

	/**
	 * The number of structural features of the '<em>Measure Converter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASURE_CONVERTER_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Measure Converter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASURE_CONVERTER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link mscharacterization.model.mscharacterization.impl.MavenArtifactImpl <em>Maven Artifact</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mscharacterization.model.mscharacterization.impl.MavenArtifactImpl
	 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getMavenArtifact()
	 * @generated
	 */
	int MAVEN_ARTIFACT = 10;

	/**
	 * The feature id for the '<em><b>Maven Group Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAVEN_ARTIFACT__MAVEN_GROUP_ID = 0;

	/**
	 * The feature id for the '<em><b>Maven Artifact Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAVEN_ARTIFACT__MAVEN_ARTIFACT_ID = 1;

	/**
	 * The feature id for the '<em><b>Maven Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAVEN_ARTIFACT__MAVEN_VERSION = 2;

	/**
	 * The number of structural features of the '<em>Maven Artifact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAVEN_ARTIFACT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Maven Artifact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAVEN_ARTIFACT_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link mscharacterization.model.mscharacterization.MSCharacterization <em>MS Characterization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MS Characterization</em>'.
	 * @see mscharacterization.model.mscharacterization.MSCharacterization
	 * @generated
	 */
	EClass getMSCharacterization();

	/**
	 * Returns the meta object for the containment reference list '{@link mscharacterization.model.mscharacterization.MSCharacterization#getContainsMeasures <em>Contains Measures</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contains Measures</em>'.
	 * @see mscharacterization.model.mscharacterization.MSCharacterization#getContainsMeasures()
	 * @see #getMSCharacterization()
	 * @generated
	 */
	EReference getMSCharacterization_ContainsMeasures();

	/**
	 * Returns the meta object for the containment reference list '{@link mscharacterization.model.mscharacterization.MSCharacterization#getContainsViewpoints <em>Contains Viewpoints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contains Viewpoints</em>'.
	 * @see mscharacterization.model.mscharacterization.MSCharacterization#getContainsViewpoints()
	 * @see #getMSCharacterization()
	 * @generated
	 */
	EReference getMSCharacterization_ContainsViewpoints();

	/**
	 * Returns the meta object for the containment reference list '{@link mscharacterization.model.mscharacterization.MSCharacterization#getContainsInScaleRules <em>Contains In Scale Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contains In Scale Rules</em>'.
	 * @see mscharacterization.model.mscharacterization.MSCharacterization#getContainsInScaleRules()
	 * @see #getMSCharacterization()
	 * @generated
	 */
	EReference getMSCharacterization_ContainsInScaleRules();

	/**
	 * Returns the meta object for the containment reference list '{@link mscharacterization.model.mscharacterization.MSCharacterization#getContainsBasicProbes <em>Contains Basic Probes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contains Basic Probes</em>'.
	 * @see mscharacterization.model.mscharacterization.MSCharacterization#getContainsBasicProbes()
	 * @see #getMSCharacterization()
	 * @generated
	 */
	EReference getMSCharacterization_ContainsBasicProbes();

	/**
	 * Returns the meta object for the containment reference list '{@link mscharacterization.model.mscharacterization.MSCharacterization#getContainsMeasureConverters <em>Contains Measure Converters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contains Measure Converters</em>'.
	 * @see mscharacterization.model.mscharacterization.MSCharacterization#getContainsMeasureConverters()
	 * @see #getMSCharacterization()
	 * @generated
	 */
	EReference getMSCharacterization_ContainsMeasureConverters();

	/**
	 * Returns the meta object for the containment reference list '{@link mscharacterization.model.mscharacterization.MSCharacterization#getContainsMavenArtifacts <em>Contains Maven Artifacts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contains Maven Artifacts</em>'.
	 * @see mscharacterization.model.mscharacterization.MSCharacterization#getContainsMavenArtifacts()
	 * @see #getMSCharacterization()
	 * @generated
	 */
	EReference getMSCharacterization_ContainsMavenArtifacts();

	/**
	 * Returns the meta object for the reference '{@link mscharacterization.model.mscharacterization.MSCharacterization#getGeneratedMSProbesMavenArtifact <em>Generated MS Probes Maven Artifact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Generated MS Probes Maven Artifact</em>'.
	 * @see mscharacterization.model.mscharacterization.MSCharacterization#getGeneratedMSProbesMavenArtifact()
	 * @see #getMSCharacterization()
	 * @generated
	 */
	EReference getMSCharacterization_GeneratedMSProbesMavenArtifact();

	/**
	 * Returns the meta object for class '{@link mscharacterization.model.mscharacterization.Viewpoint <em>Viewpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Viewpoint</em>'.
	 * @see mscharacterization.model.mscharacterization.Viewpoint
	 * @generated
	 */
	EClass getViewpoint();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.Viewpoint#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mscharacterization.model.mscharacterization.Viewpoint#getName()
	 * @see #getViewpoint()
	 * @generated
	 */
	EAttribute getViewpoint_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link mscharacterization.model.mscharacterization.Viewpoint#getViewpointContainsDimensions <em>Viewpoint Contains Dimensions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Viewpoint Contains Dimensions</em>'.
	 * @see mscharacterization.model.mscharacterization.Viewpoint#getViewpointContainsDimensions()
	 * @see #getViewpoint()
	 * @generated
	 */
	EReference getViewpoint_ViewpointContainsDimensions();

	/**
	 * Returns the meta object for the attribute list '{@link mscharacterization.model.mscharacterization.Viewpoint#getProjectionProperties <em>Projection Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Projection Properties</em>'.
	 * @see mscharacterization.model.mscharacterization.Viewpoint#getProjectionProperties()
	 * @see #getViewpoint()
	 * @generated
	 */
	EAttribute getViewpoint_ProjectionProperties();

	/**
	 * Returns the meta object for class '{@link mscharacterization.model.mscharacterization.Dimension <em>Dimension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dimension</em>'.
	 * @see mscharacterization.model.mscharacterization.Dimension
	 * @generated
	 */
	EClass getDimension();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.Dimension#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mscharacterization.model.mscharacterization.Dimension#getName()
	 * @see #getDimension()
	 * @generated
	 */
	EAttribute getDimension_Name();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.Dimension#getTreeDimensionName <em>Tree Dimension Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tree Dimension Name</em>'.
	 * @see mscharacterization.model.mscharacterization.Dimension#getTreeDimensionName()
	 * @see #getDimension()
	 * @generated
	 */
	EAttribute getDimension_TreeDimensionName();

	/**
	 * Returns the meta object for the container reference '{@link mscharacterization.model.mscharacterization.Dimension#getDimensionIsContainedInViewpoint <em>Dimension Is Contained In Viewpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Dimension Is Contained In Viewpoint</em>'.
	 * @see mscharacterization.model.mscharacterization.Dimension#getDimensionIsContainedInViewpoint()
	 * @see #getDimension()
	 * @generated
	 */
	EReference getDimension_DimensionIsContainedInViewpoint();

	/**
	 * Returns the meta object for the containment reference list '{@link mscharacterization.model.mscharacterization.Dimension#getDimensionContainsScaleSets <em>Dimension Contains Scale Sets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dimension Contains Scale Sets</em>'.
	 * @see mscharacterization.model.mscharacterization.Dimension#getDimensionContainsScaleSets()
	 * @see #getDimension()
	 * @generated
	 */
	EReference getDimension_DimensionContainsScaleSets();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.Dimension#isGetScaleMethodHasParameters <em>Get Scale Method Has Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Get Scale Method Has Parameters</em>'.
	 * @see mscharacterization.model.mscharacterization.Dimension#isGetScaleMethodHasParameters()
	 * @see #getDimension()
	 * @generated
	 */
	EAttribute getDimension_GetScaleMethodHasParameters();

	/**
	 * Returns the meta object for the reference '{@link mscharacterization.model.mscharacterization.Dimension#getDimensionHasBasicProbe <em>Dimension Has Basic Probe</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dimension Has Basic Probe</em>'.
	 * @see mscharacterization.model.mscharacterization.Dimension#getDimensionHasBasicProbe()
	 * @see #getDimension()
	 * @generated
	 */
	EReference getDimension_DimensionHasBasicProbe();

	/**
	 * Returns the meta object for class '{@link mscharacterization.model.mscharacterization.Measure <em>Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Measure</em>'.
	 * @see mscharacterization.model.mscharacterization.Measure
	 * @generated
	 */
	EClass getMeasure();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.Measure#getUnity <em>Unity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unity</em>'.
	 * @see mscharacterization.model.mscharacterization.Measure#getUnity()
	 * @see #getMeasure()
	 * @generated
	 */
	EAttribute getMeasure_Unity();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.Measure#getValueType <em>Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Type</em>'.
	 * @see mscharacterization.model.mscharacterization.Measure#getValueType()
	 * @see #getMeasure()
	 * @generated
	 */
	EAttribute getMeasure_ValueType();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.Measure#getCompareToMethodBody_THISVALUE_OTHERVALUE <em>Compare To Method Body THISVALUE OTHERVALUE</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Compare To Method Body THISVALUE OTHERVALUE</em>'.
	 * @see mscharacterization.model.mscharacterization.Measure#getCompareToMethodBody_THISVALUE_OTHERVALUE()
	 * @see #getMeasure()
	 * @generated
	 */
	EAttribute getMeasure_CompareToMethodBody_THISVALUE_OTHERVALUE();

	/**
	 * Returns the meta object for the reference list '{@link mscharacterization.model.mscharacterization.Measure#getFromMeasureConverters <em>From Measure Converters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>From Measure Converters</em>'.
	 * @see mscharacterization.model.mscharacterization.Measure#getFromMeasureConverters()
	 * @see #getMeasure()
	 * @generated
	 */
	EReference getMeasure_FromMeasureConverters();

	/**
	 * Returns the meta object for the reference list '{@link mscharacterization.model.mscharacterization.Measure#getToMeasureConverters <em>To Measure Converters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>To Measure Converters</em>'.
	 * @see mscharacterization.model.mscharacterization.Measure#getToMeasureConverters()
	 * @see #getMeasure()
	 * @generated
	 */
	EReference getMeasure_ToMeasureConverters();

	/**
	 * Returns the meta object for the '{@link mscharacterization.model.mscharacterization.Measure#valueTypeIsComparable(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Value Type Is Comparable</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Value Type Is Comparable</em>' operation.
	 * @see mscharacterization.model.mscharacterization.Measure#valueTypeIsComparable(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getMeasure__ValueTypeIsComparable__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link mscharacterization.model.mscharacterization.InScaleRule <em>In Scale Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>In Scale Rule</em>'.
	 * @see mscharacterization.model.mscharacterization.InScaleRule
	 * @generated
	 */
	EClass getInScaleRule();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.InScaleRule#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mscharacterization.model.mscharacterization.InScaleRule#getName()
	 * @see #getInScaleRule()
	 * @generated
	 */
	EAttribute getInScaleRule_Name();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.InScaleRule#getIsInScaleCondition_VALUE_SCALE <em>Is In Scale Condition VALUE SCALE</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is In Scale Condition VALUE SCALE</em>'.
	 * @see mscharacterization.model.mscharacterization.InScaleRule#getIsInScaleCondition_VALUE_SCALE()
	 * @see #getInScaleRule()
	 * @generated
	 */
	EAttribute getInScaleRule_IsInScaleCondition_VALUE_SCALE();

	/**
	 * Returns the meta object for class '{@link mscharacterization.model.mscharacterization.ScaleSet <em>Scale Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scale Set</em>'.
	 * @see mscharacterization.model.mscharacterization.ScaleSet
	 * @generated
	 */
	EClass getScaleSet();

	/**
	 * Returns the meta object for the containment reference list '{@link mscharacterization.model.mscharacterization.ScaleSet#getScaleSetContainsScales <em>Scale Set Contains Scales</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Scale Set Contains Scales</em>'.
	 * @see mscharacterization.model.mscharacterization.ScaleSet#getScaleSetContainsScales()
	 * @see #getScaleSet()
	 * @generated
	 */
	EReference getScaleSet_ScaleSetContainsScales();

	/**
	 * Returns the meta object for the container reference '{@link mscharacterization.model.mscharacterization.ScaleSet#getScaleSetIsContainedInDimension <em>Scale Set Is Contained In Dimension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Scale Set Is Contained In Dimension</em>'.
	 * @see mscharacterization.model.mscharacterization.ScaleSet#getScaleSetIsContainedInDimension()
	 * @see #getScaleSet()
	 * @generated
	 */
	EReference getScaleSet_ScaleSetIsContainedInDimension();

	/**
	 * Returns the meta object for the reference '{@link mscharacterization.model.mscharacterization.ScaleSet#getScaleSetHasInScaleRule <em>Scale Set Has In Scale Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scale Set Has In Scale Rule</em>'.
	 * @see mscharacterization.model.mscharacterization.ScaleSet#getScaleSetHasInScaleRule()
	 * @see #getScaleSet()
	 * @generated
	 */
	EReference getScaleSet_ScaleSetHasInScaleRule();

	/**
	 * Returns the meta object for the reference '{@link mscharacterization.model.mscharacterization.ScaleSet#getScaleSetHasMeasure <em>Scale Set Has Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scale Set Has Measure</em>'.
	 * @see mscharacterization.model.mscharacterization.ScaleSet#getScaleSetHasMeasure()
	 * @see #getScaleSet()
	 * @generated
	 */
	EReference getScaleSet_ScaleSetHasMeasure();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.ScaleSet#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mscharacterization.model.mscharacterization.ScaleSet#getName()
	 * @see #getScaleSet()
	 * @generated
	 */
	EAttribute getScaleSet_Name();

	/**
	 * Returns the meta object for class '{@link mscharacterization.model.mscharacterization.Scale <em>Scale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scale</em>'.
	 * @see mscharacterization.model.mscharacterization.Scale
	 * @generated
	 */
	EClass getScale();

	/**
	 * Returns the meta object for the container reference '{@link mscharacterization.model.mscharacterization.Scale#getScaleIsContainedInScaleSet <em>Scale Is Contained In Scale Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Scale Is Contained In Scale Set</em>'.
	 * @see mscharacterization.model.mscharacterization.Scale#getScaleIsContainedInScaleSet()
	 * @see #getScale()
	 * @generated
	 */
	EReference getScale_ScaleIsContainedInScaleSet();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.Scale#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mscharacterization.model.mscharacterization.Scale#getName()
	 * @see #getScale()
	 * @generated
	 */
	EAttribute getScale_Name();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.Scale#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min</em>'.
	 * @see mscharacterization.model.mscharacterization.Scale#getMin()
	 * @see #getScale()
	 * @generated
	 */
	EAttribute getScale_Min();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.Scale#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max</em>'.
	 * @see mscharacterization.model.mscharacterization.Scale#getMax()
	 * @see #getScale()
	 * @generated
	 */
	EAttribute getScale_Max();

	/**
	 * Returns the meta object for the '{@link mscharacterization.model.mscharacterization.Scale#minHasRequiredType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Min Has Required Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Min Has Required Type</em>' operation.
	 * @see mscharacterization.model.mscharacterization.Scale#minHasRequiredType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getScale__MinHasRequiredType__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link mscharacterization.model.mscharacterization.Scale#maxHasRequiredType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Max Has Required Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Max Has Required Type</em>' operation.
	 * @see mscharacterization.model.mscharacterization.Scale#maxHasRequiredType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getScale__MaxHasRequiredType__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link mscharacterization.model.mscharacterization.BasicProbe <em>Basic Probe</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Basic Probe</em>'.
	 * @see mscharacterization.model.mscharacterization.BasicProbe
	 * @generated
	 */
	EClass getBasicProbe();

	/**
	 * Returns the meta object for the reference '{@link mscharacterization.model.mscharacterization.BasicProbe#getMavenArtifact <em>Maven Artifact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Maven Artifact</em>'.
	 * @see mscharacterization.model.mscharacterization.BasicProbe#getMavenArtifact()
	 * @see #getBasicProbe()
	 * @generated
	 */
	EReference getBasicProbe_MavenArtifact();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.BasicProbe#getFullyQualifiedClassName <em>Fully Qualified Class Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fully Qualified Class Name</em>'.
	 * @see mscharacterization.model.mscharacterization.BasicProbe#getFullyQualifiedClassName()
	 * @see #getBasicProbe()
	 * @generated
	 */
	EAttribute getBasicProbe_FullyQualifiedClassName();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.BasicProbe#getInformation <em>Information</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Information</em>'.
	 * @see mscharacterization.model.mscharacterization.BasicProbe#getInformation()
	 * @see #getBasicProbe()
	 * @generated
	 */
	EAttribute getBasicProbe_Information();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.BasicProbe#getTreeInformation <em>Tree Information</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tree Information</em>'.
	 * @see mscharacterization.model.mscharacterization.BasicProbe#getTreeInformation()
	 * @see #getBasicProbe()
	 * @generated
	 */
	EAttribute getBasicProbe_TreeInformation();

	/**
	 * Returns the meta object for the reference '{@link mscharacterization.model.mscharacterization.BasicProbe#getBasicProbeHasMeasure <em>Basic Probe Has Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Basic Probe Has Measure</em>'.
	 * @see mscharacterization.model.mscharacterization.BasicProbe#getBasicProbeHasMeasure()
	 * @see #getBasicProbe()
	 * @generated
	 */
	EReference getBasicProbe_BasicProbeHasMeasure();

	/**
	 * Returns the meta object for the containment reference '{@link mscharacterization.model.mscharacterization.BasicProbe#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameter</em>'.
	 * @see mscharacterization.model.mscharacterization.BasicProbe#getParameter()
	 * @see #getBasicProbe()
	 * @generated
	 */
	EReference getBasicProbe_Parameter();

	/**
	 * Returns the meta object for the attribute list '{@link mscharacterization.model.mscharacterization.BasicProbe#getConstructorProperties <em>Constructor Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Constructor Properties</em>'.
	 * @see mscharacterization.model.mscharacterization.BasicProbe#getConstructorProperties()
	 * @see #getBasicProbe()
	 * @generated
	 */
	EAttribute getBasicProbe_ConstructorProperties();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.BasicProbe#getInitialization_PROBE <em>Initialization PROBE</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initialization PROBE</em>'.
	 * @see mscharacterization.model.mscharacterization.BasicProbe#getInitialization_PROBE()
	 * @see #getBasicProbe()
	 * @generated
	 */
	EAttribute getBasicProbe_Initialization_PROBE();

	/**
	 * Returns the meta object for class '{@link mscharacterization.model.mscharacterization.BasicProbeInformationParameter <em>Basic Probe Information Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Basic Probe Information Parameter</em>'.
	 * @see mscharacterization.model.mscharacterization.BasicProbeInformationParameter
	 * @generated
	 */
	EClass getBasicProbeInformationParameter();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.BasicProbeInformationParameter#getParameterType <em>Parameter Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Parameter Type</em>'.
	 * @see mscharacterization.model.mscharacterization.BasicProbeInformationParameter#getParameterType()
	 * @see #getBasicProbeInformationParameter()
	 * @generated
	 */
	EAttribute getBasicProbeInformationParameter_ParameterType();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.BasicProbeInformationParameter#getValueFromProjectionField <em>Value From Projection Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value From Projection Field</em>'.
	 * @see mscharacterization.model.mscharacterization.BasicProbeInformationParameter#getValueFromProjectionField()
	 * @see #getBasicProbeInformationParameter()
	 * @generated
	 */
	EAttribute getBasicProbeInformationParameter_ValueFromProjectionField();

	/**
	 * Returns the meta object for the '{@link mscharacterization.model.mscharacterization.BasicProbeInformationParameter#equals(java.lang.Object) <em>Equals</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Equals</em>' operation.
	 * @see mscharacterization.model.mscharacterization.BasicProbeInformationParameter#equals(java.lang.Object)
	 * @generated
	 */
	EOperation getBasicProbeInformationParameter__Equals__Object();

	/**
	 * Returns the meta object for class '{@link mscharacterization.model.mscharacterization.MeasureConverter <em>Measure Converter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Measure Converter</em>'.
	 * @see mscharacterization.model.mscharacterization.MeasureConverter
	 * @generated
	 */
	EClass getMeasureConverter();

	/**
	 * Returns the meta object for the reference '{@link mscharacterization.model.mscharacterization.MeasureConverter#getFromMeasure <em>From Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>From Measure</em>'.
	 * @see mscharacterization.model.mscharacterization.MeasureConverter#getFromMeasure()
	 * @see #getMeasureConverter()
	 * @generated
	 */
	EReference getMeasureConverter_FromMeasure();

	/**
	 * Returns the meta object for the reference '{@link mscharacterization.model.mscharacterization.MeasureConverter#getToMeasure <em>To Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>To Measure</em>'.
	 * @see mscharacterization.model.mscharacterization.MeasureConverter#getToMeasure()
	 * @see #getMeasureConverter()
	 * @generated
	 */
	EReference getMeasureConverter_ToMeasure();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.MeasureConverter#getConversionFunction_VALUE <em>Conversion Function VALUE</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Conversion Function VALUE</em>'.
	 * @see mscharacterization.model.mscharacterization.MeasureConverter#getConversionFunction_VALUE()
	 * @see #getMeasureConverter()
	 * @generated
	 */
	EAttribute getMeasureConverter_ConversionFunction_VALUE();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.MeasureConverter#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mscharacterization.model.mscharacterization.MeasureConverter#getName()
	 * @see #getMeasureConverter()
	 * @generated
	 */
	EAttribute getMeasureConverter_Name();

	/**
	 * Returns the meta object for class '{@link mscharacterization.model.mscharacterization.MavenArtifact <em>Maven Artifact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Maven Artifact</em>'.
	 * @see mscharacterization.model.mscharacterization.MavenArtifact
	 * @generated
	 */
	EClass getMavenArtifact();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.MavenArtifact#getMavenGroupId <em>Maven Group Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Maven Group Id</em>'.
	 * @see mscharacterization.model.mscharacterization.MavenArtifact#getMavenGroupId()
	 * @see #getMavenArtifact()
	 * @generated
	 */
	EAttribute getMavenArtifact_MavenGroupId();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.MavenArtifact#getMavenArtifactId <em>Maven Artifact Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Maven Artifact Id</em>'.
	 * @see mscharacterization.model.mscharacterization.MavenArtifact#getMavenArtifactId()
	 * @see #getMavenArtifact()
	 * @generated
	 */
	EAttribute getMavenArtifact_MavenArtifactId();

	/**
	 * Returns the meta object for the attribute '{@link mscharacterization.model.mscharacterization.MavenArtifact#getMavenVersion <em>Maven Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Maven Version</em>'.
	 * @see mscharacterization.model.mscharacterization.MavenArtifact#getMavenVersion()
	 * @see #getMavenArtifact()
	 * @generated
	 */
	EAttribute getMavenArtifact_MavenVersion();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MSCharacterizationFactory getMSCharacterizationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link mscharacterization.model.mscharacterization.impl.MSCharacterizationImpl <em>MS Characterization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationImpl
		 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getMSCharacterization()
		 * @generated
		 */
		EClass MS_CHARACTERIZATION = eINSTANCE.getMSCharacterization();

		/**
		 * The meta object literal for the '<em><b>Contains Measures</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MS_CHARACTERIZATION__CONTAINS_MEASURES = eINSTANCE.getMSCharacterization_ContainsMeasures();

		/**
		 * The meta object literal for the '<em><b>Contains Viewpoints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MS_CHARACTERIZATION__CONTAINS_VIEWPOINTS = eINSTANCE.getMSCharacterization_ContainsViewpoints();

		/**
		 * The meta object literal for the '<em><b>Contains In Scale Rules</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MS_CHARACTERIZATION__CONTAINS_IN_SCALE_RULES = eINSTANCE.getMSCharacterization_ContainsInScaleRules();

		/**
		 * The meta object literal for the '<em><b>Contains Basic Probes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MS_CHARACTERIZATION__CONTAINS_BASIC_PROBES = eINSTANCE.getMSCharacterization_ContainsBasicProbes();

		/**
		 * The meta object literal for the '<em><b>Contains Measure Converters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MS_CHARACTERIZATION__CONTAINS_MEASURE_CONVERTERS = eINSTANCE.getMSCharacterization_ContainsMeasureConverters();

		/**
		 * The meta object literal for the '<em><b>Contains Maven Artifacts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MS_CHARACTERIZATION__CONTAINS_MAVEN_ARTIFACTS = eINSTANCE.getMSCharacterization_ContainsMavenArtifacts();

		/**
		 * The meta object literal for the '<em><b>Generated MS Probes Maven Artifact</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MS_CHARACTERIZATION__GENERATED_MS_PROBES_MAVEN_ARTIFACT = eINSTANCE.getMSCharacterization_GeneratedMSProbesMavenArtifact();

		/**
		 * The meta object literal for the '{@link mscharacterization.model.mscharacterization.impl.ViewpointImpl <em>Viewpoint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mscharacterization.model.mscharacterization.impl.ViewpointImpl
		 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getViewpoint()
		 * @generated
		 */
		EClass VIEWPOINT = eINSTANCE.getViewpoint();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VIEWPOINT__NAME = eINSTANCE.getViewpoint_Name();

		/**
		 * The meta object literal for the '<em><b>Viewpoint Contains Dimensions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VIEWPOINT__VIEWPOINT_CONTAINS_DIMENSIONS = eINSTANCE.getViewpoint_ViewpointContainsDimensions();

		/**
		 * The meta object literal for the '<em><b>Projection Properties</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VIEWPOINT__PROJECTION_PROPERTIES = eINSTANCE.getViewpoint_ProjectionProperties();

		/**
		 * The meta object literal for the '{@link mscharacterization.model.mscharacterization.impl.DimensionImpl <em>Dimension</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mscharacterization.model.mscharacterization.impl.DimensionImpl
		 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getDimension()
		 * @generated
		 */
		EClass DIMENSION = eINSTANCE.getDimension();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIMENSION__NAME = eINSTANCE.getDimension_Name();

		/**
		 * The meta object literal for the '<em><b>Tree Dimension Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIMENSION__TREE_DIMENSION_NAME = eINSTANCE.getDimension_TreeDimensionName();

		/**
		 * The meta object literal for the '<em><b>Dimension Is Contained In Viewpoint</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIMENSION__DIMENSION_IS_CONTAINED_IN_VIEWPOINT = eINSTANCE.getDimension_DimensionIsContainedInViewpoint();

		/**
		 * The meta object literal for the '<em><b>Dimension Contains Scale Sets</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIMENSION__DIMENSION_CONTAINS_SCALE_SETS = eINSTANCE.getDimension_DimensionContainsScaleSets();

		/**
		 * The meta object literal for the '<em><b>Get Scale Method Has Parameters</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIMENSION__GET_SCALE_METHOD_HAS_PARAMETERS = eINSTANCE.getDimension_GetScaleMethodHasParameters();

		/**
		 * The meta object literal for the '<em><b>Dimension Has Basic Probe</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIMENSION__DIMENSION_HAS_BASIC_PROBE = eINSTANCE.getDimension_DimensionHasBasicProbe();

		/**
		 * The meta object literal for the '{@link mscharacterization.model.mscharacterization.impl.MeasureImpl <em>Measure</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mscharacterization.model.mscharacterization.impl.MeasureImpl
		 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getMeasure()
		 * @generated
		 */
		EClass MEASURE = eINSTANCE.getMeasure();

		/**
		 * The meta object literal for the '<em><b>Unity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEASURE__UNITY = eINSTANCE.getMeasure_Unity();

		/**
		 * The meta object literal for the '<em><b>Value Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEASURE__VALUE_TYPE = eINSTANCE.getMeasure_ValueType();

		/**
		 * The meta object literal for the '<em><b>Compare To Method Body THISVALUE OTHERVALUE</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEASURE__COMPARE_TO_METHOD_BODY_THISVALUE_OTHERVALUE = eINSTANCE.getMeasure_CompareToMethodBody_THISVALUE_OTHERVALUE();

		/**
		 * The meta object literal for the '<em><b>From Measure Converters</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEASURE__FROM_MEASURE_CONVERTERS = eINSTANCE.getMeasure_FromMeasureConverters();

		/**
		 * The meta object literal for the '<em><b>To Measure Converters</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEASURE__TO_MEASURE_CONVERTERS = eINSTANCE.getMeasure_ToMeasureConverters();

		/**
		 * The meta object literal for the '<em><b>Value Type Is Comparable</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MEASURE___VALUE_TYPE_IS_COMPARABLE__DIAGNOSTICCHAIN_MAP = eINSTANCE.getMeasure__ValueTypeIsComparable__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '{@link mscharacterization.model.mscharacterization.impl.InScaleRuleImpl <em>In Scale Rule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mscharacterization.model.mscharacterization.impl.InScaleRuleImpl
		 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getInScaleRule()
		 * @generated
		 */
		EClass IN_SCALE_RULE = eINSTANCE.getInScaleRule();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_SCALE_RULE__NAME = eINSTANCE.getInScaleRule_Name();

		/**
		 * The meta object literal for the '<em><b>Is In Scale Condition VALUE SCALE</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_SCALE_RULE__IS_IN_SCALE_CONDITION_VALUE_SCALE = eINSTANCE.getInScaleRule_IsInScaleCondition_VALUE_SCALE();

		/**
		 * The meta object literal for the '{@link mscharacterization.model.mscharacterization.impl.ScaleSetImpl <em>Scale Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mscharacterization.model.mscharacterization.impl.ScaleSetImpl
		 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getScaleSet()
		 * @generated
		 */
		EClass SCALE_SET = eINSTANCE.getScaleSet();

		/**
		 * The meta object literal for the '<em><b>Scale Set Contains Scales</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCALE_SET__SCALE_SET_CONTAINS_SCALES = eINSTANCE.getScaleSet_ScaleSetContainsScales();

		/**
		 * The meta object literal for the '<em><b>Scale Set Is Contained In Dimension</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCALE_SET__SCALE_SET_IS_CONTAINED_IN_DIMENSION = eINSTANCE.getScaleSet_ScaleSetIsContainedInDimension();

		/**
		 * The meta object literal for the '<em><b>Scale Set Has In Scale Rule</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCALE_SET__SCALE_SET_HAS_IN_SCALE_RULE = eINSTANCE.getScaleSet_ScaleSetHasInScaleRule();

		/**
		 * The meta object literal for the '<em><b>Scale Set Has Measure</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCALE_SET__SCALE_SET_HAS_MEASURE = eINSTANCE.getScaleSet_ScaleSetHasMeasure();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCALE_SET__NAME = eINSTANCE.getScaleSet_Name();

		/**
		 * The meta object literal for the '{@link mscharacterization.model.mscharacterization.impl.ScaleImpl <em>Scale</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mscharacterization.model.mscharacterization.impl.ScaleImpl
		 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getScale()
		 * @generated
		 */
		EClass SCALE = eINSTANCE.getScale();

		/**
		 * The meta object literal for the '<em><b>Scale Is Contained In Scale Set</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCALE__SCALE_IS_CONTAINED_IN_SCALE_SET = eINSTANCE.getScale_ScaleIsContainedInScaleSet();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCALE__NAME = eINSTANCE.getScale_Name();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCALE__MIN = eINSTANCE.getScale_Min();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCALE__MAX = eINSTANCE.getScale_Max();

		/**
		 * The meta object literal for the '<em><b>Min Has Required Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SCALE___MIN_HAS_REQUIRED_TYPE__DIAGNOSTICCHAIN_MAP = eINSTANCE.getScale__MinHasRequiredType__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '<em><b>Max Has Required Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SCALE___MAX_HAS_REQUIRED_TYPE__DIAGNOSTICCHAIN_MAP = eINSTANCE.getScale__MaxHasRequiredType__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '{@link mscharacterization.model.mscharacterization.impl.BasicProbeImpl <em>Basic Probe</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mscharacterization.model.mscharacterization.impl.BasicProbeImpl
		 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getBasicProbe()
		 * @generated
		 */
		EClass BASIC_PROBE = eINSTANCE.getBasicProbe();

		/**
		 * The meta object literal for the '<em><b>Maven Artifact</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASIC_PROBE__MAVEN_ARTIFACT = eINSTANCE.getBasicProbe_MavenArtifact();

		/**
		 * The meta object literal for the '<em><b>Fully Qualified Class Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASIC_PROBE__FULLY_QUALIFIED_CLASS_NAME = eINSTANCE.getBasicProbe_FullyQualifiedClassName();

		/**
		 * The meta object literal for the '<em><b>Information</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASIC_PROBE__INFORMATION = eINSTANCE.getBasicProbe_Information();

		/**
		 * The meta object literal for the '<em><b>Tree Information</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASIC_PROBE__TREE_INFORMATION = eINSTANCE.getBasicProbe_TreeInformation();

		/**
		 * The meta object literal for the '<em><b>Basic Probe Has Measure</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASIC_PROBE__BASIC_PROBE_HAS_MEASURE = eINSTANCE.getBasicProbe_BasicProbeHasMeasure();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASIC_PROBE__PARAMETER = eINSTANCE.getBasicProbe_Parameter();

		/**
		 * The meta object literal for the '<em><b>Constructor Properties</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASIC_PROBE__CONSTRUCTOR_PROPERTIES = eINSTANCE.getBasicProbe_ConstructorProperties();

		/**
		 * The meta object literal for the '<em><b>Initialization PROBE</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASIC_PROBE__INITIALIZATION_PROBE = eINSTANCE.getBasicProbe_Initialization_PROBE();

		/**
		 * The meta object literal for the '{@link mscharacterization.model.mscharacterization.impl.BasicProbeInformationParameterImpl <em>Basic Probe Information Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mscharacterization.model.mscharacterization.impl.BasicProbeInformationParameterImpl
		 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getBasicProbeInformationParameter()
		 * @generated
		 */
		EClass BASIC_PROBE_INFORMATION_PARAMETER = eINSTANCE.getBasicProbeInformationParameter();

		/**
		 * The meta object literal for the '<em><b>Parameter Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASIC_PROBE_INFORMATION_PARAMETER__PARAMETER_TYPE = eINSTANCE.getBasicProbeInformationParameter_ParameterType();

		/**
		 * The meta object literal for the '<em><b>Value From Projection Field</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASIC_PROBE_INFORMATION_PARAMETER__VALUE_FROM_PROJECTION_FIELD = eINSTANCE.getBasicProbeInformationParameter_ValueFromProjectionField();

		/**
		 * The meta object literal for the '<em><b>Equals</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BASIC_PROBE_INFORMATION_PARAMETER___EQUALS__OBJECT = eINSTANCE.getBasicProbeInformationParameter__Equals__Object();

		/**
		 * The meta object literal for the '{@link mscharacterization.model.mscharacterization.impl.MeasureConverterImpl <em>Measure Converter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mscharacterization.model.mscharacterization.impl.MeasureConverterImpl
		 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getMeasureConverter()
		 * @generated
		 */
		EClass MEASURE_CONVERTER = eINSTANCE.getMeasureConverter();

		/**
		 * The meta object literal for the '<em><b>From Measure</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEASURE_CONVERTER__FROM_MEASURE = eINSTANCE.getMeasureConverter_FromMeasure();

		/**
		 * The meta object literal for the '<em><b>To Measure</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEASURE_CONVERTER__TO_MEASURE = eINSTANCE.getMeasureConverter_ToMeasure();

		/**
		 * The meta object literal for the '<em><b>Conversion Function VALUE</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEASURE_CONVERTER__CONVERSION_FUNCTION_VALUE = eINSTANCE.getMeasureConverter_ConversionFunction_VALUE();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEASURE_CONVERTER__NAME = eINSTANCE.getMeasureConverter_Name();

		/**
		 * The meta object literal for the '{@link mscharacterization.model.mscharacterization.impl.MavenArtifactImpl <em>Maven Artifact</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mscharacterization.model.mscharacterization.impl.MavenArtifactImpl
		 * @see mscharacterization.model.mscharacterization.impl.MSCharacterizationPackageImpl#getMavenArtifact()
		 * @generated
		 */
		EClass MAVEN_ARTIFACT = eINSTANCE.getMavenArtifact();

		/**
		 * The meta object literal for the '<em><b>Maven Group Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAVEN_ARTIFACT__MAVEN_GROUP_ID = eINSTANCE.getMavenArtifact_MavenGroupId();

		/**
		 * The meta object literal for the '<em><b>Maven Artifact Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAVEN_ARTIFACT__MAVEN_ARTIFACT_ID = eINSTANCE.getMavenArtifact_MavenArtifactId();

		/**
		 * The meta object literal for the '<em><b>Maven Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAVEN_ARTIFACT__MAVEN_VERSION = eINSTANCE.getMavenArtifact_MavenVersion();

	}

} //MSCharacterizationPackage
