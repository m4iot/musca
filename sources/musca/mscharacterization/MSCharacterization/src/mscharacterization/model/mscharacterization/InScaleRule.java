/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>In Scale Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.InScaleRule#getName <em>Name</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.InScaleRule#getIsInScaleCondition_VALUE_SCALE <em>Is In Scale Condition VALUE SCALE</em>}</li>
 * </ul>
 * </p>
 *
 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getInScaleRule()
 * @model
 * @generated
 */
public interface InScaleRule extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getInScaleRule_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.InScaleRule#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Is In Scale Condition VALUE SCALE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is In Scale Condition VALUE SCALE</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is In Scale Condition VALUE SCALE</em>' attribute.
	 * @see #setIsInScaleCondition_VALUE_SCALE(String)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getInScaleRule_IsInScaleCondition_VALUE_SCALE()
	 * @model required="true"
	 * @generated
	 */
	String getIsInScaleCondition_VALUE_SCALE();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.InScaleRule#getIsInScaleCondition_VALUE_SCALE <em>Is In Scale Condition VALUE SCALE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is In Scale Condition VALUE SCALE</em>' attribute.
	 * @see #getIsInScaleCondition_VALUE_SCALE()
	 * @generated
	 */
	void setIsInScaleCondition_VALUE_SCALE(String value);

} // InScaleRule
