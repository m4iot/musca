/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization.impl;

import java.util.Collection;

import mscharacterization.model.mscharacterization.Dimension;
import mscharacterization.model.mscharacterization.InScaleRule;
import mscharacterization.model.mscharacterization.MSCharacterizationPackage;
import mscharacterization.model.mscharacterization.Measure;
import mscharacterization.model.mscharacterization.Scale;
import mscharacterization.model.mscharacterization.ScaleSet;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Scale Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.ScaleSetImpl#getScaleSetContainsScales <em>Scale Set Contains Scales</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.ScaleSetImpl#getScaleSetIsContainedInDimension <em>Scale Set Is Contained In Dimension</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.ScaleSetImpl#getScaleSetHasInScaleRule <em>Scale Set Has In Scale Rule</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.ScaleSetImpl#getScaleSetHasMeasure <em>Scale Set Has Measure</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.ScaleSetImpl#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ScaleSetImpl extends MinimalEObjectImpl.Container implements ScaleSet {
	/**
	 * The cached value of the '{@link #getScaleSetContainsScales() <em>Scale Set Contains Scales</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScaleSetContainsScales()
	 * @generated
	 * @ordered
	 */
	protected EList<Scale> scaleSetContainsScales;

	/**
	 * The cached value of the '{@link #getScaleSetHasInScaleRule() <em>Scale Set Has In Scale Rule</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScaleSetHasInScaleRule()
	 * @generated
	 * @ordered
	 */
	protected InScaleRule scaleSetHasInScaleRule;

	/**
	 * The cached value of the '{@link #getScaleSetHasMeasure() <em>Scale Set Has Measure</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScaleSetHasMeasure()
	 * @generated
	 * @ordered
	 */
	protected Measure scaleSetHasMeasure;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = "";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScaleSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MSCharacterizationPackage.Literals.SCALE_SET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Scale> getScaleSetContainsScales() {
		if (scaleSetContainsScales == null) {
			scaleSetContainsScales = new EObjectContainmentWithInverseEList<Scale>(Scale.class, this, MSCharacterizationPackage.SCALE_SET__SCALE_SET_CONTAINS_SCALES, MSCharacterizationPackage.SCALE__SCALE_IS_CONTAINED_IN_SCALE_SET);
		}
		return scaleSetContainsScales;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dimension getScaleSetIsContainedInDimension() {
		if (eContainerFeatureID() != MSCharacterizationPackage.SCALE_SET__SCALE_SET_IS_CONTAINED_IN_DIMENSION) return null;
		return (Dimension)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetScaleSetIsContainedInDimension(Dimension newScaleSetIsContainedInDimension, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newScaleSetIsContainedInDimension, MSCharacterizationPackage.SCALE_SET__SCALE_SET_IS_CONTAINED_IN_DIMENSION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScaleSetIsContainedInDimension(Dimension newScaleSetIsContainedInDimension) {
		if (newScaleSetIsContainedInDimension != eInternalContainer() || (eContainerFeatureID() != MSCharacterizationPackage.SCALE_SET__SCALE_SET_IS_CONTAINED_IN_DIMENSION && newScaleSetIsContainedInDimension != null)) {
			if (EcoreUtil.isAncestor(this, newScaleSetIsContainedInDimension))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newScaleSetIsContainedInDimension != null)
				msgs = ((InternalEObject)newScaleSetIsContainedInDimension).eInverseAdd(this, MSCharacterizationPackage.DIMENSION__DIMENSION_CONTAINS_SCALE_SETS, Dimension.class, msgs);
			msgs = basicSetScaleSetIsContainedInDimension(newScaleSetIsContainedInDimension, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.SCALE_SET__SCALE_SET_IS_CONTAINED_IN_DIMENSION, newScaleSetIsContainedInDimension, newScaleSetIsContainedInDimension));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InScaleRule getScaleSetHasInScaleRule() {
		if (scaleSetHasInScaleRule != null && scaleSetHasInScaleRule.eIsProxy()) {
			InternalEObject oldScaleSetHasInScaleRule = (InternalEObject)scaleSetHasInScaleRule;
			scaleSetHasInScaleRule = (InScaleRule)eResolveProxy(oldScaleSetHasInScaleRule);
			if (scaleSetHasInScaleRule != oldScaleSetHasInScaleRule) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MSCharacterizationPackage.SCALE_SET__SCALE_SET_HAS_IN_SCALE_RULE, oldScaleSetHasInScaleRule, scaleSetHasInScaleRule));
			}
		}
		return scaleSetHasInScaleRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InScaleRule basicGetScaleSetHasInScaleRule() {
		return scaleSetHasInScaleRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScaleSetHasInScaleRule(InScaleRule newScaleSetHasInScaleRule) {
		InScaleRule oldScaleSetHasInScaleRule = scaleSetHasInScaleRule;
		scaleSetHasInScaleRule = newScaleSetHasInScaleRule;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.SCALE_SET__SCALE_SET_HAS_IN_SCALE_RULE, oldScaleSetHasInScaleRule, scaleSetHasInScaleRule));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Measure getScaleSetHasMeasure() {
		if (scaleSetHasMeasure != null && scaleSetHasMeasure.eIsProxy()) {
			InternalEObject oldScaleSetHasMeasure = (InternalEObject)scaleSetHasMeasure;
			scaleSetHasMeasure = (Measure)eResolveProxy(oldScaleSetHasMeasure);
			if (scaleSetHasMeasure != oldScaleSetHasMeasure) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MSCharacterizationPackage.SCALE_SET__SCALE_SET_HAS_MEASURE, oldScaleSetHasMeasure, scaleSetHasMeasure));
			}
		}
		return scaleSetHasMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Measure basicGetScaleSetHasMeasure() {
		return scaleSetHasMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScaleSetHasMeasure(Measure newScaleSetHasMeasure) {
		Measure oldScaleSetHasMeasure = scaleSetHasMeasure;
		scaleSetHasMeasure = newScaleSetHasMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.SCALE_SET__SCALE_SET_HAS_MEASURE, oldScaleSetHasMeasure, scaleSetHasMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getName() {
		if (this.getScaleSetIsContainedInDimension() != null && this.getScaleSetHasMeasure() != null) {
			return this.getScaleSetIsContainedInDimension().getName() + " Scale Set in " + this.getScaleSetHasMeasure().getUnity();
		} else {
			return "";
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_CONTAINS_SCALES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getScaleSetContainsScales()).basicAdd(otherEnd, msgs);
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_IS_CONTAINED_IN_DIMENSION:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetScaleSetIsContainedInDimension((Dimension)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_CONTAINS_SCALES:
				return ((InternalEList<?>)getScaleSetContainsScales()).basicRemove(otherEnd, msgs);
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_IS_CONTAINED_IN_DIMENSION:
				return basicSetScaleSetIsContainedInDimension(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_IS_CONTAINED_IN_DIMENSION:
				return eInternalContainer().eInverseRemove(this, MSCharacterizationPackage.DIMENSION__DIMENSION_CONTAINS_SCALE_SETS, Dimension.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_CONTAINS_SCALES:
				return getScaleSetContainsScales();
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_IS_CONTAINED_IN_DIMENSION:
				return getScaleSetIsContainedInDimension();
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_HAS_IN_SCALE_RULE:
				if (resolve) return getScaleSetHasInScaleRule();
				return basicGetScaleSetHasInScaleRule();
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_HAS_MEASURE:
				if (resolve) return getScaleSetHasMeasure();
				return basicGetScaleSetHasMeasure();
			case MSCharacterizationPackage.SCALE_SET__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_CONTAINS_SCALES:
				getScaleSetContainsScales().clear();
				getScaleSetContainsScales().addAll((Collection<? extends Scale>)newValue);
				return;
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_IS_CONTAINED_IN_DIMENSION:
				setScaleSetIsContainedInDimension((Dimension)newValue);
				return;
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_HAS_IN_SCALE_RULE:
				setScaleSetHasInScaleRule((InScaleRule)newValue);
				return;
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_HAS_MEASURE:
				setScaleSetHasMeasure((Measure)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_CONTAINS_SCALES:
				getScaleSetContainsScales().clear();
				return;
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_IS_CONTAINED_IN_DIMENSION:
				setScaleSetIsContainedInDimension((Dimension)null);
				return;
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_HAS_IN_SCALE_RULE:
				setScaleSetHasInScaleRule((InScaleRule)null);
				return;
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_HAS_MEASURE:
				setScaleSetHasMeasure((Measure)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_CONTAINS_SCALES:
				return scaleSetContainsScales != null && !scaleSetContainsScales.isEmpty();
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_IS_CONTAINED_IN_DIMENSION:
				return getScaleSetIsContainedInDimension() != null;
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_HAS_IN_SCALE_RULE:
				return scaleSetHasInScaleRule != null;
			case MSCharacterizationPackage.SCALE_SET__SCALE_SET_HAS_MEASURE:
				return scaleSetHasMeasure != null;
			case MSCharacterizationPackage.SCALE_SET__NAME:
				return NAME_EDEFAULT == null ? getName() != null : !NAME_EDEFAULT.equals(getName());
		}
		return super.eIsSet(featureID);
	}

} //ScaleSetImpl
