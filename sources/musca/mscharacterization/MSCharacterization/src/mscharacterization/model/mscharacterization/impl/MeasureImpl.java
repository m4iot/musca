/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.Map;

import mscharacterization.model.mscharacterization.MSCharacterizationPackage;
import mscharacterization.model.mscharacterization.Measure;
import mscharacterization.model.mscharacterization.MeasureConverter;

import mscharacterization.model.mscharacterization.util.MSCharacterizationValidator;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Measure</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.MeasureImpl#getUnity <em>Unity</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.MeasureImpl#getValueType <em>Value Type</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.MeasureImpl#getCompareToMethodBody_THISVALUE_OTHERVALUE <em>Compare To Method Body THISVALUE OTHERVALUE</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.MeasureImpl#getFromMeasureConverters <em>From Measure Converters</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.MeasureImpl#getToMeasureConverters <em>To Measure Converters</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MeasureImpl extends MinimalEObjectImpl.Container implements Measure {
	/**
	 * The default value of the '{@link #getUnity() <em>Unity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnity()
	 * @generated
	 * @ordered
	 */
	protected static final String UNITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnity() <em>Unity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnity()
	 * @generated
	 * @ordered
	 */
	protected String unity = UNITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getValueType() <em>Value Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueType()
	 * @generated
	 * @ordered
	 */
	protected Class<?> valueType;

	/**
	 * The default value of the '{@link #getCompareToMethodBody_THISVALUE_OTHERVALUE() <em>Compare To Method Body THISVALUE OTHERVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompareToMethodBody_THISVALUE_OTHERVALUE()
	 * @generated
	 * @ordered
	 */
	protected static final String COMPARE_TO_METHOD_BODY_THISVALUE_OTHERVALUE_EDEFAULT = "THISVALUE.compareTo(OTHERVALUE)";

	/**
	 * The cached value of the '{@link #getCompareToMethodBody_THISVALUE_OTHERVALUE() <em>Compare To Method Body THISVALUE OTHERVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompareToMethodBody_THISVALUE_OTHERVALUE()
	 * @generated
	 * @ordered
	 */
	protected String compareToMethodBody_THISVALUE_OTHERVALUE = COMPARE_TO_METHOD_BODY_THISVALUE_OTHERVALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFromMeasureConverters() <em>From Measure Converters</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromMeasureConverters()
	 * @generated
	 * @ordered
	 */
	protected EList<MeasureConverter> fromMeasureConverters;

	/**
	 * The cached value of the '{@link #getToMeasureConverters() <em>To Measure Converters</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToMeasureConverters()
	 * @generated
	 * @ordered
	 */
	protected EList<MeasureConverter> toMeasureConverters;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MeasureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MSCharacterizationPackage.Literals.MEASURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnity() {
		return unity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnity(String newUnity) {
		String oldUnity = unity;
		unity = newUnity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.MEASURE__UNITY, oldUnity, unity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Class<?> getValueType() {
		return valueType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueType(Class<?> newValueType) {
		Class<?> oldValueType = valueType;
		valueType = newValueType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.MEASURE__VALUE_TYPE, oldValueType, valueType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCompareToMethodBody_THISVALUE_OTHERVALUE() {
		return compareToMethodBody_THISVALUE_OTHERVALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCompareToMethodBody_THISVALUE_OTHERVALUE(String newCompareToMethodBody_THISVALUE_OTHERVALUE) {
		String oldCompareToMethodBody_THISVALUE_OTHERVALUE = compareToMethodBody_THISVALUE_OTHERVALUE;
		compareToMethodBody_THISVALUE_OTHERVALUE = newCompareToMethodBody_THISVALUE_OTHERVALUE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.MEASURE__COMPARE_TO_METHOD_BODY_THISVALUE_OTHERVALUE, oldCompareToMethodBody_THISVALUE_OTHERVALUE, compareToMethodBody_THISVALUE_OTHERVALUE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MeasureConverter> getFromMeasureConverters() {
		if (fromMeasureConverters == null) {
			fromMeasureConverters = new EObjectWithInverseResolvingEList<MeasureConverter>(MeasureConverter.class, this, MSCharacterizationPackage.MEASURE__FROM_MEASURE_CONVERTERS, MSCharacterizationPackage.MEASURE_CONVERTER__FROM_MEASURE);
		}
		return fromMeasureConverters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MeasureConverter> getToMeasureConverters() {
		if (toMeasureConverters == null) {
			toMeasureConverters = new EObjectWithInverseResolvingEList<MeasureConverter>(MeasureConverter.class, this, MSCharacterizationPackage.MEASURE__TO_MEASURE_CONVERTERS, MSCharacterizationPackage.MEASURE_CONVERTER__TO_MEASURE);
		}
		return toMeasureConverters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean valueTypeIsComparable(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		
		// TODO Improve error message
		if (!Comparable.class.isAssignableFrom(getValueType())) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 MSCharacterizationValidator.DIAGNOSTIC_SOURCE,
						 MSCharacterizationValidator.MEASURE__VALUE_TYPE_IS_COMPARABLE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "valueTypeIsComparable", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MSCharacterizationPackage.MEASURE__FROM_MEASURE_CONVERTERS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getFromMeasureConverters()).basicAdd(otherEnd, msgs);
			case MSCharacterizationPackage.MEASURE__TO_MEASURE_CONVERTERS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getToMeasureConverters()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MSCharacterizationPackage.MEASURE__FROM_MEASURE_CONVERTERS:
				return ((InternalEList<?>)getFromMeasureConverters()).basicRemove(otherEnd, msgs);
			case MSCharacterizationPackage.MEASURE__TO_MEASURE_CONVERTERS:
				return ((InternalEList<?>)getToMeasureConverters()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MSCharacterizationPackage.MEASURE__UNITY:
				return getUnity();
			case MSCharacterizationPackage.MEASURE__VALUE_TYPE:
				return getValueType();
			case MSCharacterizationPackage.MEASURE__COMPARE_TO_METHOD_BODY_THISVALUE_OTHERVALUE:
				return getCompareToMethodBody_THISVALUE_OTHERVALUE();
			case MSCharacterizationPackage.MEASURE__FROM_MEASURE_CONVERTERS:
				return getFromMeasureConverters();
			case MSCharacterizationPackage.MEASURE__TO_MEASURE_CONVERTERS:
				return getToMeasureConverters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MSCharacterizationPackage.MEASURE__UNITY:
				setUnity((String)newValue);
				return;
			case MSCharacterizationPackage.MEASURE__VALUE_TYPE:
				setValueType((Class<?>)newValue);
				return;
			case MSCharacterizationPackage.MEASURE__COMPARE_TO_METHOD_BODY_THISVALUE_OTHERVALUE:
				setCompareToMethodBody_THISVALUE_OTHERVALUE((String)newValue);
				return;
			case MSCharacterizationPackage.MEASURE__FROM_MEASURE_CONVERTERS:
				getFromMeasureConverters().clear();
				getFromMeasureConverters().addAll((Collection<? extends MeasureConverter>)newValue);
				return;
			case MSCharacterizationPackage.MEASURE__TO_MEASURE_CONVERTERS:
				getToMeasureConverters().clear();
				getToMeasureConverters().addAll((Collection<? extends MeasureConverter>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.MEASURE__UNITY:
				setUnity(UNITY_EDEFAULT);
				return;
			case MSCharacterizationPackage.MEASURE__VALUE_TYPE:
				setValueType((Class<?>)null);
				return;
			case MSCharacterizationPackage.MEASURE__COMPARE_TO_METHOD_BODY_THISVALUE_OTHERVALUE:
				setCompareToMethodBody_THISVALUE_OTHERVALUE(COMPARE_TO_METHOD_BODY_THISVALUE_OTHERVALUE_EDEFAULT);
				return;
			case MSCharacterizationPackage.MEASURE__FROM_MEASURE_CONVERTERS:
				getFromMeasureConverters().clear();
				return;
			case MSCharacterizationPackage.MEASURE__TO_MEASURE_CONVERTERS:
				getToMeasureConverters().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.MEASURE__UNITY:
				return UNITY_EDEFAULT == null ? unity != null : !UNITY_EDEFAULT.equals(unity);
			case MSCharacterizationPackage.MEASURE__VALUE_TYPE:
				return valueType != null;
			case MSCharacterizationPackage.MEASURE__COMPARE_TO_METHOD_BODY_THISVALUE_OTHERVALUE:
				return COMPARE_TO_METHOD_BODY_THISVALUE_OTHERVALUE_EDEFAULT == null ? compareToMethodBody_THISVALUE_OTHERVALUE != null : !COMPARE_TO_METHOD_BODY_THISVALUE_OTHERVALUE_EDEFAULT.equals(compareToMethodBody_THISVALUE_OTHERVALUE);
			case MSCharacterizationPackage.MEASURE__FROM_MEASURE_CONVERTERS:
				return fromMeasureConverters != null && !fromMeasureConverters.isEmpty();
			case MSCharacterizationPackage.MEASURE__TO_MEASURE_CONVERTERS:
				return toMeasureConverters != null && !toMeasureConverters.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case MSCharacterizationPackage.MEASURE___VALUE_TYPE_IS_COMPARABLE__DIAGNOSTICCHAIN_MAP:
				return valueTypeIsComparable((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (unity: ");
		result.append(unity);
		result.append(", valueType: ");
		result.append(valueType);
		result.append(", compareToMethodBody_THISVALUE_OTHERVALUE: ");
		result.append(compareToMethodBody_THISVALUE_OTHERVALUE);
		result.append(')');
		return result.toString();
	}

} //MeasureImpl
