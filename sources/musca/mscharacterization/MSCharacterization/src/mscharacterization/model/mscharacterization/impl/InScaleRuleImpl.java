/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization.impl;

import mscharacterization.model.mscharacterization.InScaleRule;
import mscharacterization.model.mscharacterization.MSCharacterizationPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>In Scale Rule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.InScaleRuleImpl#getName <em>Name</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.InScaleRuleImpl#getIsInScaleCondition_VALUE_SCALE <em>Is In Scale Condition VALUE SCALE</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class InScaleRuleImpl extends MinimalEObjectImpl.Container implements InScaleRule {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getIsInScaleCondition_VALUE_SCALE() <em>Is In Scale Condition VALUE SCALE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsInScaleCondition_VALUE_SCALE()
	 * @generated
	 * @ordered
	 */
	protected static final String IS_IN_SCALE_CONDITION_VALUE_SCALE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIsInScaleCondition_VALUE_SCALE() <em>Is In Scale Condition VALUE SCALE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsInScaleCondition_VALUE_SCALE()
	 * @generated
	 * @ordered
	 */
	protected String isInScaleCondition_VALUE_SCALE = IS_IN_SCALE_CONDITION_VALUE_SCALE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InScaleRuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MSCharacterizationPackage.Literals.IN_SCALE_RULE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.IN_SCALE_RULE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getIsInScaleCondition_VALUE_SCALE() {
		return isInScaleCondition_VALUE_SCALE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsInScaleCondition_VALUE_SCALE(String newIsInScaleCondition_VALUE_SCALE) {
		String oldIsInScaleCondition_VALUE_SCALE = isInScaleCondition_VALUE_SCALE;
		isInScaleCondition_VALUE_SCALE = newIsInScaleCondition_VALUE_SCALE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.IN_SCALE_RULE__IS_IN_SCALE_CONDITION_VALUE_SCALE, oldIsInScaleCondition_VALUE_SCALE, isInScaleCondition_VALUE_SCALE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MSCharacterizationPackage.IN_SCALE_RULE__NAME:
				return getName();
			case MSCharacterizationPackage.IN_SCALE_RULE__IS_IN_SCALE_CONDITION_VALUE_SCALE:
				return getIsInScaleCondition_VALUE_SCALE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MSCharacterizationPackage.IN_SCALE_RULE__NAME:
				setName((String)newValue);
				return;
			case MSCharacterizationPackage.IN_SCALE_RULE__IS_IN_SCALE_CONDITION_VALUE_SCALE:
				setIsInScaleCondition_VALUE_SCALE((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.IN_SCALE_RULE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case MSCharacterizationPackage.IN_SCALE_RULE__IS_IN_SCALE_CONDITION_VALUE_SCALE:
				setIsInScaleCondition_VALUE_SCALE(IS_IN_SCALE_CONDITION_VALUE_SCALE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.IN_SCALE_RULE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case MSCharacterizationPackage.IN_SCALE_RULE__IS_IN_SCALE_CONDITION_VALUE_SCALE:
				return IS_IN_SCALE_CONDITION_VALUE_SCALE_EDEFAULT == null ? isInScaleCondition_VALUE_SCALE != null : !IS_IN_SCALE_CONDITION_VALUE_SCALE_EDEFAULT.equals(isInScaleCondition_VALUE_SCALE);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", isInScaleCondition_VALUE_SCALE: ");
		result.append(isInScaleCondition_VALUE_SCALE);
		result.append(')');
		return result.toString();
	}

} //InScaleRuleImpl
