/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization.impl;

import mscharacterization.model.mscharacterization.MSCharacterizationPackage;
import mscharacterization.model.mscharacterization.MavenArtifact;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Maven Artifact</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.MavenArtifactImpl#getMavenGroupId <em>Maven Group Id</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.MavenArtifactImpl#getMavenArtifactId <em>Maven Artifact Id</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.MavenArtifactImpl#getMavenVersion <em>Maven Version</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MavenArtifactImpl extends MinimalEObjectImpl.Container implements MavenArtifact {
	/**
	 * The default value of the '{@link #getMavenGroupId() <em>Maven Group Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMavenGroupId()
	 * @generated
	 * @ordered
	 */
	protected static final String MAVEN_GROUP_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMavenGroupId() <em>Maven Group Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMavenGroupId()
	 * @generated
	 * @ordered
	 */
	protected String mavenGroupId = MAVEN_GROUP_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getMavenArtifactId() <em>Maven Artifact Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMavenArtifactId()
	 * @generated
	 * @ordered
	 */
	protected static final String MAVEN_ARTIFACT_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMavenArtifactId() <em>Maven Artifact Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMavenArtifactId()
	 * @generated
	 * @ordered
	 */
	protected String mavenArtifactId = MAVEN_ARTIFACT_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getMavenVersion() <em>Maven Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMavenVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String MAVEN_VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMavenVersion() <em>Maven Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMavenVersion()
	 * @generated
	 * @ordered
	 */
	protected String mavenVersion = MAVEN_VERSION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MavenArtifactImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MSCharacterizationPackage.Literals.MAVEN_ARTIFACT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMavenGroupId() {
		return mavenGroupId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMavenGroupId(String newMavenGroupId) {
		String oldMavenGroupId = mavenGroupId;
		mavenGroupId = newMavenGroupId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.MAVEN_ARTIFACT__MAVEN_GROUP_ID, oldMavenGroupId, mavenGroupId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMavenArtifactId() {
		return mavenArtifactId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMavenArtifactId(String newMavenArtifactId) {
		String oldMavenArtifactId = mavenArtifactId;
		mavenArtifactId = newMavenArtifactId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.MAVEN_ARTIFACT__MAVEN_ARTIFACT_ID, oldMavenArtifactId, mavenArtifactId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMavenVersion() {
		return mavenVersion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMavenVersion(String newMavenVersion) {
		String oldMavenVersion = mavenVersion;
		mavenVersion = newMavenVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.MAVEN_ARTIFACT__MAVEN_VERSION, oldMavenVersion, mavenVersion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MSCharacterizationPackage.MAVEN_ARTIFACT__MAVEN_GROUP_ID:
				return getMavenGroupId();
			case MSCharacterizationPackage.MAVEN_ARTIFACT__MAVEN_ARTIFACT_ID:
				return getMavenArtifactId();
			case MSCharacterizationPackage.MAVEN_ARTIFACT__MAVEN_VERSION:
				return getMavenVersion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MSCharacterizationPackage.MAVEN_ARTIFACT__MAVEN_GROUP_ID:
				setMavenGroupId((String)newValue);
				return;
			case MSCharacterizationPackage.MAVEN_ARTIFACT__MAVEN_ARTIFACT_ID:
				setMavenArtifactId((String)newValue);
				return;
			case MSCharacterizationPackage.MAVEN_ARTIFACT__MAVEN_VERSION:
				setMavenVersion((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.MAVEN_ARTIFACT__MAVEN_GROUP_ID:
				setMavenGroupId(MAVEN_GROUP_ID_EDEFAULT);
				return;
			case MSCharacterizationPackage.MAVEN_ARTIFACT__MAVEN_ARTIFACT_ID:
				setMavenArtifactId(MAVEN_ARTIFACT_ID_EDEFAULT);
				return;
			case MSCharacterizationPackage.MAVEN_ARTIFACT__MAVEN_VERSION:
				setMavenVersion(MAVEN_VERSION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.MAVEN_ARTIFACT__MAVEN_GROUP_ID:
				return MAVEN_GROUP_ID_EDEFAULT == null ? mavenGroupId != null : !MAVEN_GROUP_ID_EDEFAULT.equals(mavenGroupId);
			case MSCharacterizationPackage.MAVEN_ARTIFACT__MAVEN_ARTIFACT_ID:
				return MAVEN_ARTIFACT_ID_EDEFAULT == null ? mavenArtifactId != null : !MAVEN_ARTIFACT_ID_EDEFAULT.equals(mavenArtifactId);
			case MSCharacterizationPackage.MAVEN_ARTIFACT__MAVEN_VERSION:
				return MAVEN_VERSION_EDEFAULT == null ? mavenVersion != null : !MAVEN_VERSION_EDEFAULT.equals(mavenVersion);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mavenGroupId: ");
		result.append(mavenGroupId);
		result.append(", mavenArtifactId: ");
		result.append(mavenArtifactId);
		result.append(", mavenVersion: ");
		result.append(mavenVersion);
		result.append(')');
		return result.toString();
	}

} //MavenArtifactImpl
