/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import mscharacterization.model.mscharacterization.MSCharacterizationPackage;
import mscharacterization.model.mscharacterization.Scale;
import mscharacterization.model.mscharacterization.ScaleSet;

import mscharacterization.model.mscharacterization.util.MSCharacterizationValidator;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Scale</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.ScaleImpl#getScaleIsContainedInScaleSet <em>Scale Is Contained In Scale Set</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.ScaleImpl#getName <em>Name</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.ScaleImpl#getMin <em>Min</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.ScaleImpl#getMax <em>Max</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ScaleImpl extends MinimalEObjectImpl.Container implements Scale {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getMin() <em>Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin()
	 * @generated
	 * @ordered
	 */
	protected static final String MIN_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMin() <em>Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin()
	 * @generated
	 * @ordered
	 */
	protected String min = MIN_EDEFAULT;

	/**
	 * The default value of the '{@link #getMax() <em>Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax()
	 * @generated
	 * @ordered
	 */
	protected static final String MAX_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMax() <em>Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax()
	 * @generated
	 * @ordered
	 */
	protected String max = MAX_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScaleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MSCharacterizationPackage.Literals.SCALE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScaleSet getScaleIsContainedInScaleSet() {
		if (eContainerFeatureID() != MSCharacterizationPackage.SCALE__SCALE_IS_CONTAINED_IN_SCALE_SET) return null;
		return (ScaleSet)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetScaleIsContainedInScaleSet(ScaleSet newScaleIsContainedInScaleSet, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newScaleIsContainedInScaleSet, MSCharacterizationPackage.SCALE__SCALE_IS_CONTAINED_IN_SCALE_SET, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScaleIsContainedInScaleSet(ScaleSet newScaleIsContainedInScaleSet) {
		if (newScaleIsContainedInScaleSet != eInternalContainer() || (eContainerFeatureID() != MSCharacterizationPackage.SCALE__SCALE_IS_CONTAINED_IN_SCALE_SET && newScaleIsContainedInScaleSet != null)) {
			if (EcoreUtil.isAncestor(this, newScaleIsContainedInScaleSet))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newScaleIsContainedInScaleSet != null)
				msgs = ((InternalEObject)newScaleIsContainedInScaleSet).eInverseAdd(this, MSCharacterizationPackage.SCALE_SET__SCALE_SET_CONTAINS_SCALES, ScaleSet.class, msgs);
			msgs = basicSetScaleIsContainedInScaleSet(newScaleIsContainedInScaleSet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.SCALE__SCALE_IS_CONTAINED_IN_SCALE_SET, newScaleIsContainedInScaleSet, newScaleIsContainedInScaleSet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.SCALE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMin() {
		return min;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMin(String newMin) {
		String oldMin = min;
		min = newMin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.SCALE__MIN, oldMin, min));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMax() {
		return max;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMax(String newMax) {
		String oldMax = max;
		max = newMax;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.SCALE__MAX, oldMax, max));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean minHasRequiredType(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		
		Object min;
		try {
			Class<?> valueType = getScaleIsContainedInScaleSet().getScaleSetHasMeasure().getValueType();
			min = valueType.getDeclaredConstructor(String.class).newInstance(getMin());
		} catch (Exception e) {
			min = null;
		}
			
		// TODO Improve error message
		if (min == null) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 MSCharacterizationValidator.DIAGNOSTIC_SOURCE,
						 MSCharacterizationValidator.SCALE__MIN_HAS_REQUIRED_TYPE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "minHasRequiredType", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean maxHasRequiredType(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		
		Object max;
		try {
			Class<?> valueType = getScaleIsContainedInScaleSet().getScaleSetHasMeasure().getValueType();
			max = valueType.getDeclaredConstructor(String.class).newInstance(getMax());
		} catch (Exception e) {
			max = null;
		}
		
		// TODO Improve error message
		if (max == null) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 MSCharacterizationValidator.DIAGNOSTIC_SOURCE,
						 MSCharacterizationValidator.SCALE__MAX_HAS_REQUIRED_TYPE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "maxHasRequiredType", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MSCharacterizationPackage.SCALE__SCALE_IS_CONTAINED_IN_SCALE_SET:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetScaleIsContainedInScaleSet((ScaleSet)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MSCharacterizationPackage.SCALE__SCALE_IS_CONTAINED_IN_SCALE_SET:
				return basicSetScaleIsContainedInScaleSet(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case MSCharacterizationPackage.SCALE__SCALE_IS_CONTAINED_IN_SCALE_SET:
				return eInternalContainer().eInverseRemove(this, MSCharacterizationPackage.SCALE_SET__SCALE_SET_CONTAINS_SCALES, ScaleSet.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MSCharacterizationPackage.SCALE__SCALE_IS_CONTAINED_IN_SCALE_SET:
				return getScaleIsContainedInScaleSet();
			case MSCharacterizationPackage.SCALE__NAME:
				return getName();
			case MSCharacterizationPackage.SCALE__MIN:
				return getMin();
			case MSCharacterizationPackage.SCALE__MAX:
				return getMax();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MSCharacterizationPackage.SCALE__SCALE_IS_CONTAINED_IN_SCALE_SET:
				setScaleIsContainedInScaleSet((ScaleSet)newValue);
				return;
			case MSCharacterizationPackage.SCALE__NAME:
				setName((String)newValue);
				return;
			case MSCharacterizationPackage.SCALE__MIN:
				setMin((String)newValue);
				return;
			case MSCharacterizationPackage.SCALE__MAX:
				setMax((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.SCALE__SCALE_IS_CONTAINED_IN_SCALE_SET:
				setScaleIsContainedInScaleSet((ScaleSet)null);
				return;
			case MSCharacterizationPackage.SCALE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case MSCharacterizationPackage.SCALE__MIN:
				setMin(MIN_EDEFAULT);
				return;
			case MSCharacterizationPackage.SCALE__MAX:
				setMax(MAX_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.SCALE__SCALE_IS_CONTAINED_IN_SCALE_SET:
				return getScaleIsContainedInScaleSet() != null;
			case MSCharacterizationPackage.SCALE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case MSCharacterizationPackage.SCALE__MIN:
				return MIN_EDEFAULT == null ? min != null : !MIN_EDEFAULT.equals(min);
			case MSCharacterizationPackage.SCALE__MAX:
				return MAX_EDEFAULT == null ? max != null : !MAX_EDEFAULT.equals(max);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case MSCharacterizationPackage.SCALE___MIN_HAS_REQUIRED_TYPE__DIAGNOSTICCHAIN_MAP:
				return minHasRequiredType((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case MSCharacterizationPackage.SCALE___MAX_HAS_REQUIRED_TYPE__DIAGNOSTICCHAIN_MAP:
				return maxHasRequiredType((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", min: ");
		result.append(min);
		result.append(", max: ");
		result.append(max);
		result.append(')');
		return result.toString();
	}

} //ScaleImpl
