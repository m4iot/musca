/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Basic Probe</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.BasicProbe#getMavenArtifact <em>Maven Artifact</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.BasicProbe#getFullyQualifiedClassName <em>Fully Qualified Class Name</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.BasicProbe#getInformation <em>Information</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.BasicProbe#getTreeInformation <em>Tree Information</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.BasicProbe#getBasicProbeHasMeasure <em>Basic Probe Has Measure</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.BasicProbe#getParameter <em>Parameter</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.BasicProbe#getConstructorProperties <em>Constructor Properties</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.BasicProbe#getInitialization_PROBE <em>Initialization PROBE</em>}</li>
 * </ul>
 * </p>
 *
 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getBasicProbe()
 * @model
 * @generated
 */
public interface BasicProbe extends EObject {
	/**
	 * Returns the value of the '<em><b>Maven Artifact</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Maven Artifact</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Maven Artifact</em>' reference.
	 * @see #setMavenArtifact(MavenArtifact)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getBasicProbe_MavenArtifact()
	 * @model required="true"
	 * @generated
	 */
	MavenArtifact getMavenArtifact();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.BasicProbe#getMavenArtifact <em>Maven Artifact</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Maven Artifact</em>' reference.
	 * @see #getMavenArtifact()
	 * @generated
	 */
	void setMavenArtifact(MavenArtifact value);

	/**
	 * Returns the value of the '<em><b>Fully Qualified Class Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fully Qualified Class Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fully Qualified Class Name</em>' attribute.
	 * @see #setFullyQualifiedClassName(String)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getBasicProbe_FullyQualifiedClassName()
	 * @model required="true"
	 * @generated
	 */
	String getFullyQualifiedClassName();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.BasicProbe#getFullyQualifiedClassName <em>Fully Qualified Class Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fully Qualified Class Name</em>' attribute.
	 * @see #getFullyQualifiedClassName()
	 * @generated
	 */
	void setFullyQualifiedClassName(String value);

	/**
	 * Returns the value of the '<em><b>Information</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Information</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Information</em>' attribute.
	 * @see #setInformation(String)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getBasicProbe_Information()
	 * @model required="true"
	 * @generated
	 */
	String getInformation();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.BasicProbe#getInformation <em>Information</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Information</em>' attribute.
	 * @see #getInformation()
	 * @generated
	 */
	void setInformation(String value);

	/**
	 * Returns the value of the '<em><b>Tree Information</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tree Information</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tree Information</em>' attribute.
	 * @see #setTreeInformation(String)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getBasicProbe_TreeInformation()
	 * @model
	 * @generated
	 */
	String getTreeInformation();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.BasicProbe#getTreeInformation <em>Tree Information</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tree Information</em>' attribute.
	 * @see #getTreeInformation()
	 * @generated
	 */
	void setTreeInformation(String value);

	/**
	 * Returns the value of the '<em><b>Basic Probe Has Measure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Basic Probe Has Measure</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Basic Probe Has Measure</em>' reference.
	 * @see #setBasicProbeHasMeasure(Measure)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getBasicProbe_BasicProbeHasMeasure()
	 * @model required="true"
	 * @generated
	 */
	Measure getBasicProbeHasMeasure();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.BasicProbe#getBasicProbeHasMeasure <em>Basic Probe Has Measure</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Basic Probe Has Measure</em>' reference.
	 * @see #getBasicProbeHasMeasure()
	 * @generated
	 */
	void setBasicProbeHasMeasure(Measure value);

	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' containment reference.
	 * @see #setParameter(BasicProbeInformationParameter)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getBasicProbe_Parameter()
	 * @model containment="true"
	 * @generated
	 */
	BasicProbeInformationParameter getParameter();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.BasicProbe#getParameter <em>Parameter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter</em>' containment reference.
	 * @see #getParameter()
	 * @generated
	 */
	void setParameter(BasicProbeInformationParameter value);

	/**
	 * Returns the value of the '<em><b>Constructor Properties</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constructor Properties</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constructor Properties</em>' attribute list.
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getBasicProbe_ConstructorProperties()
	 * @model
	 * @generated
	 */
	EList<String> getConstructorProperties();

	/**
	 * Returns the value of the '<em><b>Initialization PROBE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The optional initialization code of the probe, in Java.
	 * The keyword PROBE will be replaced by the actual probe object.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Initialization PROBE</em>' attribute.
	 * @see #setInitialization_PROBE(String)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getBasicProbe_Initialization_PROBE()
	 * @model
	 * @generated
	 */
	String getInitialization_PROBE();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.BasicProbe#getInitialization_PROBE <em>Initialization PROBE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initialization PROBE</em>' attribute.
	 * @see #getInitialization_PROBE()
	 * @generated
	 */
	void setInitialization_PROBE(String value);

} // BasicProbe
