/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MS Characterization</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.MSCharacterization#getContainsMeasures <em>Contains Measures</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.MSCharacterization#getContainsViewpoints <em>Contains Viewpoints</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.MSCharacterization#getContainsInScaleRules <em>Contains In Scale Rules</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.MSCharacterization#getContainsBasicProbes <em>Contains Basic Probes</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.MSCharacterization#getContainsMeasureConverters <em>Contains Measure Converters</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.MSCharacterization#getContainsMavenArtifacts <em>Contains Maven Artifacts</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.MSCharacterization#getGeneratedMSProbesMavenArtifact <em>Generated MS Probes Maven Artifact</em>}</li>
 * </ul>
 * </p>
 *
 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMSCharacterization()
 * @model
 * @generated
 */
public interface MSCharacterization extends EObject {
	/**
	 * Returns the value of the '<em><b>Contains Measures</b></em>' containment reference list.
	 * The list contents are of type {@link mscharacterization.model.mscharacterization.Measure}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contains Measures</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contains Measures</em>' containment reference list.
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMSCharacterization_ContainsMeasures()
	 * @model containment="true"
	 * @generated
	 */
	EList<Measure> getContainsMeasures();

	/**
	 * Returns the value of the '<em><b>Contains Viewpoints</b></em>' containment reference list.
	 * The list contents are of type {@link mscharacterization.model.mscharacterization.Viewpoint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contains Viewpoints</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contains Viewpoints</em>' containment reference list.
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMSCharacterization_ContainsViewpoints()
	 * @model containment="true"
	 * @generated
	 */
	EList<Viewpoint> getContainsViewpoints();

	/**
	 * Returns the value of the '<em><b>Contains In Scale Rules</b></em>' containment reference list.
	 * The list contents are of type {@link mscharacterization.model.mscharacterization.InScaleRule}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contains In Scale Rules</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contains In Scale Rules</em>' containment reference list.
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMSCharacterization_ContainsInScaleRules()
	 * @model containment="true"
	 * @generated
	 */
	EList<InScaleRule> getContainsInScaleRules();

	/**
	 * Returns the value of the '<em><b>Contains Basic Probes</b></em>' containment reference list.
	 * The list contents are of type {@link mscharacterization.model.mscharacterization.BasicProbe}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contains Basic Probes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contains Basic Probes</em>' containment reference list.
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMSCharacterization_ContainsBasicProbes()
	 * @model containment="true"
	 * @generated
	 */
	EList<BasicProbe> getContainsBasicProbes();

	/**
	 * Returns the value of the '<em><b>Contains Measure Converters</b></em>' containment reference list.
	 * The list contents are of type {@link mscharacterization.model.mscharacterization.MeasureConverter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contains Measure Converters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contains Measure Converters</em>' containment reference list.
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMSCharacterization_ContainsMeasureConverters()
	 * @model containment="true"
	 * @generated
	 */
	EList<MeasureConverter> getContainsMeasureConverters();

	/**
	 * Returns the value of the '<em><b>Contains Maven Artifacts</b></em>' containment reference list.
	 * The list contents are of type {@link mscharacterization.model.mscharacterization.MavenArtifact}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contains Maven Artifacts</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contains Maven Artifacts</em>' containment reference list.
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMSCharacterization_ContainsMavenArtifacts()
	 * @model containment="true"
	 * @generated
	 */
	EList<MavenArtifact> getContainsMavenArtifacts();

	/**
	 * Returns the value of the '<em><b>Generated MS Probes Maven Artifact</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generated MS Probes Maven Artifact</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generated MS Probes Maven Artifact</em>' reference.
	 * @see #setGeneratedMSProbesMavenArtifact(MavenArtifact)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMSCharacterization_GeneratedMSProbesMavenArtifact()
	 * @model required="true"
	 * @generated
	 */
	MavenArtifact getGeneratedMSProbesMavenArtifact();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.MSCharacterization#getGeneratedMSProbesMavenArtifact <em>Generated MS Probes Maven Artifact</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generated MS Probes Maven Artifact</em>' reference.
	 * @see #getGeneratedMSProbesMavenArtifact()
	 * @generated
	 */
	void setGeneratedMSProbesMavenArtifact(MavenArtifact value);

} // MSCharacterization
