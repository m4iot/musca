/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization.impl;

import java.util.Collection;

import mscharacterization.model.mscharacterization.BasicProbe;
import mscharacterization.model.mscharacterization.Dimension;
import mscharacterization.model.mscharacterization.MSCharacterizationPackage;
import mscharacterization.model.mscharacterization.ScaleSet;
import mscharacterization.model.mscharacterization.Viewpoint;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dimension</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.DimensionImpl#getName <em>Name</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.DimensionImpl#getTreeDimensionName <em>Tree Dimension Name</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.DimensionImpl#getDimensionIsContainedInViewpoint <em>Dimension Is Contained In Viewpoint</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.DimensionImpl#getDimensionContainsScaleSets <em>Dimension Contains Scale Sets</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.DimensionImpl#isGetScaleMethodHasParameters <em>Get Scale Method Has Parameters</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.DimensionImpl#getDimensionHasBasicProbe <em>Dimension Has Basic Probe</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DimensionImpl extends MinimalEObjectImpl.Container implements Dimension {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getTreeDimensionName() <em>Tree Dimension Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTreeDimensionName()
	 * @generated
	 * @ordered
	 */
	protected static final String TREE_DIMENSION_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTreeDimensionName() <em>Tree Dimension Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTreeDimensionName()
	 * @generated
	 * @ordered
	 */
	protected String treeDimensionName = TREE_DIMENSION_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDimensionContainsScaleSets() <em>Dimension Contains Scale Sets</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDimensionContainsScaleSets()
	 * @generated
	 * @ordered
	 */
	protected EList<ScaleSet> dimensionContainsScaleSets;

	/**
	 * The default value of the '{@link #isGetScaleMethodHasParameters() <em>Get Scale Method Has Parameters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isGetScaleMethodHasParameters()
	 * @generated
	 * @ordered
	 */
	protected static final boolean GET_SCALE_METHOD_HAS_PARAMETERS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isGetScaleMethodHasParameters() <em>Get Scale Method Has Parameters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isGetScaleMethodHasParameters()
	 * @generated
	 * @ordered
	 */
	protected boolean getScaleMethodHasParameters = GET_SCALE_METHOD_HAS_PARAMETERS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDimensionHasBasicProbe() <em>Dimension Has Basic Probe</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDimensionHasBasicProbe()
	 * @generated
	 * @ordered
	 */
	protected BasicProbe dimensionHasBasicProbe;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DimensionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MSCharacterizationPackage.Literals.DIMENSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.DIMENSION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTreeDimensionName() {
		return treeDimensionName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTreeDimensionName(String newTreeDimensionName) {
		String oldTreeDimensionName = treeDimensionName;
		treeDimensionName = newTreeDimensionName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.DIMENSION__TREE_DIMENSION_NAME, oldTreeDimensionName, treeDimensionName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Viewpoint getDimensionIsContainedInViewpoint() {
		if (eContainerFeatureID() != MSCharacterizationPackage.DIMENSION__DIMENSION_IS_CONTAINED_IN_VIEWPOINT) return null;
		return (Viewpoint)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDimensionIsContainedInViewpoint(Viewpoint newDimensionIsContainedInViewpoint, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newDimensionIsContainedInViewpoint, MSCharacterizationPackage.DIMENSION__DIMENSION_IS_CONTAINED_IN_VIEWPOINT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDimensionIsContainedInViewpoint(Viewpoint newDimensionIsContainedInViewpoint) {
		if (newDimensionIsContainedInViewpoint != eInternalContainer() || (eContainerFeatureID() != MSCharacterizationPackage.DIMENSION__DIMENSION_IS_CONTAINED_IN_VIEWPOINT && newDimensionIsContainedInViewpoint != null)) {
			if (EcoreUtil.isAncestor(this, newDimensionIsContainedInViewpoint))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newDimensionIsContainedInViewpoint != null)
				msgs = ((InternalEObject)newDimensionIsContainedInViewpoint).eInverseAdd(this, MSCharacterizationPackage.VIEWPOINT__VIEWPOINT_CONTAINS_DIMENSIONS, Viewpoint.class, msgs);
			msgs = basicSetDimensionIsContainedInViewpoint(newDimensionIsContainedInViewpoint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.DIMENSION__DIMENSION_IS_CONTAINED_IN_VIEWPOINT, newDimensionIsContainedInViewpoint, newDimensionIsContainedInViewpoint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScaleSet> getDimensionContainsScaleSets() {
		if (dimensionContainsScaleSets == null) {
			dimensionContainsScaleSets = new EObjectContainmentWithInverseEList<ScaleSet>(ScaleSet.class, this, MSCharacterizationPackage.DIMENSION__DIMENSION_CONTAINS_SCALE_SETS, MSCharacterizationPackage.SCALE_SET__SCALE_SET_IS_CONTAINED_IN_DIMENSION);
		}
		return dimensionContainsScaleSets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isGetScaleMethodHasParameters() {
		return getScaleMethodHasParameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGetScaleMethodHasParameters(boolean newGetScaleMethodHasParameters) {
		boolean oldGetScaleMethodHasParameters = getScaleMethodHasParameters;
		getScaleMethodHasParameters = newGetScaleMethodHasParameters;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.DIMENSION__GET_SCALE_METHOD_HAS_PARAMETERS, oldGetScaleMethodHasParameters, getScaleMethodHasParameters));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicProbe getDimensionHasBasicProbe() {
		if (dimensionHasBasicProbe != null && dimensionHasBasicProbe.eIsProxy()) {
			InternalEObject oldDimensionHasBasicProbe = (InternalEObject)dimensionHasBasicProbe;
			dimensionHasBasicProbe = (BasicProbe)eResolveProxy(oldDimensionHasBasicProbe);
			if (dimensionHasBasicProbe != oldDimensionHasBasicProbe) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MSCharacterizationPackage.DIMENSION__DIMENSION_HAS_BASIC_PROBE, oldDimensionHasBasicProbe, dimensionHasBasicProbe));
			}
		}
		return dimensionHasBasicProbe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicProbe basicGetDimensionHasBasicProbe() {
		return dimensionHasBasicProbe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDimensionHasBasicProbe(BasicProbe newDimensionHasBasicProbe) {
		BasicProbe oldDimensionHasBasicProbe = dimensionHasBasicProbe;
		dimensionHasBasicProbe = newDimensionHasBasicProbe;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.DIMENSION__DIMENSION_HAS_BASIC_PROBE, oldDimensionHasBasicProbe, dimensionHasBasicProbe));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MSCharacterizationPackage.DIMENSION__DIMENSION_IS_CONTAINED_IN_VIEWPOINT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetDimensionIsContainedInViewpoint((Viewpoint)otherEnd, msgs);
			case MSCharacterizationPackage.DIMENSION__DIMENSION_CONTAINS_SCALE_SETS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getDimensionContainsScaleSets()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MSCharacterizationPackage.DIMENSION__DIMENSION_IS_CONTAINED_IN_VIEWPOINT:
				return basicSetDimensionIsContainedInViewpoint(null, msgs);
			case MSCharacterizationPackage.DIMENSION__DIMENSION_CONTAINS_SCALE_SETS:
				return ((InternalEList<?>)getDimensionContainsScaleSets()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case MSCharacterizationPackage.DIMENSION__DIMENSION_IS_CONTAINED_IN_VIEWPOINT:
				return eInternalContainer().eInverseRemove(this, MSCharacterizationPackage.VIEWPOINT__VIEWPOINT_CONTAINS_DIMENSIONS, Viewpoint.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MSCharacterizationPackage.DIMENSION__NAME:
				return getName();
			case MSCharacterizationPackage.DIMENSION__TREE_DIMENSION_NAME:
				return getTreeDimensionName();
			case MSCharacterizationPackage.DIMENSION__DIMENSION_IS_CONTAINED_IN_VIEWPOINT:
				return getDimensionIsContainedInViewpoint();
			case MSCharacterizationPackage.DIMENSION__DIMENSION_CONTAINS_SCALE_SETS:
				return getDimensionContainsScaleSets();
			case MSCharacterizationPackage.DIMENSION__GET_SCALE_METHOD_HAS_PARAMETERS:
				return isGetScaleMethodHasParameters();
			case MSCharacterizationPackage.DIMENSION__DIMENSION_HAS_BASIC_PROBE:
				if (resolve) return getDimensionHasBasicProbe();
				return basicGetDimensionHasBasicProbe();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MSCharacterizationPackage.DIMENSION__NAME:
				setName((String)newValue);
				return;
			case MSCharacterizationPackage.DIMENSION__TREE_DIMENSION_NAME:
				setTreeDimensionName((String)newValue);
				return;
			case MSCharacterizationPackage.DIMENSION__DIMENSION_IS_CONTAINED_IN_VIEWPOINT:
				setDimensionIsContainedInViewpoint((Viewpoint)newValue);
				return;
			case MSCharacterizationPackage.DIMENSION__DIMENSION_CONTAINS_SCALE_SETS:
				getDimensionContainsScaleSets().clear();
				getDimensionContainsScaleSets().addAll((Collection<? extends ScaleSet>)newValue);
				return;
			case MSCharacterizationPackage.DIMENSION__GET_SCALE_METHOD_HAS_PARAMETERS:
				setGetScaleMethodHasParameters((Boolean)newValue);
				return;
			case MSCharacterizationPackage.DIMENSION__DIMENSION_HAS_BASIC_PROBE:
				setDimensionHasBasicProbe((BasicProbe)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.DIMENSION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case MSCharacterizationPackage.DIMENSION__TREE_DIMENSION_NAME:
				setTreeDimensionName(TREE_DIMENSION_NAME_EDEFAULT);
				return;
			case MSCharacterizationPackage.DIMENSION__DIMENSION_IS_CONTAINED_IN_VIEWPOINT:
				setDimensionIsContainedInViewpoint((Viewpoint)null);
				return;
			case MSCharacterizationPackage.DIMENSION__DIMENSION_CONTAINS_SCALE_SETS:
				getDimensionContainsScaleSets().clear();
				return;
			case MSCharacterizationPackage.DIMENSION__GET_SCALE_METHOD_HAS_PARAMETERS:
				setGetScaleMethodHasParameters(GET_SCALE_METHOD_HAS_PARAMETERS_EDEFAULT);
				return;
			case MSCharacterizationPackage.DIMENSION__DIMENSION_HAS_BASIC_PROBE:
				setDimensionHasBasicProbe((BasicProbe)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.DIMENSION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case MSCharacterizationPackage.DIMENSION__TREE_DIMENSION_NAME:
				return TREE_DIMENSION_NAME_EDEFAULT == null ? treeDimensionName != null : !TREE_DIMENSION_NAME_EDEFAULT.equals(treeDimensionName);
			case MSCharacterizationPackage.DIMENSION__DIMENSION_IS_CONTAINED_IN_VIEWPOINT:
				return getDimensionIsContainedInViewpoint() != null;
			case MSCharacterizationPackage.DIMENSION__DIMENSION_CONTAINS_SCALE_SETS:
				return dimensionContainsScaleSets != null && !dimensionContainsScaleSets.isEmpty();
			case MSCharacterizationPackage.DIMENSION__GET_SCALE_METHOD_HAS_PARAMETERS:
				return getScaleMethodHasParameters != GET_SCALE_METHOD_HAS_PARAMETERS_EDEFAULT;
			case MSCharacterizationPackage.DIMENSION__DIMENSION_HAS_BASIC_PROBE:
				return dimensionHasBasicProbe != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", treeDimensionName: ");
		result.append(treeDimensionName);
		result.append(", getScaleMethodHasParameters: ");
		result.append(getScaleMethodHasParameters);
		result.append(')');
		return result.toString();
	}

} //DimensionImpl
