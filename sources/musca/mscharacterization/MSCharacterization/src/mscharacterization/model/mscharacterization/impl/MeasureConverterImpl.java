/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization.impl;

import mscharacterization.model.mscharacterization.MSCharacterizationPackage;
import mscharacterization.model.mscharacterization.Measure;
import mscharacterization.model.mscharacterization.MeasureConverter;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Measure Converter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.MeasureConverterImpl#getFromMeasure <em>From Measure</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.MeasureConverterImpl#getToMeasure <em>To Measure</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.MeasureConverterImpl#getConversionFunction_VALUE <em>Conversion Function VALUE</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.MeasureConverterImpl#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MeasureConverterImpl extends MinimalEObjectImpl.Container implements MeasureConverter {
	/**
	 * The cached value of the '{@link #getFromMeasure() <em>From Measure</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromMeasure()
	 * @generated
	 * @ordered
	 */
	protected Measure fromMeasure;

	/**
	 * The cached value of the '{@link #getToMeasure() <em>To Measure</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToMeasure()
	 * @generated
	 * @ordered
	 */
	protected Measure toMeasure;

	/**
	 * The default value of the '{@link #getConversionFunction_VALUE() <em>Conversion Function VALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConversionFunction_VALUE()
	 * @generated
	 * @ordered
	 */
	protected static final String CONVERSION_FUNCTION_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getConversionFunction_VALUE() <em>Conversion Function VALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConversionFunction_VALUE()
	 * @generated
	 * @ordered
	 */
	protected String conversionFunction_VALUE = CONVERSION_FUNCTION_VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = "";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MeasureConverterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MSCharacterizationPackage.Literals.MEASURE_CONVERTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Measure getFromMeasure() {
		if (fromMeasure != null && fromMeasure.eIsProxy()) {
			InternalEObject oldFromMeasure = (InternalEObject)fromMeasure;
			fromMeasure = (Measure)eResolveProxy(oldFromMeasure);
			if (fromMeasure != oldFromMeasure) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MSCharacterizationPackage.MEASURE_CONVERTER__FROM_MEASURE, oldFromMeasure, fromMeasure));
			}
		}
		return fromMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Measure basicGetFromMeasure() {
		return fromMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFromMeasure(Measure newFromMeasure, NotificationChain msgs) {
		Measure oldFromMeasure = fromMeasure;
		fromMeasure = newFromMeasure;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.MEASURE_CONVERTER__FROM_MEASURE, oldFromMeasure, newFromMeasure);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFromMeasure(Measure newFromMeasure) {
		if (newFromMeasure != fromMeasure) {
			NotificationChain msgs = null;
			if (fromMeasure != null)
				msgs = ((InternalEObject)fromMeasure).eInverseRemove(this, MSCharacterizationPackage.MEASURE__FROM_MEASURE_CONVERTERS, Measure.class, msgs);
			if (newFromMeasure != null)
				msgs = ((InternalEObject)newFromMeasure).eInverseAdd(this, MSCharacterizationPackage.MEASURE__FROM_MEASURE_CONVERTERS, Measure.class, msgs);
			msgs = basicSetFromMeasure(newFromMeasure, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.MEASURE_CONVERTER__FROM_MEASURE, newFromMeasure, newFromMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Measure getToMeasure() {
		if (toMeasure != null && toMeasure.eIsProxy()) {
			InternalEObject oldToMeasure = (InternalEObject)toMeasure;
			toMeasure = (Measure)eResolveProxy(oldToMeasure);
			if (toMeasure != oldToMeasure) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MSCharacterizationPackage.MEASURE_CONVERTER__TO_MEASURE, oldToMeasure, toMeasure));
			}
		}
		return toMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Measure basicGetToMeasure() {
		return toMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetToMeasure(Measure newToMeasure, NotificationChain msgs) {
		Measure oldToMeasure = toMeasure;
		toMeasure = newToMeasure;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.MEASURE_CONVERTER__TO_MEASURE, oldToMeasure, newToMeasure);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToMeasure(Measure newToMeasure) {
		if (newToMeasure != toMeasure) {
			NotificationChain msgs = null;
			if (toMeasure != null)
				msgs = ((InternalEObject)toMeasure).eInverseRemove(this, MSCharacterizationPackage.MEASURE__TO_MEASURE_CONVERTERS, Measure.class, msgs);
			if (newToMeasure != null)
				msgs = ((InternalEObject)newToMeasure).eInverseAdd(this, MSCharacterizationPackage.MEASURE__TO_MEASURE_CONVERTERS, Measure.class, msgs);
			msgs = basicSetToMeasure(newToMeasure, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.MEASURE_CONVERTER__TO_MEASURE, newToMeasure, newToMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConversionFunction_VALUE() {
		return conversionFunction_VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConversionFunction_VALUE(String newConversionFunction_VALUE) {
		String oldConversionFunction_VALUE = conversionFunction_VALUE;
		conversionFunction_VALUE = newConversionFunction_VALUE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.MEASURE_CONVERTER__CONVERSION_FUNCTION_VALUE, oldConversionFunction_VALUE, conversionFunction_VALUE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getName() {
		return "from" + getFromMeasure().getUnity() + " To " + getToMeasure().getUnity();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MSCharacterizationPackage.MEASURE_CONVERTER__FROM_MEASURE:
				if (fromMeasure != null)
					msgs = ((InternalEObject)fromMeasure).eInverseRemove(this, MSCharacterizationPackage.MEASURE__FROM_MEASURE_CONVERTERS, Measure.class, msgs);
				return basicSetFromMeasure((Measure)otherEnd, msgs);
			case MSCharacterizationPackage.MEASURE_CONVERTER__TO_MEASURE:
				if (toMeasure != null)
					msgs = ((InternalEObject)toMeasure).eInverseRemove(this, MSCharacterizationPackage.MEASURE__TO_MEASURE_CONVERTERS, Measure.class, msgs);
				return basicSetToMeasure((Measure)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MSCharacterizationPackage.MEASURE_CONVERTER__FROM_MEASURE:
				return basicSetFromMeasure(null, msgs);
			case MSCharacterizationPackage.MEASURE_CONVERTER__TO_MEASURE:
				return basicSetToMeasure(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MSCharacterizationPackage.MEASURE_CONVERTER__FROM_MEASURE:
				if (resolve) return getFromMeasure();
				return basicGetFromMeasure();
			case MSCharacterizationPackage.MEASURE_CONVERTER__TO_MEASURE:
				if (resolve) return getToMeasure();
				return basicGetToMeasure();
			case MSCharacterizationPackage.MEASURE_CONVERTER__CONVERSION_FUNCTION_VALUE:
				return getConversionFunction_VALUE();
			case MSCharacterizationPackage.MEASURE_CONVERTER__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MSCharacterizationPackage.MEASURE_CONVERTER__FROM_MEASURE:
				setFromMeasure((Measure)newValue);
				return;
			case MSCharacterizationPackage.MEASURE_CONVERTER__TO_MEASURE:
				setToMeasure((Measure)newValue);
				return;
			case MSCharacterizationPackage.MEASURE_CONVERTER__CONVERSION_FUNCTION_VALUE:
				setConversionFunction_VALUE((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.MEASURE_CONVERTER__FROM_MEASURE:
				setFromMeasure((Measure)null);
				return;
			case MSCharacterizationPackage.MEASURE_CONVERTER__TO_MEASURE:
				setToMeasure((Measure)null);
				return;
			case MSCharacterizationPackage.MEASURE_CONVERTER__CONVERSION_FUNCTION_VALUE:
				setConversionFunction_VALUE(CONVERSION_FUNCTION_VALUE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.MEASURE_CONVERTER__FROM_MEASURE:
				return fromMeasure != null;
			case MSCharacterizationPackage.MEASURE_CONVERTER__TO_MEASURE:
				return toMeasure != null;
			case MSCharacterizationPackage.MEASURE_CONVERTER__CONVERSION_FUNCTION_VALUE:
				return CONVERSION_FUNCTION_VALUE_EDEFAULT == null ? conversionFunction_VALUE != null : !CONVERSION_FUNCTION_VALUE_EDEFAULT.equals(conversionFunction_VALUE);
			case MSCharacterizationPackage.MEASURE_CONVERTER__NAME:
				return NAME_EDEFAULT == null ? getName() != null : !NAME_EDEFAULT.equals(getName());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (conversionFunction_VALUE: ");
		result.append(conversionFunction_VALUE);
		result.append(')');
		return result.toString();
	}

} //MeasureConverterImpl
