/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Maven Artifact</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.MavenArtifact#getMavenGroupId <em>Maven Group Id</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.MavenArtifact#getMavenArtifactId <em>Maven Artifact Id</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.MavenArtifact#getMavenVersion <em>Maven Version</em>}</li>
 * </ul>
 * </p>
 *
 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMavenArtifact()
 * @model
 * @generated
 */
public interface MavenArtifact extends EObject {
	/**
	 * Returns the value of the '<em><b>Maven Group Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Maven Group Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Maven Group Id</em>' attribute.
	 * @see #setMavenGroupId(String)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMavenArtifact_MavenGroupId()
	 * @model required="true"
	 * @generated
	 */
	String getMavenGroupId();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.MavenArtifact#getMavenGroupId <em>Maven Group Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Maven Group Id</em>' attribute.
	 * @see #getMavenGroupId()
	 * @generated
	 */
	void setMavenGroupId(String value);

	/**
	 * Returns the value of the '<em><b>Maven Artifact Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Maven Artifact Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Maven Artifact Id</em>' attribute.
	 * @see #setMavenArtifactId(String)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMavenArtifact_MavenArtifactId()
	 * @model required="true"
	 * @generated
	 */
	String getMavenArtifactId();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.MavenArtifact#getMavenArtifactId <em>Maven Artifact Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Maven Artifact Id</em>' attribute.
	 * @see #getMavenArtifactId()
	 * @generated
	 */
	void setMavenArtifactId(String value);

	/**
	 * Returns the value of the '<em><b>Maven Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Maven Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Maven Version</em>' attribute.
	 * @see #setMavenVersion(String)
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#getMavenArtifact_MavenVersion()
	 * @model required="true"
	 * @generated
	 */
	String getMavenVersion();

	/**
	 * Sets the value of the '{@link mscharacterization.model.mscharacterization.MavenArtifact#getMavenVersion <em>Maven Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Maven Version</em>' attribute.
	 * @see #getMavenVersion()
	 * @generated
	 */
	void setMavenVersion(String value);

} // MavenArtifact
