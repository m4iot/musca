/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization.impl;

import mscharacterization.model.mscharacterization.BasicProbe;
import mscharacterization.model.mscharacterization.BasicProbeInformationParameter;
import mscharacterization.model.mscharacterization.Dimension;
import mscharacterization.model.mscharacterization.InScaleRule;
import mscharacterization.model.mscharacterization.MSCharacterization;
import mscharacterization.model.mscharacterization.MSCharacterizationFactory;
import mscharacterization.model.mscharacterization.MSCharacterizationPackage;
import mscharacterization.model.mscharacterization.MavenArtifact;
import mscharacterization.model.mscharacterization.Measure;
import mscharacterization.model.mscharacterization.MeasureConverter;
import mscharacterization.model.mscharacterization.Scale;
import mscharacterization.model.mscharacterization.ScaleSet;
import mscharacterization.model.mscharacterization.Viewpoint;

import mscharacterization.model.mscharacterization.util.MSCharacterizationValidator;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MSCharacterizationPackageImpl extends EPackageImpl implements MSCharacterizationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass msCharacterizationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass viewpointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dimensionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass measureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inScaleRuleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scaleSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scaleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass basicProbeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass basicProbeInformationParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass measureConverterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mavenArtifactEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see mscharacterization.model.mscharacterization.MSCharacterizationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MSCharacterizationPackageImpl() {
		super(eNS_URI, MSCharacterizationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link MSCharacterizationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static MSCharacterizationPackage init() {
		if (isInited) return (MSCharacterizationPackage)EPackage.Registry.INSTANCE.getEPackage(MSCharacterizationPackage.eNS_URI);

		// Obtain or create and register package
		MSCharacterizationPackageImpl theMSCharacterizationPackage = (MSCharacterizationPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof MSCharacterizationPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new MSCharacterizationPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theMSCharacterizationPackage.createPackageContents();

		// Initialize created meta-data
		theMSCharacterizationPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theMSCharacterizationPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return MSCharacterizationValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theMSCharacterizationPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(MSCharacterizationPackage.eNS_URI, theMSCharacterizationPackage);
		return theMSCharacterizationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMSCharacterization() {
		return msCharacterizationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSCharacterization_ContainsMeasures() {
		return (EReference)msCharacterizationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSCharacterization_ContainsViewpoints() {
		return (EReference)msCharacterizationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSCharacterization_ContainsInScaleRules() {
		return (EReference)msCharacterizationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSCharacterization_ContainsBasicProbes() {
		return (EReference)msCharacterizationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSCharacterization_ContainsMeasureConverters() {
		return (EReference)msCharacterizationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSCharacterization_ContainsMavenArtifacts() {
		return (EReference)msCharacterizationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSCharacterization_GeneratedMSProbesMavenArtifact() {
		return (EReference)msCharacterizationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getViewpoint() {
		return viewpointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getViewpoint_Name() {
		return (EAttribute)viewpointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getViewpoint_ViewpointContainsDimensions() {
		return (EReference)viewpointEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getViewpoint_ProjectionProperties() {
		return (EAttribute)viewpointEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDimension() {
		return dimensionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDimension_Name() {
		return (EAttribute)dimensionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDimension_TreeDimensionName() {
		return (EAttribute)dimensionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDimension_DimensionIsContainedInViewpoint() {
		return (EReference)dimensionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDimension_DimensionContainsScaleSets() {
		return (EReference)dimensionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDimension_GetScaleMethodHasParameters() {
		return (EAttribute)dimensionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDimension_DimensionHasBasicProbe() {
		return (EReference)dimensionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMeasure() {
		return measureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMeasure_Unity() {
		return (EAttribute)measureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMeasure_ValueType() {
		return (EAttribute)measureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMeasure_CompareToMethodBody_THISVALUE_OTHERVALUE() {
		return (EAttribute)measureEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMeasure_FromMeasureConverters() {
		return (EReference)measureEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMeasure_ToMeasureConverters() {
		return (EReference)measureEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMeasure__ValueTypeIsComparable__DiagnosticChain_Map() {
		return measureEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInScaleRule() {
		return inScaleRuleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInScaleRule_Name() {
		return (EAttribute)inScaleRuleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInScaleRule_IsInScaleCondition_VALUE_SCALE() {
		return (EAttribute)inScaleRuleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScaleSet() {
		return scaleSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScaleSet_ScaleSetContainsScales() {
		return (EReference)scaleSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScaleSet_ScaleSetIsContainedInDimension() {
		return (EReference)scaleSetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScaleSet_ScaleSetHasInScaleRule() {
		return (EReference)scaleSetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScaleSet_ScaleSetHasMeasure() {
		return (EReference)scaleSetEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScaleSet_Name() {
		return (EAttribute)scaleSetEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScale() {
		return scaleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScale_ScaleIsContainedInScaleSet() {
		return (EReference)scaleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScale_Name() {
		return (EAttribute)scaleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScale_Min() {
		return (EAttribute)scaleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScale_Max() {
		return (EAttribute)scaleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getScale__MinHasRequiredType__DiagnosticChain_Map() {
		return scaleEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getScale__MaxHasRequiredType__DiagnosticChain_Map() {
		return scaleEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBasicProbe() {
		return basicProbeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBasicProbe_MavenArtifact() {
		return (EReference)basicProbeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBasicProbe_FullyQualifiedClassName() {
		return (EAttribute)basicProbeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBasicProbe_Information() {
		return (EAttribute)basicProbeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBasicProbe_TreeInformation() {
		return (EAttribute)basicProbeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBasicProbe_BasicProbeHasMeasure() {
		return (EReference)basicProbeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBasicProbe_Parameter() {
		return (EReference)basicProbeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBasicProbe_ConstructorProperties() {
		return (EAttribute)basicProbeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBasicProbe_Initialization_PROBE() {
		return (EAttribute)basicProbeEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBasicProbeInformationParameter() {
		return basicProbeInformationParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBasicProbeInformationParameter_ParameterType() {
		return (EAttribute)basicProbeInformationParameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBasicProbeInformationParameter_ValueFromProjectionField() {
		return (EAttribute)basicProbeInformationParameterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBasicProbeInformationParameter__Equals__Object() {
		return basicProbeInformationParameterEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMeasureConverter() {
		return measureConverterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMeasureConverter_FromMeasure() {
		return (EReference)measureConverterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMeasureConverter_ToMeasure() {
		return (EReference)measureConverterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMeasureConverter_ConversionFunction_VALUE() {
		return (EAttribute)measureConverterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMeasureConverter_Name() {
		return (EAttribute)measureConverterEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMavenArtifact() {
		return mavenArtifactEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMavenArtifact_MavenGroupId() {
		return (EAttribute)mavenArtifactEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMavenArtifact_MavenArtifactId() {
		return (EAttribute)mavenArtifactEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMavenArtifact_MavenVersion() {
		return (EAttribute)mavenArtifactEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSCharacterizationFactory getMSCharacterizationFactory() {
		return (MSCharacterizationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		msCharacterizationEClass = createEClass(MS_CHARACTERIZATION);
		createEReference(msCharacterizationEClass, MS_CHARACTERIZATION__CONTAINS_MEASURES);
		createEReference(msCharacterizationEClass, MS_CHARACTERIZATION__CONTAINS_VIEWPOINTS);
		createEReference(msCharacterizationEClass, MS_CHARACTERIZATION__CONTAINS_IN_SCALE_RULES);
		createEReference(msCharacterizationEClass, MS_CHARACTERIZATION__CONTAINS_BASIC_PROBES);
		createEReference(msCharacterizationEClass, MS_CHARACTERIZATION__CONTAINS_MEASURE_CONVERTERS);
		createEReference(msCharacterizationEClass, MS_CHARACTERIZATION__CONTAINS_MAVEN_ARTIFACTS);
		createEReference(msCharacterizationEClass, MS_CHARACTERIZATION__GENERATED_MS_PROBES_MAVEN_ARTIFACT);

		viewpointEClass = createEClass(VIEWPOINT);
		createEAttribute(viewpointEClass, VIEWPOINT__NAME);
		createEReference(viewpointEClass, VIEWPOINT__VIEWPOINT_CONTAINS_DIMENSIONS);
		createEAttribute(viewpointEClass, VIEWPOINT__PROJECTION_PROPERTIES);

		dimensionEClass = createEClass(DIMENSION);
		createEAttribute(dimensionEClass, DIMENSION__NAME);
		createEAttribute(dimensionEClass, DIMENSION__TREE_DIMENSION_NAME);
		createEReference(dimensionEClass, DIMENSION__DIMENSION_IS_CONTAINED_IN_VIEWPOINT);
		createEReference(dimensionEClass, DIMENSION__DIMENSION_CONTAINS_SCALE_SETS);
		createEAttribute(dimensionEClass, DIMENSION__GET_SCALE_METHOD_HAS_PARAMETERS);
		createEReference(dimensionEClass, DIMENSION__DIMENSION_HAS_BASIC_PROBE);

		measureEClass = createEClass(MEASURE);
		createEAttribute(measureEClass, MEASURE__UNITY);
		createEAttribute(measureEClass, MEASURE__VALUE_TYPE);
		createEAttribute(measureEClass, MEASURE__COMPARE_TO_METHOD_BODY_THISVALUE_OTHERVALUE);
		createEReference(measureEClass, MEASURE__FROM_MEASURE_CONVERTERS);
		createEReference(measureEClass, MEASURE__TO_MEASURE_CONVERTERS);
		createEOperation(measureEClass, MEASURE___VALUE_TYPE_IS_COMPARABLE__DIAGNOSTICCHAIN_MAP);

		inScaleRuleEClass = createEClass(IN_SCALE_RULE);
		createEAttribute(inScaleRuleEClass, IN_SCALE_RULE__NAME);
		createEAttribute(inScaleRuleEClass, IN_SCALE_RULE__IS_IN_SCALE_CONDITION_VALUE_SCALE);

		scaleSetEClass = createEClass(SCALE_SET);
		createEReference(scaleSetEClass, SCALE_SET__SCALE_SET_CONTAINS_SCALES);
		createEReference(scaleSetEClass, SCALE_SET__SCALE_SET_IS_CONTAINED_IN_DIMENSION);
		createEReference(scaleSetEClass, SCALE_SET__SCALE_SET_HAS_IN_SCALE_RULE);
		createEReference(scaleSetEClass, SCALE_SET__SCALE_SET_HAS_MEASURE);
		createEAttribute(scaleSetEClass, SCALE_SET__NAME);

		scaleEClass = createEClass(SCALE);
		createEReference(scaleEClass, SCALE__SCALE_IS_CONTAINED_IN_SCALE_SET);
		createEAttribute(scaleEClass, SCALE__NAME);
		createEAttribute(scaleEClass, SCALE__MIN);
		createEAttribute(scaleEClass, SCALE__MAX);
		createEOperation(scaleEClass, SCALE___MIN_HAS_REQUIRED_TYPE__DIAGNOSTICCHAIN_MAP);
		createEOperation(scaleEClass, SCALE___MAX_HAS_REQUIRED_TYPE__DIAGNOSTICCHAIN_MAP);

		basicProbeEClass = createEClass(BASIC_PROBE);
		createEReference(basicProbeEClass, BASIC_PROBE__MAVEN_ARTIFACT);
		createEAttribute(basicProbeEClass, BASIC_PROBE__FULLY_QUALIFIED_CLASS_NAME);
		createEAttribute(basicProbeEClass, BASIC_PROBE__INFORMATION);
		createEAttribute(basicProbeEClass, BASIC_PROBE__TREE_INFORMATION);
		createEReference(basicProbeEClass, BASIC_PROBE__BASIC_PROBE_HAS_MEASURE);
		createEReference(basicProbeEClass, BASIC_PROBE__PARAMETER);
		createEAttribute(basicProbeEClass, BASIC_PROBE__CONSTRUCTOR_PROPERTIES);
		createEAttribute(basicProbeEClass, BASIC_PROBE__INITIALIZATION_PROBE);

		basicProbeInformationParameterEClass = createEClass(BASIC_PROBE_INFORMATION_PARAMETER);
		createEAttribute(basicProbeInformationParameterEClass, BASIC_PROBE_INFORMATION_PARAMETER__PARAMETER_TYPE);
		createEAttribute(basicProbeInformationParameterEClass, BASIC_PROBE_INFORMATION_PARAMETER__VALUE_FROM_PROJECTION_FIELD);
		createEOperation(basicProbeInformationParameterEClass, BASIC_PROBE_INFORMATION_PARAMETER___EQUALS__OBJECT);

		measureConverterEClass = createEClass(MEASURE_CONVERTER);
		createEReference(measureConverterEClass, MEASURE_CONVERTER__FROM_MEASURE);
		createEReference(measureConverterEClass, MEASURE_CONVERTER__TO_MEASURE);
		createEAttribute(measureConverterEClass, MEASURE_CONVERTER__CONVERSION_FUNCTION_VALUE);
		createEAttribute(measureConverterEClass, MEASURE_CONVERTER__NAME);

		mavenArtifactEClass = createEClass(MAVEN_ARTIFACT);
		createEAttribute(mavenArtifactEClass, MAVEN_ARTIFACT__MAVEN_GROUP_ID);
		createEAttribute(mavenArtifactEClass, MAVEN_ARTIFACT__MAVEN_ARTIFACT_ID);
		createEAttribute(mavenArtifactEClass, MAVEN_ARTIFACT__MAVEN_VERSION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(msCharacterizationEClass, MSCharacterization.class, "MSCharacterization", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMSCharacterization_ContainsMeasures(), this.getMeasure(), null, "containsMeasures", null, 0, -1, MSCharacterization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSCharacterization_ContainsViewpoints(), this.getViewpoint(), null, "containsViewpoints", null, 0, -1, MSCharacterization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSCharacterization_ContainsInScaleRules(), this.getInScaleRule(), null, "containsInScaleRules", null, 0, -1, MSCharacterization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSCharacterization_ContainsBasicProbes(), this.getBasicProbe(), null, "containsBasicProbes", null, 0, -1, MSCharacterization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSCharacterization_ContainsMeasureConverters(), this.getMeasureConverter(), null, "containsMeasureConverters", null, 0, -1, MSCharacterization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSCharacterization_ContainsMavenArtifacts(), this.getMavenArtifact(), null, "containsMavenArtifacts", null, 0, -1, MSCharacterization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSCharacterization_GeneratedMSProbesMavenArtifact(), this.getMavenArtifact(), null, "generatedMSProbesMavenArtifact", null, 1, 1, MSCharacterization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(viewpointEClass, Viewpoint.class, "Viewpoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getViewpoint_Name(), ecorePackage.getEString(), "name", null, 1, 1, Viewpoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getViewpoint_ViewpointContainsDimensions(), this.getDimension(), this.getDimension_DimensionIsContainedInViewpoint(), "viewpointContainsDimensions", null, 0, -1, Viewpoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getViewpoint_ProjectionProperties(), ecorePackage.getEString(), "projectionProperties", null, 0, -1, Viewpoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dimensionEClass, Dimension.class, "Dimension", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDimension_Name(), ecorePackage.getEString(), "name", null, 1, 1, Dimension.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDimension_TreeDimensionName(), ecorePackage.getEString(), "treeDimensionName", null, 0, 1, Dimension.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDimension_DimensionIsContainedInViewpoint(), this.getViewpoint(), this.getViewpoint_ViewpointContainsDimensions(), "dimensionIsContainedInViewpoint", null, 1, 1, Dimension.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDimension_DimensionContainsScaleSets(), this.getScaleSet(), this.getScaleSet_ScaleSetIsContainedInDimension(), "dimensionContainsScaleSets", null, 0, -1, Dimension.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDimension_GetScaleMethodHasParameters(), ecorePackage.getEBoolean(), "getScaleMethodHasParameters", "false", 1, 1, Dimension.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDimension_DimensionHasBasicProbe(), this.getBasicProbe(), null, "dimensionHasBasicProbe", null, 0, 1, Dimension.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(measureEClass, Measure.class, "Measure", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMeasure_Unity(), ecorePackage.getEString(), "unity", null, 1, 1, Measure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEJavaClass());
		EGenericType g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getMeasure_ValueType(), g1, "valueType", null, 0, 1, Measure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMeasure_CompareToMethodBody_THISVALUE_OTHERVALUE(), ecorePackage.getEString(), "compareToMethodBody_THISVALUE_OTHERVALUE", "THISVALUE.compareTo(OTHERVALUE)", 0, 1, Measure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMeasure_FromMeasureConverters(), this.getMeasureConverter(), this.getMeasureConverter_FromMeasure(), "fromMeasureConverters", null, 0, -1, Measure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMeasure_ToMeasureConverters(), this.getMeasureConverter(), this.getMeasureConverter_ToMeasure(), "toMeasureConverters", null, 0, -1, Measure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getMeasure__ValueTypeIsComparable__DiagnosticChain_Map(), ecorePackage.getEBoolean(), "valueTypeIsComparable", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(inScaleRuleEClass, InScaleRule.class, "InScaleRule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInScaleRule_Name(), ecorePackage.getEString(), "name", null, 1, 1, InScaleRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInScaleRule_IsInScaleCondition_VALUE_SCALE(), ecorePackage.getEString(), "isInScaleCondition_VALUE_SCALE", null, 1, 1, InScaleRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scaleSetEClass, ScaleSet.class, "ScaleSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScaleSet_ScaleSetContainsScales(), this.getScale(), this.getScale_ScaleIsContainedInScaleSet(), "scaleSetContainsScales", null, 0, -1, ScaleSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScaleSet_ScaleSetIsContainedInDimension(), this.getDimension(), this.getDimension_DimensionContainsScaleSets(), "scaleSetIsContainedInDimension", null, 1, 1, ScaleSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScaleSet_ScaleSetHasInScaleRule(), this.getInScaleRule(), null, "scaleSetHasInScaleRule", null, 1, 1, ScaleSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScaleSet_ScaleSetHasMeasure(), this.getMeasure(), null, "scaleSetHasMeasure", null, 1, 1, ScaleSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScaleSet_Name(), ecorePackage.getEString(), "name", "", 1, 1, ScaleSet.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(scaleEClass, Scale.class, "Scale", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScale_ScaleIsContainedInScaleSet(), this.getScaleSet(), this.getScaleSet_ScaleSetContainsScales(), "scaleIsContainedInScaleSet", null, 1, 1, Scale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScale_Name(), ecorePackage.getEString(), "name", null, 1, 1, Scale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScale_Min(), ecorePackage.getEString(), "min", null, 0, 1, Scale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScale_Max(), ecorePackage.getEString(), "max", null, 0, 1, Scale.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getScale__MinHasRequiredType__DiagnosticChain_Map(), ecorePackage.getEBoolean(), "minHasRequiredType", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getScale__MaxHasRequiredType__DiagnosticChain_Map(), ecorePackage.getEBoolean(), "maxHasRequiredType", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(basicProbeEClass, BasicProbe.class, "BasicProbe", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBasicProbe_MavenArtifact(), this.getMavenArtifact(), null, "mavenArtifact", null, 1, 1, BasicProbe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBasicProbe_FullyQualifiedClassName(), ecorePackage.getEString(), "fullyQualifiedClassName", null, 1, 1, BasicProbe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBasicProbe_Information(), ecorePackage.getEString(), "information", null, 1, 1, BasicProbe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBasicProbe_TreeInformation(), ecorePackage.getEString(), "treeInformation", null, 0, 1, BasicProbe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBasicProbe_BasicProbeHasMeasure(), this.getMeasure(), null, "basicProbeHasMeasure", null, 1, 1, BasicProbe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBasicProbe_Parameter(), this.getBasicProbeInformationParameter(), null, "parameter", null, 0, 1, BasicProbe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBasicProbe_ConstructorProperties(), ecorePackage.getEString(), "constructorProperties", null, 0, -1, BasicProbe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBasicProbe_Initialization_PROBE(), ecorePackage.getEString(), "initialization_PROBE", null, 0, 1, BasicProbe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(basicProbeInformationParameterEClass, BasicProbeInformationParameter.class, "BasicProbeInformationParameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getBasicProbeInformationParameter_ParameterType(), g1, "parameterType", "java.lang.String", 1, 1, BasicProbeInformationParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBasicProbeInformationParameter_ValueFromProjectionField(), ecorePackage.getEString(), "valueFromProjectionField", "id", 1, 1, BasicProbeInformationParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getBasicProbeInformationParameter__Equals__Object(), ecorePackage.getEBoolean(), "equals", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEJavaObject(), "other", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(measureConverterEClass, MeasureConverter.class, "MeasureConverter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMeasureConverter_FromMeasure(), this.getMeasure(), this.getMeasure_FromMeasureConverters(), "fromMeasure", null, 1, 1, MeasureConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMeasureConverter_ToMeasure(), this.getMeasure(), this.getMeasure_ToMeasureConverters(), "toMeasure", null, 1, 1, MeasureConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMeasureConverter_ConversionFunction_VALUE(), ecorePackage.getEString(), "conversionFunction_VALUE", null, 1, 1, MeasureConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMeasureConverter_Name(), ecorePackage.getEString(), "name", "", 1, 1, MeasureConverter.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(mavenArtifactEClass, MavenArtifact.class, "MavenArtifact", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMavenArtifact_MavenGroupId(), ecorePackage.getEString(), "mavenGroupId", null, 1, 1, MavenArtifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMavenArtifact_MavenArtifactId(), ecorePackage.getEString(), "mavenArtifactId", null, 1, 1, MavenArtifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMavenArtifact_MavenVersion(), ecorePackage.getEString(), "mavenVersion", null, 1, 1, MavenArtifact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //MSCharacterizationPackageImpl
