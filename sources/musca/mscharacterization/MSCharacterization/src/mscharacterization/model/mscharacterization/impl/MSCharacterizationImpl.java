/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization.impl;

import java.util.Collection;

import mscharacterization.model.mscharacterization.BasicProbe;
import mscharacterization.model.mscharacterization.InScaleRule;
import mscharacterization.model.mscharacterization.MSCharacterization;
import mscharacterization.model.mscharacterization.MSCharacterizationPackage;
import mscharacterization.model.mscharacterization.MavenArtifact;
import mscharacterization.model.mscharacterization.Measure;
import mscharacterization.model.mscharacterization.MeasureConverter;
import mscharacterization.model.mscharacterization.Viewpoint;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MS Characterization</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.MSCharacterizationImpl#getContainsMeasures <em>Contains Measures</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.MSCharacterizationImpl#getContainsViewpoints <em>Contains Viewpoints</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.MSCharacterizationImpl#getContainsInScaleRules <em>Contains In Scale Rules</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.MSCharacterizationImpl#getContainsBasicProbes <em>Contains Basic Probes</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.MSCharacterizationImpl#getContainsMeasureConverters <em>Contains Measure Converters</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.MSCharacterizationImpl#getContainsMavenArtifacts <em>Contains Maven Artifacts</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.impl.MSCharacterizationImpl#getGeneratedMSProbesMavenArtifact <em>Generated MS Probes Maven Artifact</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MSCharacterizationImpl extends MinimalEObjectImpl.Container implements MSCharacterization {
	/**
	 * The cached value of the '{@link #getContainsMeasures() <em>Contains Measures</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainsMeasures()
	 * @generated
	 * @ordered
	 */
	protected EList<Measure> containsMeasures;

	/**
	 * The cached value of the '{@link #getContainsViewpoints() <em>Contains Viewpoints</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainsViewpoints()
	 * @generated
	 * @ordered
	 */
	protected EList<Viewpoint> containsViewpoints;

	/**
	 * The cached value of the '{@link #getContainsInScaleRules() <em>Contains In Scale Rules</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainsInScaleRules()
	 * @generated
	 * @ordered
	 */
	protected EList<InScaleRule> containsInScaleRules;

	/**
	 * The cached value of the '{@link #getContainsBasicProbes() <em>Contains Basic Probes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainsBasicProbes()
	 * @generated
	 * @ordered
	 */
	protected EList<BasicProbe> containsBasicProbes;

	/**
	 * The cached value of the '{@link #getContainsMeasureConverters() <em>Contains Measure Converters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainsMeasureConverters()
	 * @generated
	 * @ordered
	 */
	protected EList<MeasureConverter> containsMeasureConverters;

	/**
	 * The cached value of the '{@link #getContainsMavenArtifacts() <em>Contains Maven Artifacts</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainsMavenArtifacts()
	 * @generated
	 * @ordered
	 */
	protected EList<MavenArtifact> containsMavenArtifacts;

	/**
	 * The cached value of the '{@link #getGeneratedMSProbesMavenArtifact() <em>Generated MS Probes Maven Artifact</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGeneratedMSProbesMavenArtifact()
	 * @generated
	 * @ordered
	 */
	protected MavenArtifact generatedMSProbesMavenArtifact;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MSCharacterizationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MSCharacterizationPackage.Literals.MS_CHARACTERIZATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Measure> getContainsMeasures() {
		if (containsMeasures == null) {
			containsMeasures = new EObjectContainmentEList<Measure>(Measure.class, this, MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MEASURES);
		}
		return containsMeasures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Viewpoint> getContainsViewpoints() {
		if (containsViewpoints == null) {
			containsViewpoints = new EObjectContainmentEList<Viewpoint>(Viewpoint.class, this, MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_VIEWPOINTS);
		}
		return containsViewpoints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InScaleRule> getContainsInScaleRules() {
		if (containsInScaleRules == null) {
			containsInScaleRules = new EObjectContainmentEList<InScaleRule>(InScaleRule.class, this, MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_IN_SCALE_RULES);
		}
		return containsInScaleRules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BasicProbe> getContainsBasicProbes() {
		if (containsBasicProbes == null) {
			containsBasicProbes = new EObjectContainmentEList<BasicProbe>(BasicProbe.class, this, MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_BASIC_PROBES);
		}
		return containsBasicProbes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MeasureConverter> getContainsMeasureConverters() {
		if (containsMeasureConverters == null) {
			containsMeasureConverters = new EObjectContainmentEList<MeasureConverter>(MeasureConverter.class, this, MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MEASURE_CONVERTERS);
		}
		return containsMeasureConverters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MavenArtifact> getContainsMavenArtifacts() {
		if (containsMavenArtifacts == null) {
			containsMavenArtifacts = new EObjectContainmentEList<MavenArtifact>(MavenArtifact.class, this, MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MAVEN_ARTIFACTS);
		}
		return containsMavenArtifacts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MavenArtifact getGeneratedMSProbesMavenArtifact() {
		if (generatedMSProbesMavenArtifact != null && generatedMSProbesMavenArtifact.eIsProxy()) {
			InternalEObject oldGeneratedMSProbesMavenArtifact = (InternalEObject)generatedMSProbesMavenArtifact;
			generatedMSProbesMavenArtifact = (MavenArtifact)eResolveProxy(oldGeneratedMSProbesMavenArtifact);
			if (generatedMSProbesMavenArtifact != oldGeneratedMSProbesMavenArtifact) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MSCharacterizationPackage.MS_CHARACTERIZATION__GENERATED_MS_PROBES_MAVEN_ARTIFACT, oldGeneratedMSProbesMavenArtifact, generatedMSProbesMavenArtifact));
			}
		}
		return generatedMSProbesMavenArtifact;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MavenArtifact basicGetGeneratedMSProbesMavenArtifact() {
		return generatedMSProbesMavenArtifact;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGeneratedMSProbesMavenArtifact(MavenArtifact newGeneratedMSProbesMavenArtifact) {
		MavenArtifact oldGeneratedMSProbesMavenArtifact = generatedMSProbesMavenArtifact;
		generatedMSProbesMavenArtifact = newGeneratedMSProbesMavenArtifact;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MSCharacterizationPackage.MS_CHARACTERIZATION__GENERATED_MS_PROBES_MAVEN_ARTIFACT, oldGeneratedMSProbesMavenArtifact, generatedMSProbesMavenArtifact));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MEASURES:
				return ((InternalEList<?>)getContainsMeasures()).basicRemove(otherEnd, msgs);
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_VIEWPOINTS:
				return ((InternalEList<?>)getContainsViewpoints()).basicRemove(otherEnd, msgs);
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_IN_SCALE_RULES:
				return ((InternalEList<?>)getContainsInScaleRules()).basicRemove(otherEnd, msgs);
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_BASIC_PROBES:
				return ((InternalEList<?>)getContainsBasicProbes()).basicRemove(otherEnd, msgs);
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MEASURE_CONVERTERS:
				return ((InternalEList<?>)getContainsMeasureConverters()).basicRemove(otherEnd, msgs);
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MAVEN_ARTIFACTS:
				return ((InternalEList<?>)getContainsMavenArtifacts()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MEASURES:
				return getContainsMeasures();
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_VIEWPOINTS:
				return getContainsViewpoints();
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_IN_SCALE_RULES:
				return getContainsInScaleRules();
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_BASIC_PROBES:
				return getContainsBasicProbes();
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MEASURE_CONVERTERS:
				return getContainsMeasureConverters();
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MAVEN_ARTIFACTS:
				return getContainsMavenArtifacts();
			case MSCharacterizationPackage.MS_CHARACTERIZATION__GENERATED_MS_PROBES_MAVEN_ARTIFACT:
				if (resolve) return getGeneratedMSProbesMavenArtifact();
				return basicGetGeneratedMSProbesMavenArtifact();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MEASURES:
				getContainsMeasures().clear();
				getContainsMeasures().addAll((Collection<? extends Measure>)newValue);
				return;
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_VIEWPOINTS:
				getContainsViewpoints().clear();
				getContainsViewpoints().addAll((Collection<? extends Viewpoint>)newValue);
				return;
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_IN_SCALE_RULES:
				getContainsInScaleRules().clear();
				getContainsInScaleRules().addAll((Collection<? extends InScaleRule>)newValue);
				return;
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_BASIC_PROBES:
				getContainsBasicProbes().clear();
				getContainsBasicProbes().addAll((Collection<? extends BasicProbe>)newValue);
				return;
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MEASURE_CONVERTERS:
				getContainsMeasureConverters().clear();
				getContainsMeasureConverters().addAll((Collection<? extends MeasureConverter>)newValue);
				return;
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MAVEN_ARTIFACTS:
				getContainsMavenArtifacts().clear();
				getContainsMavenArtifacts().addAll((Collection<? extends MavenArtifact>)newValue);
				return;
			case MSCharacterizationPackage.MS_CHARACTERIZATION__GENERATED_MS_PROBES_MAVEN_ARTIFACT:
				setGeneratedMSProbesMavenArtifact((MavenArtifact)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MEASURES:
				getContainsMeasures().clear();
				return;
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_VIEWPOINTS:
				getContainsViewpoints().clear();
				return;
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_IN_SCALE_RULES:
				getContainsInScaleRules().clear();
				return;
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_BASIC_PROBES:
				getContainsBasicProbes().clear();
				return;
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MEASURE_CONVERTERS:
				getContainsMeasureConverters().clear();
				return;
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MAVEN_ARTIFACTS:
				getContainsMavenArtifacts().clear();
				return;
			case MSCharacterizationPackage.MS_CHARACTERIZATION__GENERATED_MS_PROBES_MAVEN_ARTIFACT:
				setGeneratedMSProbesMavenArtifact((MavenArtifact)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MEASURES:
				return containsMeasures != null && !containsMeasures.isEmpty();
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_VIEWPOINTS:
				return containsViewpoints != null && !containsViewpoints.isEmpty();
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_IN_SCALE_RULES:
				return containsInScaleRules != null && !containsInScaleRules.isEmpty();
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_BASIC_PROBES:
				return containsBasicProbes != null && !containsBasicProbes.isEmpty();
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MEASURE_CONVERTERS:
				return containsMeasureConverters != null && !containsMeasureConverters.isEmpty();
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MAVEN_ARTIFACTS:
				return containsMavenArtifacts != null && !containsMavenArtifacts.isEmpty();
			case MSCharacterizationPackage.MS_CHARACTERIZATION__GENERATED_MS_PROBES_MAVEN_ARTIFACT:
				return generatedMSProbesMavenArtifact != null;
		}
		return super.eIsSet(featureID);
	}

} //MSCharacterizationImpl
