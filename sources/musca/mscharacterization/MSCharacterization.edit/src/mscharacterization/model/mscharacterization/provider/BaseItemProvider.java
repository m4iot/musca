/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization.provider;

import java.util.ArrayList;
import java.util.Collection;

import mscharacterization.model.mscharacterization.MSCharacterization;
import mscharacterization.model.mscharacterization.MSCharacterizationPackage;
import mscharacterization.model.mscharacterization.ScaleSet;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;

public class BaseItemProvider extends ItemProviderAdapter {

	public BaseItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	
	// Surchargé pour personnaliser la méthode getChoiceOfValues de ItemPropertyDescriptor
	@Override
	protected ItemPropertyDescriptor createItemPropertyDescriptor(
			AdapterFactory adapterFactory, ResourceLocator resourceLocator,
			String displayName, String description, EStructuralFeature feature,
			boolean isSettable, boolean multiLine, boolean sortChoices,
			Object staticImage, String category, String[] filterFlags) {
		
		return new ItemPropertyDescriptor(adapterFactory, resourceLocator,
				displayName, description, feature, isSettable, multiLine, sortChoices,
				staticImage, category, filterFlags) {

					@Override
					public Collection<?> getChoiceOfValues(Object object) {
							Collection<?> values = super.getChoiceOfValues(object);
							
							Collection<EObject> result = new ArrayList<EObject>();
							
							EObject currentEObject = (EObject) object;
							
							// Feature à comparer pour savoir si l'eObject est déjà présent en local
							EStructuralFeature idFeature;
							switch (feature.getEType().getClassifierID()) {
							case MSCharacterizationPackage.VIEWPOINT:
								idFeature = MSCharacterizationPackage.Literals.VIEWPOINT__NAME;
								break;
							case MSCharacterizationPackage.MEASURE:
								idFeature = MSCharacterizationPackage.Literals.MEASURE__UNITY;
								break;
							case MSCharacterizationPackage.IN_SCALE_RULE:
								idFeature = MSCharacterizationPackage.Literals.IN_SCALE_RULE__NAME;
								break;
							case MSCharacterizationPackage.DIMENSION:
								idFeature = MSCharacterizationPackage.Literals.DIMENSION__NAME;
								break;
							case MSCharacterizationPackage.SCALE_SET:
								idFeature = MSCharacterizationPackage.Literals.SCALE_SET__NAME;
								break;

							default:
								return values;
							}
	
							boolean localExists = false;
	
							for (Object value : values) {
								if (value != null) {
									EObject eObject = (EObject) value;
									if (eObject.eResource() == currentEObject.eResource()) {
										// eObect local
										result.add(eObject);
									} else {
										// eObject global
										localExists = false;
										for (Object value2 : values) {
											if (value2 != null) {
												EObject eObject2 = (EObject) value2;
												System.out.println("eObject2 : " + eObject2.eGet(idFeature) + ", eObject : " + eObject.eGet(idFeature));
												if (eObject2.eResource() == currentEObject.eResource()
														&& eObject2.eGet(idFeature).equals(eObject.eGet(idFeature))) {
													// il existe un eObject2 local de même nom que le eObject global
													localExists = true;
													break;
												}
											}
										}
										if (!localExists) {
											// Si l'eObject est contenu dans l'objet racine on ne le copie pas (sauf si la feature selectionnée est la racine)
											if (!(feature.getEContainingClass().equals(MSCharacterizationPackage.Literals.MS_CHARACTERIZATION))
													&& eObject.eContainer() instanceof MSCharacterization) {
												result.add(eObject);
											} else {
												EObject copy = EcoreUtil.copy(eObject);
												if (copy instanceof ScaleSet) {
													((ScaleSet) copy).setScaleSetHasInScaleRule(EcoreUtil.copy(((ScaleSet) eObject).getScaleSetHasInScaleRule()));
												} else {
													for (EReference eReference : copy.eClass().getEAllReferences()) {
														copy.eUnset(eReference);
													}
												}
												result.add(copy);
											}
										}
									}
								}
							}
							return result;
					}
		};
	}
	
}
