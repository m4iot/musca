/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization.provider;


import java.util.Collection;
import java.util.List;

import mscharacterization.model.mscharacterization.MSCharacterization;
import mscharacterization.model.mscharacterization.MSCharacterizationFactory;
import mscharacterization.model.mscharacterization.MSCharacterizationPackage;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link mscharacterization.model.mscharacterization.MSCharacterization} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MSCharacterizationItemProvider
	extends BaseItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSCharacterizationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addContainsMeasuresPropertyDescriptor(object);
			addContainsViewpointsPropertyDescriptor(object);
			addContainsInScaleRulesPropertyDescriptor(object);
			addGeneratedMSProbesMavenArtifactPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Contains Measures feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainsMeasuresPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MSCharacterization_containsMeasures_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MSCharacterization_containsMeasures_feature", "_UI_MSCharacterization_type"),
				 MSCharacterizationPackage.Literals.MS_CHARACTERIZATION__CONTAINS_MEASURES,
				 true,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Contains Viewpoints feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainsViewpointsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MSCharacterization_containsViewpoints_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MSCharacterization_containsViewpoints_feature", "_UI_MSCharacterization_type"),
				 MSCharacterizationPackage.Literals.MS_CHARACTERIZATION__CONTAINS_VIEWPOINTS,
				 true,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Contains In Scale Rules feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainsInScaleRulesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MSCharacterization_containsInScaleRules_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MSCharacterization_containsInScaleRules_feature", "_UI_MSCharacterization_type"),
				 MSCharacterizationPackage.Literals.MS_CHARACTERIZATION__CONTAINS_IN_SCALE_RULES,
				 true,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Generated MS Probes Maven Artifact feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addGeneratedMSProbesMavenArtifactPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MSCharacterization_generatedMSProbesMavenArtifact_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MSCharacterization_generatedMSProbesMavenArtifact_feature", "_UI_MSCharacterization_type"),
				 MSCharacterizationPackage.Literals.MS_CHARACTERIZATION__GENERATED_MS_PROBES_MAVEN_ARTIFACT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(MSCharacterizationPackage.Literals.MS_CHARACTERIZATION__CONTAINS_MEASURES);
			childrenFeatures.add(MSCharacterizationPackage.Literals.MS_CHARACTERIZATION__CONTAINS_VIEWPOINTS);
			childrenFeatures.add(MSCharacterizationPackage.Literals.MS_CHARACTERIZATION__CONTAINS_IN_SCALE_RULES);
			childrenFeatures.add(MSCharacterizationPackage.Literals.MS_CHARACTERIZATION__CONTAINS_BASIC_PROBES);
			childrenFeatures.add(MSCharacterizationPackage.Literals.MS_CHARACTERIZATION__CONTAINS_MEASURE_CONVERTERS);
			childrenFeatures.add(MSCharacterizationPackage.Literals.MS_CHARACTERIZATION__CONTAINS_MAVEN_ARTIFACTS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MSCharacterization.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MSCharacterization"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_MSCharacterization_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MSCharacterization.class)) {
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MEASURES:
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_VIEWPOINTS:
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_IN_SCALE_RULES:
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_BASIC_PROBES:
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MEASURE_CONVERTERS:
			case MSCharacterizationPackage.MS_CHARACTERIZATION__CONTAINS_MAVEN_ARTIFACTS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(MSCharacterizationPackage.Literals.MS_CHARACTERIZATION__CONTAINS_MEASURES,
				 MSCharacterizationFactory.eINSTANCE.createMeasure()));

		newChildDescriptors.add
			(createChildParameter
				(MSCharacterizationPackage.Literals.MS_CHARACTERIZATION__CONTAINS_VIEWPOINTS,
				 MSCharacterizationFactory.eINSTANCE.createViewpoint()));

		newChildDescriptors.add
			(createChildParameter
				(MSCharacterizationPackage.Literals.MS_CHARACTERIZATION__CONTAINS_IN_SCALE_RULES,
				 MSCharacterizationFactory.eINSTANCE.createInScaleRule()));

		newChildDescriptors.add
			(createChildParameter
				(MSCharacterizationPackage.Literals.MS_CHARACTERIZATION__CONTAINS_BASIC_PROBES,
				 MSCharacterizationFactory.eINSTANCE.createBasicProbe()));

		newChildDescriptors.add
			(createChildParameter
				(MSCharacterizationPackage.Literals.MS_CHARACTERIZATION__CONTAINS_MEASURE_CONVERTERS,
				 MSCharacterizationFactory.eINSTANCE.createMeasureConverter()));

		newChildDescriptors.add
			(createChildParameter
				(MSCharacterizationPackage.Literals.MS_CHARACTERIZATION__CONTAINS_MAVEN_ARTIFACTS,
				 MSCharacterizationFactory.eINSTANCE.createMavenArtifact()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MSCharacterizationEditPlugin.INSTANCE;
	}

//	
//	// Surchargé pour personnaliser la méthode getChoiceOfValues de ItemPropertyDescriptor
//	@Override
//	protected ItemPropertyDescriptor createItemPropertyDescriptor(
//			AdapterFactory adapterFactory, ResourceLocator resourceLocator,
//			String displayName, String description, EStructuralFeature feature,
//			boolean isSettable, boolean multiLine, boolean sortChoices,
//			Object staticImage, String category, String[] filterFlags) {
//		
//		return new ItemPropertyDescriptor(adapterFactory, resourceLocator,
//				displayName, description, feature, isSettable, multiLine, sortChoices,
//				staticImage, category, filterFlags) {
//
//					@Override
//					public Collection<?> getChoiceOfValues(Object object) {
//							Collection<?> values = super.getChoiceOfValues(object);
//							Collection<EObject> result = new ArrayList<EObject>();
//							
//							MSCharacterization mschar = (MSCharacterization) object;
//							
//							// Feature à comparer pour savoir si l'eObject est déjà présent en local
//							EStructuralFeature idFeature;
//							switch (feature.getEType().getClassifierID()) {
//							case MSCharacterizationPackage.VIEWPOINT:
//								idFeature = MSCharacterizationPackage.Literals.VIEWPOINT__NAME;
//								break;
//							case MSCharacterizationPackage.MEASURE:
//								idFeature = MSCharacterizationPackage.Literals.MEASURE__UNITY;
//								break;
//							case MSCharacterizationPackage.IN_SCALE_RULE:
//								idFeature = MSCharacterizationPackage.Literals.VIEWPOINT__NAME;
//								break;
//
//							default:
//								return values;
//							}
//	
//							boolean localExists = false;
//	
//							for (Object value : values) {
//								EObject eObject = (EObject) value;
//								if (eObject.eResource() == mschar.eResource()) {
//									// eObect local
//									result.add(eObject);
//								} else {
//									// eObject global
//									localExists = false;
//									for (Object value2 : values) {
//										EObject eObject2 = (EObject) value2;
//										if (eObject2.eResource() == mschar.eResource()
//												&& eObject2.eGet(idFeature).equals(eObject.eGet(idFeature))) {
//											// il existe un viewpoint2 local de même nom que le viewpoint global
//											localExists = true;
//											break;
//										}
//									}
//									if (!localExists) {
//										EObject copy = EcoreUtil.copy(eObject);
//										for (EReference eReference : copy.eClass().getEAllReferences()) {
//											copy.eUnset(eReference);
//										}
//										result.add(copy);
//									}
//								}
//							}
//							return result;
//					}
//		};
//	}
}
