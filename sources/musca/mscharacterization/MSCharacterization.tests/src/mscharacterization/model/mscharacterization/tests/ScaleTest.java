/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import mscharacterization.model.mscharacterization.MSCharacterizationFactory;
import mscharacterization.model.mscharacterization.Scale;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Scale</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link mscharacterization.model.mscharacterization.Scale#minHasRequiredType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Min Has Required Type</em>}</li>
 *   <li>{@link mscharacterization.model.mscharacterization.Scale#maxHasRequiredType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Max Has Required Type</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class ScaleTest extends TestCase {

	/**
	 * The fixture for this Scale test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Scale fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ScaleTest.class);
	}

	/**
	 * Constructs a new Scale test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScaleTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Scale test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Scale fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Scale test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Scale getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(MSCharacterizationFactory.eINSTANCE.createScale());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link mscharacterization.model.mscharacterization.Scale#minHasRequiredType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Min Has Required Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mscharacterization.model.mscharacterization.Scale#minHasRequiredType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testMinHasRequiredType__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link mscharacterization.model.mscharacterization.Scale#maxHasRequiredType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Max Has Required Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mscharacterization.model.mscharacterization.Scale#maxHasRequiredType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testMaxHasRequiredType__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //ScaleTest
