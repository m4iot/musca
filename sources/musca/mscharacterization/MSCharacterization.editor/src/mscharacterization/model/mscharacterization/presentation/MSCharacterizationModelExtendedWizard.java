/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package mscharacterization.model.mscharacterization.presentation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mscharacterization.model.mscharacterization.Dimension;
import mscharacterization.model.mscharacterization.MSCharacterization;
import mscharacterization.model.mscharacterization.MSCharacterizationPackage;
import mscharacterization.model.mscharacterization.ScaleSet;
import mscharacterization.model.mscharacterization.Viewpoint;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;

public class MSCharacterizationModelExtendedWizard extends
MSCharacterizationModelWizard {
	
	private static final String MS_CHARACTERIZATION_TAXONOMY_PATH = "MSTaxonomy/taxonomy.mscharacterization";
	
	private MSCharacterization msCharacterization;
	private MSCharacterizationModelExtendedWizardViewpointsSelectionPage viewpointsSelectionPage;
	private MSCharacterizationModelExtendedWizardDimensionsSelectionPage dimensionsSelectionPage;
	private MSCharacterizationModelExtendedWizardScaleSetsSelectionPage scaleSetsSelectionPage;
	
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		super.init(workbench, selection);
		ResourceSet resourceSet = new ResourceSetImpl();
		URI fileURI = URI.createPlatformResourceURI(MS_CHARACTERIZATION_TAXONOMY_PATH, true);
		Resource resource = resourceSet.getResource(fileURI, true);
		msCharacterization = (MSCharacterization) resource.getContents().get(0);		
	}

	@Override
	public void createPageControls(Composite pageContainer) {
		// Empty to create pages dynamically
	}
	
	public class MSCharacterizationModelExtendedWizardViewpointsSelectionPage extends WizardPage {

		private CheckboxTableViewer checkboxTableViewer;

		protected MSCharacterizationModelExtendedWizardViewpointsSelectionPage(
				String pageName) {
			super(pageName);
			setTitle("Viewpoint Selection");
			setDescription("Select the viewpoints");
		}

		public void createControl(Composite parent) {
			Composite composite = new Composite(parent, SWT.NONE); {
				GridLayout layout = new GridLayout();
				layout.numColumns = 1;
				layout.verticalSpacing = 12;
				composite.setLayout(layout);

				GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
				composite.setLayoutData(data);
			}
			
			Label label = new Label(composite, SWT.NONE);
			label.setText("Select desired viewpoints");
			label.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
			
			checkboxTableViewer = CheckboxTableViewer.newCheckList(composite, SWT.FULL_SELECTION|SWT.BORDER);
			checkboxTableViewer.getTable().setLinesVisible(true);
			checkboxTableViewer.setContentProvider(new ArrayContentProvider());
			checkboxTableViewer.setLabelProvider(new ITableLabelProvider() {
				
				@Override
				public void removeListener(ILabelProviderListener listener) {
					
				}
				
				@Override
				public boolean isLabelProperty(Object element, String property) {
					return false;
				}
				
				@Override
				public void dispose() {
					
				}
				
				@Override
				public void addListener(ILabelProviderListener listener) {
					
				}
				
				@Override
				public String getColumnText(Object element, int columnIndex) {
					return ((Viewpoint) element).getName();
				}
				
				@Override
				public Image getColumnImage(Object element, int columnIndex) {
					return null;
				}
			});
			
			EList<Viewpoint> viewpoints = msCharacterization.getContainsViewpoints();			
			checkboxTableViewer.setInput(viewpoints);
			
			checkboxTableViewer.addCheckStateListener(new ICheckStateListener() {
				
				@Override
				public void checkStateChanged(CheckStateChangedEvent event) {
					if (checkboxTableViewer.getCheckedElements().length > 0) {
						setPageComplete(true);;
					} else {
						setPageComplete(false);
					}
					
				}
			});

			setPageComplete(false);
			setControl(composite);
		}
		
		public List<Viewpoint> getSelectedViewpoints() {
			ArrayList<Viewpoint> selectedViewpoints = new ArrayList<Viewpoint>();
			for (Object object : checkboxTableViewer.getCheckedElements()) {
				selectedViewpoints.add((Viewpoint) object);
			}
			return selectedViewpoints;
		}

		@Override
		public IWizardPage getPreviousPage() {
			return null;
		}

	}
	
	public class MSCharacterizationModelExtendedWizardDimensionsSelectionPage extends WizardPage {

		private HashMap<String, CheckboxTableViewer> dimensionsCheckboxTableViewers;

		protected MSCharacterizationModelExtendedWizardDimensionsSelectionPage(
				String pageName) {
			super(pageName);
			setTitle("Dimension Selection");
			setDescription("Select the dimensions for each viewpoint");
		}

		public void createControl(Composite parent) {
			Composite composite = new Composite(parent, SWT.NONE); {
				GridLayout layout = new GridLayout();
				layout.numColumns = 1;
				layout.verticalSpacing = 12;
				composite.setLayout(layout);

				GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
				composite.setLayoutData(data);
			}
			dimensionsCheckboxTableViewers = new HashMap<String, CheckboxTableViewer>();
			
			List<Viewpoint> selectedViewpoints = viewpointsSelectionPage.getSelectedViewpoints();
			
			for (Viewpoint viewpoint : selectedViewpoints) {
				if (!viewpoint.getViewpointContainsDimensions().isEmpty()) {
					Label label = new Label(composite, SWT.NONE);
					label.setText("Viewpoint " + viewpoint.getName() + " : select desired dimensions");
					label.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
					
					CheckboxTableViewer checkboxTableViewer = CheckboxTableViewer.newCheckList(composite, SWT.FULL_SELECTION|SWT.BORDER);
					checkboxTableViewer.getTable().setLinesVisible(true);
					checkboxTableViewer.setContentProvider(new ArrayContentProvider());
					checkboxTableViewer.setLabelProvider(new ITableLabelProvider() {
						
						@Override
						public void removeListener(ILabelProviderListener listener) {
							
						}
						
						@Override
						public boolean isLabelProperty(Object element, String property) {
							return false;
						}
						
						@Override
						public void dispose() {
							
						}
						
						@Override
						public void addListener(ILabelProviderListener listener) {
							
						}
						
						@Override
						public String getColumnText(Object element, int columnIndex) {
							return ((Dimension) element).getName();
						}
						
						@Override
						public Image getColumnImage(Object element, int columnIndex) {
							return null;
						}
					});
					
					EList<Dimension> dimensions = viewpoint.getViewpointContainsDimensions();
					checkboxTableViewer.setInput(dimensions);
					dimensionsCheckboxTableViewers.put(viewpoint.getName(), checkboxTableViewer);
					
					checkboxTableViewer.addCheckStateListener(new ICheckStateListener() {
						
						@Override
						public void checkStateChanged(CheckStateChangedEvent event) {
							boolean pageComplete = false;
							for (CheckboxTableViewer checkboxTableViewer : dimensionsCheckboxTableViewers.values()) {
								if (checkboxTableViewer.getCheckedElements().length > 0) {
									pageComplete = true;
									break;
								}
							}
							setPageComplete(pageComplete);
						}
					});
				}
			}

			setPageComplete(false);
			setControl(composite);
		}

		@Override
		public IWizardPage getPreviousPage() {
			return null;
		}
		
		public HashMap<String, List<Dimension>> getSelectedDimensionsPerViewpoint() {
			HashMap<String, List<Dimension>> selectedDimensionsPerViewpoint = new HashMap<String, List<Dimension>>();
			for (String viewpointName : dimensionsCheckboxTableViewers.keySet()) {
				List<Dimension> selectedDimensions = new ArrayList<Dimension>();
				for (Object object : dimensionsCheckboxTableViewers.get(viewpointName).getCheckedElements()) {
					selectedDimensions.add((Dimension) object);
				}
				selectedDimensionsPerViewpoint.put(viewpointName, selectedDimensions);
			}
			return selectedDimensionsPerViewpoint;
		}

	}
	
	public class MSCharacterizationModelExtendedWizardScaleSetsSelectionPage extends WizardPage {

		private HashMap<String, HashMap<String,CheckboxTableViewer>> scaleSetsCheckboxTableViewers;

		protected MSCharacterizationModelExtendedWizardScaleSetsSelectionPage(
				String pageName) {
			super(pageName);
			setTitle("ScaleSet Selection");
			setDescription("Select the scale sets for each dimension of each viewpoint");
		}

		public void createControl(Composite parent) {
			final Composite rootComposite = new Composite(parent, SWT.NONE);
			rootComposite.setLayout(GridLayoutFactory.fillDefaults().create());

			final ScrolledComposite sc = new ScrolledComposite(rootComposite, SWT.BORDER | SWT.V_SCROLL);
			sc.setLayoutData(GridDataFactory.fillDefaults().grab(true, true).hint(SWT.DEFAULT, 200).create());
			sc.setExpandHorizontal(true);
			sc.setExpandVertical(true);
			
			Composite composite = new Composite(sc, SWT.NONE); {
				GridLayout layout = new GridLayout();
				layout.numColumns = 1;
				layout.verticalSpacing = 12;
				composite.setLayout(layout);

				GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
				composite.setLayoutData(data);
			}

			sc.setContent(composite);
			
			scaleSetsCheckboxTableViewers = new HashMap<String, HashMap<String,CheckboxTableViewer>>();

			HashMap<String, List<Dimension>> selectedDimensionsPerViewpoint = dimensionsSelectionPage.getSelectedDimensionsPerViewpoint();
			
			for (String viewpointName : selectedDimensionsPerViewpoint.keySet()) {
				Label label = new Label(composite, SWT.NONE);
				label.setText("Viewpoint " + viewpointName + " :");
				label.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
				
				HashMap<String,CheckboxTableViewer> scaleSetsCheckboxTableViewersPerDimension = new HashMap<String, CheckboxTableViewer>();
				
				for (Dimension dimension : selectedDimensionsPerViewpoint.get(viewpointName)) {
					if (!dimension.getDimensionContainsScaleSets().isEmpty()) {
						Label label1 = new Label(composite, SWT.NONE);
						label1.setText("Dimension " + dimension.getName() + " : select desired scale sets");
						label1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
					
						CheckboxTableViewer checkboxTableViewer = CheckboxTableViewer.newCheckList(composite, SWT.FULL_SELECTION|SWT.BORDER);
						checkboxTableViewer.getTable().setLinesVisible(true);
						checkboxTableViewer.setContentProvider(new ArrayContentProvider());
						checkboxTableViewer.setLabelProvider(new ITableLabelProvider() {
							
							@Override
							public void removeListener(ILabelProviderListener listener) {
								
							}
							
							@Override
							public boolean isLabelProperty(Object element, String property) {
								return false;
							}
							
							@Override
							public void dispose() {
								
							}
							
							@Override
							public void addListener(ILabelProviderListener listener) {
								
							}
							
							@Override
							public String getColumnText(Object element, int columnIndex) {
								return ((ScaleSet) element).getName();
							}
							
							@Override
							public Image getColumnImage(Object element, int columnIndex) {
								return null;
							}
						});
						
						EList<ScaleSet> scaleSets = dimension.getDimensionContainsScaleSets();
						checkboxTableViewer.setInput(scaleSets);
						scaleSetsCheckboxTableViewersPerDimension.put(dimension.getName(), checkboxTableViewer);
						
//						checkboxTableViewer.addCheckStateListener(new ICheckStateListener() {
//							
//							@Override
//							public void checkStateChanged(CheckStateChangedEvent event) {
//								boolean pageComplete = true;
//								for (HashMap<String, CheckboxTableViewer> hashMap : scaleSetsCheckboxTableViewers.values()) {
//									for (CheckboxTableViewer checkboxTableViewer : hashMap.values()) {
//										if (checkboxTableViewer.getCheckedElements().length == 0) {
//											pageComplete = false;
//										}
//									}
//								}
//								setPageComplete(pageComplete);
//							}
//						});
					}
				}
				scaleSetsCheckboxTableViewers.put(viewpointName, scaleSetsCheckboxTableViewersPerDimension);
			}

//			setPageComplete(false);
			setPageComplete(true);

			sc.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
			setControl(rootComposite);
		}

		@Override
		public IWizardPage getPreviousPage() {
			return null;
		}
		
		public HashMap<String, HashMap<String, List<ScaleSet>>> getSelectedScaleSetsPerDimensionPerViewpoint() {
			HashMap<String, HashMap<String, List<ScaleSet>>> selectedScaleSetsPerDimensionPerViewpoint = new HashMap<String, HashMap<String, List<ScaleSet>>>();
			for (String viewpointName : scaleSetsCheckboxTableViewers.keySet()) {
				HashMap<String, CheckboxTableViewer> scaleSetsCheckboxTableViewersPerDimension = scaleSetsCheckboxTableViewers.get(viewpointName);
				HashMap<String, List<ScaleSet>> selectedScaleSetsPerDimension = new HashMap<String, List<ScaleSet>>();
				for (String dimensionName : scaleSetsCheckboxTableViewersPerDimension.keySet()) {
					ArrayList<ScaleSet> selectedScaleSets = new ArrayList<ScaleSet>();
					for (Object object : scaleSetsCheckboxTableViewersPerDimension.get(dimensionName).getCheckedElements()) {
						selectedScaleSets.add((ScaleSet) object);
					}
					selectedScaleSetsPerDimension.put(dimensionName, selectedScaleSets);
				}
				selectedScaleSetsPerDimensionPerViewpoint.put(viewpointName, selectedScaleSetsPerDimension);
			}
			return selectedScaleSetsPerDimensionPerViewpoint;
		}

	}
	
	@Override
	public void addPages() {
		// Do not call super method because we want to hide the second page
		//super.addPages();
		
		// Create a page, set the title, and the initial model file name.
		//
		newFileCreationPage = new MSCharacterizationModelWizardNewFileCreationPage("Whatever", selection);
		newFileCreationPage.setTitle(MSCharacterizationEditorPlugin.INSTANCE.getString("_UI_MSCharacterizationModelWizard_label"));
		newFileCreationPage.setDescription(MSCharacterizationEditorPlugin.INSTANCE.getString("_UI_MSCharacterizationModelWizard_description"));
		newFileCreationPage.setFileName(MSCharacterizationEditorPlugin.INSTANCE.getString("_UI_MSCharacterizationEditorFilenameDefaultBase") + "." + FILE_EXTENSIONS.get(0));
		addPage(newFileCreationPage);

		// Try and get the resource selection to determine a current directory for the file dialog.
		//
		if (selection != null && !selection.isEmpty()) {
			// Get the resource...
			//
			Object selectedElement = selection.iterator().next();
			if (selectedElement instanceof IResource) {
				// Get the resource parent, if its a file.
				//
				IResource selectedResource = (IResource)selectedElement;
				if (selectedResource.getType() == IResource.FILE) {
					selectedResource = selectedResource.getParent();
				}

				// This gives us a directory...
				//
				if (selectedResource instanceof IFolder || selectedResource instanceof IProject) {
					// Set this for the container.
					//
					newFileCreationPage.setContainerFullPath(selectedResource.getFullPath());

					// Make up a unique new name here.
					//
					String defaultModelBaseFilename = MSCharacterizationEditorPlugin.INSTANCE.getString("_UI_MSCharacterizationEditorFilenameDefaultBase");
					String defaultModelFilenameExtension = FILE_EXTENSIONS.get(0);
					String modelFilename = defaultModelBaseFilename + "." + defaultModelFilenameExtension;
					for (int i = 1; ((IContainer)selectedResource).findMember(modelFilename) != null; ++i) {
						modelFilename = defaultModelBaseFilename + i + "." + defaultModelFilenameExtension;
					}
					newFileCreationPage.setFileName(modelFilename);
				}
			}
		}
		// We create an anonymous subclass to override the methods used in performFinish
		initialObjectCreationPage = new MSCharacterizationModelWizardInitialObjectCreationPage("Whatever2") {

			@Override
			public String getInitialObjectName() {
				return msCharacterizationPackage.getMSCharacterization().getName();
			}

			@Override
			public String getEncoding() {
				return "UTF-8";
			}
			
		};
		// Do not add the second page to the wizard pages
//		initialObjectCreationPage.setTitle(MSCharacterizationEditorPlugin.INSTANCE.getString("_UI_MSCharacterizationModelWizard_label"));
//		initialObjectCreationPage.setDescription(MSCharacterizationEditorPlugin.INSTANCE.getString("_UI_Wizard_initial_object_description"));
//		addPage(initialObjectCreationPage);
		
		viewpointsSelectionPage =
				new MSCharacterizationModelExtendedWizardViewpointsSelectionPage("Viewpoints");
		addPage(viewpointsSelectionPage);
		dimensionsSelectionPage =
				new MSCharacterizationModelExtendedWizardDimensionsSelectionPage("Dimensions");
		addPage(dimensionsSelectionPage);
		scaleSetsSelectionPage =
				new MSCharacterizationModelExtendedWizardScaleSetsSelectionPage("Scale Sets");
		addPage(scaleSetsSelectionPage);
	}

	@Override
	protected EObject createInitialModel() {
		MSCharacterization rootMSCharacterization = (MSCharacterization) super.createInitialModel();
		for (Viewpoint viewpoint : viewpointsSelectionPage.getSelectedViewpoints()) {
			Viewpoint viewpointCopy = EcoreUtil.copy(viewpoint);

			// Unset the viewpoint's dimensions, keep other references
			viewpointCopy.eUnset(MSCharacterizationPackage.Literals.VIEWPOINT__VIEWPOINT_CONTAINS_DIMENSIONS);

			if (dimensionsSelectionPage.getSelectedDimensionsPerViewpoint().get(viewpoint.getName()) != null) {
				for (Dimension dimension : dimensionsSelectionPage.getSelectedDimensionsPerViewpoint().get(viewpoint.getName())) {
					Dimension dimensionCopy = EcoreUtil.copy(dimension);

					// Unset the dimension's scale sets, keep other references
					dimensionCopy.eUnset(MSCharacterizationPackage.Literals.DIMENSION__DIMENSION_CONTAINS_SCALE_SETS);

					if(scaleSetsSelectionPage.getSelectedScaleSetsPerDimensionPerViewpoint().get(viewpoint.getName()) != null
							&& scaleSetsSelectionPage.getSelectedScaleSetsPerDimensionPerViewpoint().get(viewpoint.getName()).get(dimension.getName()) != null) {
						for (ScaleSet scaleSet : scaleSetsSelectionPage.getSelectedScaleSetsPerDimensionPerViewpoint().get(viewpoint.getName()).get(dimension.getName())) {
							ScaleSet scaleSetCopy = EcoreUtil.copy(scaleSet);
							// Here we do not remove the references
							dimensionCopy.getDimensionContainsScaleSets().add(scaleSetCopy);
						}
					}
					viewpointCopy.getViewpointContainsDimensions().add(dimensionCopy);
				}
			}
			rootMSCharacterization.getContainsViewpoints().add(viewpointCopy);
		}
		return rootMSCharacterization;
	}

	@Override
	public boolean canFinish() {
		if (getContainer().getCurrentPage() == scaleSetsSelectionPage) {
			return true;
		} else {
			return false;
		}
	}

}
