/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package MSCharacterizationGenerator.services;

import java.util.ArrayList;
import java.util.List;

import mscharacterization.model.mscharacterization.BasicProbe;
import mscharacterization.model.mscharacterization.BasicProbeInformationParameter;
import mscharacterization.model.mscharacterization.Dimension;
import mscharacterization.model.mscharacterization.MSCharacterization;
import mscharacterization.model.mscharacterization.MavenArtifact;
import mscharacterization.model.mscharacterization.Measure;
import mscharacterization.model.mscharacterization.MeasureConverter;
import mscharacterization.model.mscharacterization.ScaleSet;
import mscharacterization.model.mscharacterization.Viewpoint;

public class UtilServices {

	public List<Measure> getMeasures(MSCharacterization aMSCharacterization) {
		List<Measure> result = new ArrayList<Measure>();
		for (Viewpoint aViewpoint : aMSCharacterization.getContainsViewpoints()) {
			for (ScaleSet scaleSet : getViewpointContainsScaleSets(aViewpoint)) {
				if (!result.contains(scaleSet.getScaleSetHasMeasure())) {
					result.add(scaleSet.getScaleSetHasMeasure());
				}
			}
			for (Dimension dimension : aViewpoint.getViewpointContainsDimensions()) {
				BasicProbe probe = dimension.getDimensionHasBasicProbe();
				if (probe != null && probe.getBasicProbeHasMeasure() != null && !result.contains(probe.getBasicProbeHasMeasure())) {
					result.add(probe.getBasicProbeHasMeasure());
				}
			}
		}
		
		return result;
		
	}

	public List<Measure> getMeasuresFromViewpoint(Viewpoint aViewpoint) {
		List<Measure> result = new ArrayList<Measure>();
		for (ScaleSet scaleSet : getViewpointContainsScaleSets(aViewpoint)) {
			if (!result.contains(scaleSet.getScaleSetHasMeasure())) {
				result.add(scaleSet.getScaleSetHasMeasure());
			}
		}
		
		return result;
		
	}
	

	public List<BasicProbe> getBasicProbes(MSCharacterization aMSCharacterization) {
		List<BasicProbe> result = new ArrayList<BasicProbe>();
		for (Viewpoint aViewpoint : aMSCharacterization.getContainsViewpoints()) {
			for (Dimension dimension : aViewpoint.getViewpointContainsDimensions()) {
				BasicProbe probe = dimension.getDimensionHasBasicProbe();
				if (probe !=null && !result.contains(probe)) {
					result.add(probe);
				}
			}
		}
		
		return result;
		
	}
	
	public List<MavenArtifact> getMavenArtifacts(MSCharacterization aMSCharacterization) {
		List <MavenArtifact> result = new ArrayList<MavenArtifact>();
		
		for (BasicProbe aBasicProbe : getBasicProbes(aMSCharacterization)) {
			if (!result.contains(aBasicProbe.getMavenArtifact())) {
				result.add(aBasicProbe.getMavenArtifact());
			}
		}
		
		return result;
	}
	
	public List<MeasureConverter> getMeasureConverters(MSCharacterization aMSCharacterization) {
		List <MeasureConverter> result = new ArrayList<MeasureConverter>();
		List<Measure> measures = getMeasures(aMSCharacterization);
		
		for (Measure measure : measures) {
			for (MeasureConverter converter : measure.getFromMeasureConverters()) {
				if (measures.contains(converter.getToMeasure()) && !result.contains(converter)) {
					result.add(converter);
				}
			}
			for (MeasureConverter converter : measure.getToMeasureConverters()) {
				if (measures.contains(converter.getFromMeasure()) && !result.contains(converter)) {
					result.add(converter);
				}
			}
		}
		
		return result;
	}
	

	public List<BasicProbeInformationParameter> getBasicProbeInformationParametersFromViewpoint(Viewpoint aViewpoint) {
		List<BasicProbeInformationParameter> result = new ArrayList<BasicProbeInformationParameter>();
		for (Dimension dimension : aViewpoint.getViewpointContainsDimensions()) {
			BasicProbe probe = dimension.getDimensionHasBasicProbe();
			if (probe != null && probe.getParameter() != null && !result.contains(probe.getParameter())) {
				result.add(probe.getParameter());
			}
		}

		return result;
		
	}
	
	public String getMeasureValueType(Measure aMeasure) {
		if (aMeasure.getValueType() != null) {
			return aMeasure.getValueType().getName();
		} else {
			return null;
		}
	}
	
	public String getBasicProbeParameterType(BasicProbeInformationParameter aParam) {
		if (aParam.getParameterType() != null) {
			return aParam.getParameterType().getName();
		} else {
			return null;
		}
	}
	
	public List<ScaleSet> getViewpointContainsScaleSets(Viewpoint aViewpoint) {
		List<ScaleSet> result = new ArrayList<ScaleSet>();
		
		for (Dimension aDimension : aViewpoint.getViewpointContainsDimensions()) {
			for (ScaleSet aScaleSet : aDimension.getDimensionContainsScaleSets()) {
				result.add(aScaleSet);
			}
		}
		
		return result;
	}
	
	public List<String> getProbeInformationEnum(Viewpoint aViewpoint) {
		List<String> result = new ArrayList<String>();
		
		for (ScaleSet aScaleSet : getViewpointContainsScaleSets(aViewpoint)) {
			result.add(getProbeInformationName(aViewpoint, aScaleSet));
		}
		
		return result;
	}

	public String getProbeInformationName(Viewpoint aViewpoint, ScaleSet aScaleSet) {
		return aViewpoint.getName().toUpperCase() + "_" + aScaleSet.getScaleSetIsContainedInDimension().getName().toUpperCase() + "_IN_" + aScaleSet.getName().toUpperCase() + "_PROBE";
	}
}
