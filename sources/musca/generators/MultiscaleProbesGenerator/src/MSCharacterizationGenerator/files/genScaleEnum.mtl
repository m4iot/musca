[comment encoding = UTF-8 /]
[comment]
/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
[/comment]
[module genScaleEnum('http://www-public.it-sudparis.eu/~rottenbe/mscharacterization/3.0')]

[import MSCharacterizationGenerator::util::utilTemplatesAndQueries /]

[template public genScaleEnum(aScaleSet : ScaleSet)]
[file (queryScaleEnumFileName(aScaleSet), false, 'UTF-8')]
/**
 * This file is part of the MuSCa framework.
 *
 * Copyright (c) 2011-2015 Institut Mines-Télécom / Télécom SudParis.
 * 
 * The MuSCa framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The MuSCa framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MuSCa framework.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Initial developer(s): Sam Rottenberg
 * Contributor(s):
 */
package msawareness.viewpoints.[aScaleSet.scaleSetIsContainedInDimension.dimensionIsContainedInViewpoint.name.toLowerCase()/];

import msawareness.measures.*;

public enum [queryScaleEnumName(aScaleSet)/] {
[for (scale : Scale | aScaleSet.scaleSetContainsScales) separator(',\n')]
	[scale.name.toUpperCase()/]("[scale.min/]", "[scale.max/]")
[/for];

	private final [queryMeasureValueName(aScaleSet.scaleSetHasMeasure)/] min;
	private final [queryMeasureValueName(aScaleSet.scaleSetHasMeasure)/] max;

	[queryScaleEnumName(aScaleSet)/](String min, String max) {
		this.min = new [queryMeasureValueName(aScaleSet.scaleSetHasMeasure)/](min);
		this.max = new [queryMeasureValueName(aScaleSet.scaleSetHasMeasure)/](max);
	}

	public [queryMeasureValueName(aScaleSet.scaleSetHasMeasure)/] getMin() {
		return this.min;
	}

	public [queryMeasureValueName(aScaleSet.scaleSetHasMeasure)/] getMax() {
		return this.max;
	}

	
	public static [queryScaleEnumName(aScaleSet)/] getScaleFromMeasureValue([queryMeasureValueName(aScaleSet.scaleSetHasMeasure)/] value) {
		for ([queryScaleEnumName(aScaleSet)/] scale : [queryScaleEnumName(aScaleSet)/].values()) {
			if (scale.isInScale(value)) {
				return scale;
			}
		}
		return null;
	}
	
	private boolean isInScale([queryMeasureValueName(aScaleSet.scaleSetHasMeasure)/] value) {
		return [aScaleSet.scaleSetHasInScaleRule.isInScaleCondition_VALUE_SCALE.replaceAll('VALUE', 'value').replaceAll('SCALE', 'this')/];
	}

}
[/file]
[/template]
