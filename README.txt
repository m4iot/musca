MuSCa stands for MultiScale Characterization framework.
It aims at providing models and tools to help building multiscale distributed systems based on a multiscale characterization of these systems.

This project was created in the context of a PhD funded by Télécom SudParis / Institut Mines-Télécom, Evry, France. It is a research prototype.

Main publication:
Sam Rottenberg, Sébastien Leriche, Chantal Taconet, Claire Lecocq and Thierry Desprats. MuSCa : A Multiscale Characterization Framework for Complex Distributed Systems. In Federated Conference on Computer Science and Information Systems (FedCSIS 2014), pages 1687–1695, Warsaw, Poland, September 2014.

---------------
* Administration instructions

  In order to deploy the maven modules to the maven repository
  and to publish the www documents, the following environment variables
  are needed :
  MUSCAUSER : fusionforge user
  MUSCA_DEPLOY_DIR : current directory of this README.txt file (used in root pom.xml)
