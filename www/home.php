<h1 align="center">MuSCa : MultiScale Characterization framework</h1>

<p class="indent">
The MuSCa (<b>Mu</b>lti<b>S</b>cale <b>C</b>h<b>a</b>racterization) framework includes
a characterization process based on the concepts of viewpoints,
dimensions and scales. These concepts constitute the core of a
dedicated metamodel. The proposed framework allows multiscale
software designers to share a taxonomy for qualifying their own
system. At system design time, the result of such a qualification
is a model from which the framework produces scale-awareness
artifacts, such as multiscale probes. These probes provide multiscale
components with an embedded scale-awareness ability.
</p>

<h3>Contributors</h3>
<p>
</p>
<b>Main Developer:</b>
<ul>
	<li>Sam Rottenberg, Institut Mines-T&eacute;l&eacute;com / T&eacute;l&eacute;com SudParis, UMRS CNRS Samovar</li>
</ul>
<p>
</p>
<b>Other Team Members:</b>
<ul>
	<li>Chantal Taconet (<a href="http://www-public.tem-tsp.eu/~taconet/">www</a>), Institut Mines-T&eacute;l&eacute;com / T&eacute;l&eacute;com SudParis, UMRS CNRS Samovar</li>
   <li>S&eacute;bastien Leriche, Toulouse University, ENAC</li>
	<li>Claire Lecocq (<a href="http://lecocq.wp.tem-tsp.eu/">www</a>), Institut Mines-T&eacute;l&eacute;com / T&eacute;l&eacute;com SudParis, UMRS CNRS Samovar</li>		
</ul>
<h3>Main Publications</h3>
<p>
</p>
<ul>
   <li>
   Sam Rottenberg, Sébastien Leriche, Chantal Taconet, Claire Lecocq and Thierry Desprats.
   <br>
   <i>MuSCa : A Multiscale Characterization Framework for Complex Distributed Systems.</i>
   <br>In Federated Conference on Computer Science and Information Systems (FedCSIS 2014), pages 1687–1695, Warsaw, Poland, September 2014.
	<br>	
	(<a href="https://fedcsis.org/proceedings/2014/pliks/131.pdf">link</a>)	
	</li>
   <li> Sam Rottenberg.
     <br>
     <i> Modèles, méthodes et outils pour les systèmes
répartis multiéchelles</i>
     <br>Thèse de doctorat de Télécom SudParis, préparée dans le cadre de l’école
     doctorale S&I, en accréditation conjointe avec l’Université d’Évry-Val-d’Essonne, avril 2015
     <br>(<a href="./these_rottenberg.pdf">link</a>)
  </li>
  <li>
   Sam Rottenberg, Sébastien Leriche, Chantal Taconet, Claire Lecocq and Thierry Desprats.
   <br>
   <i>Study of some Multiscale Viewpoints, Dimensions
and Scales.</i>
   <br>	
	(<a href="./MultiscaleViewpoints.pdf">link</a>)	
																       
  </li>															       
</ul>
<p></p>

<h3>License</h3>
<p>MuSCa is licensed under the
<a href="http://www.gnu.org/copyleft/lesser.html">GNU Lesser General Public License</a>.
</p>
<p>
</p>


<h3>Installation of the MuSCa framework</h3>

<p>
The MuSCa framework provides two plugins for the <a href="https://www.eclipse.org/">Eclipse IDE</a>.
For the moment, these plugins are only supposed to be compatible with the <a href="https://www.eclipse.org/kepler/">Eclipse Kepler 4.3</a> release.
</p>
<ul>
  <li>The following update can be used to install the MuSCa plugins:
    <pre><span class="inner-pre" style="font-size: 73%">
https://fusionforge.int-evry.fr/www/musca/updatesite/
    </span></pre>
  </li>
  <li>WARNING: In order to be able to access the update site, Eclipse needs to be launched with the following command:
    <pre><span class="inner-pre" style="font-size: 73%">
$ eclipse -vmargs -Djsse.enableSNIExtension=false
    </span></pre>
  </li>
</ul>

<p></p>

<h3>Developer's corner</h3>
<p>
MuSCa uses <a href="http://maven.apache.org/"><tt>Maven</tt></a> for
software project management.
</p>
<p>We suggest using also <tt>Maven</tt> for software management of
projects using MuSCa. Then, we
provide a <a href="maven-repository/release/musca/">maven repository</a> for
accessing MuSCa artefacts.
</p>
<p></p>

<ul>
	<li>
		Add Maven repository of MuSCa (in a POM file)
		<pre>
<span class="inner-pre" style="font-size: 73%">
&#60;repositories&#62;
	&#60;repository&#62;
		&#60;id&#62;musca-release&#60;/id&#62;
		&#60;name&#62;MuSCa's release repository&#60;/name&#62;
		&#60;url&#62;https://fusionforge.int-evry.fr/www/musca/maven-repository/release/&#60;/url&#62;
	&#60;/repository&#62;
&#60;/repositories&#62;
</span>  
		</pre>
	</li>		
</ul>	


<p>
</p>
<ul>
	<li>
		Public access (anonymous access) to the <a href="http://git-scm.com/">Git</a> repository of the source code:	
		<pre><span class="inner-pre" style="font-size: 73%">
$ git clone http://fusionforge.int-evry.fr/anonscm/git/musca/musca.git
		</span></pre>	
	</li>
</ul>


<p></p>

<!--h3>Changes</h3>
<p>
Current stable release of MuSCa is tagged <tt>0.14.0</tt>. You can
find the list of changes <a href="index.php?page=release_notes">here</a>.
</p>
<p></p>

<br>
<br>
<br-->

<p>
Last update: 28/02/2018.
</p>
