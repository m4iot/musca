<?php 
$pageOK = array(
   'home' => 'home.php',
   'release_notes' => 'release_notes.php'
);

// test whether the URL parameter exists and it is allowed
if ((isset($_GET['page'])) && (isset($pageOK[$_GET['page']]))){
	// the central content of the page
	include($pageOK[$_GET['page']]);
} 
else {
	// default page when the specified page does not exist
	include('home.php');          
}
?>