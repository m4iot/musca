<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
   <head>
      <meta http-equiv="content-type" content="text/html; charset=utf-8">
      <meta http-equiv="Content-Style-Type" content="text/css">
      <meta name="author" content="Sam Rottenberg" >
      <meta name="Description" content="MultiScale Characterization framework">
      <meta name="Identifier-URL" content="https://fusionforge.int-evry.fr/www/musca/index.php">
      <meta name="Keywords" content="INCOME, ANR project, multiscale, model-driven engineering,
      projet ANR, multi-&eacute;chelle, ing&eacute;nierie dirig&eacute;e par les mod&egrave;les">
      <meta name="Category" content="Research">
      <meta name="Robots" content="index, follows">
      <meta name="Copyright" content="Télécom SudParis / Institut Mines-Télécom">
      <link rel="stylesheet" type="text/css" href="css/style.css">
      <link rel="icon" href="figures/logo_tsp.jpg" type="image/jpg">
      <title>MuSCa : MultiScale Characterization framework</title>
</head>

<body>
   <div id="page" >
      <table class="center" cellpadding="0" cellspacing="0"><tr><td class="container_bg_top"></td></tr></table>
      <table class="justifiedtext" cellpadding="0" cellspacing="0">
         <tr>
            <td class="container_bg">
               <div class="center" >
                  <div id="container">
                     <div id="topbar">
                        <table class="logos" border=0 cellspacing=0 cellpadding=0>
                           <tr>
                              <td colspan=2><a href="index.php"><img src="figures/blue-home-icon.png" alt="Home" height=40 style="border:0;"></a></td>
                              <td style="width: 265px">&nbsp;</td>									
                              <td colspan=2><a href="http://www.mines-telecom.fr"> <img src="figures/logo-mines-telecom.png" alt="Institut Mines-Telecom" height=57 width=57 style="border:0;"></a></td>
                              <td style="width: 20px">&nbsp;</td>
                              <td colspan=2><a href="http://www.it-sudparis.eu"> <img src="figures/logo_tsp.jpg" alt="INT" width=57 style="border:0;"></a></td>
                              <td style="width: 18px">&nbsp;</td>
                              <td colspan=2><a href="http://samovar.telecom-sudparis.eu/"> <img src="figures/samovar05.jpg" alt="SAMOVAR" style="border:0;"></a></td>
                              <td style="width: 18px">&nbsp;</td>
                              <td colspan=2><a href="http://www.enac.fr/"> <img src="figures/logo_enac.png" alt="ENAC" height=57 style="border:0;"></a></td>
                           </tr>
                        </table>
                     </div>
                     <div id="navbar">
                        <table class="center" border=0 cellspacing=0 cellpadding=0>
                           <tr>
                              <td>
                                 <div class="ligne" ></div>
                              </td>
                           </tr>
                        </table>
                     </div>
                     <div id="main">
                        <div class="justifiedtext" id="one_column">
                           <?php
                            function currentURL() {
                              $pageURL = 'http';
                              if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
                                 $pageURL .= "://";
                              if ($_SERVER["SERVER_PORT"] != "80") {
                                 $pageURL .= $_SERVER["SERVER_NAME"].
                                        ":".
                                        $_SERVER["SERVER_PORT"].
                                        $_SERVER["REQUEST_URI"];
                              } else {
                                 $pageURL .= $_SERVER["SERVER_NAME"].
                                        $_SERVER["REQUEST_URI"];
                              }
                              return $pageURL;
                            }
                           ?>
                           <?php include ('frame.php'); ?>
                        </div>
                        <div class="spacer"></div>
                     </div>
                  </div>
               </div>
            </td>
         </tr>
      </table>
      <table cellpadding="0" cellspacing="0"><tr><td class="container_bg_footer"></td></tr></table>
      <table class="center" border=0 cellspacing=0 cellpadding=0>
         <tr>
            <td colspan=2><?php echo '<a href="http://validator.w3.org/check?uri='.currentURL().'">'; ?><img style="border:0;width:44px;height:15px"src="figures/valid-html401.png" alt="Valid HTML 4.01 Transitional" ></a></td>
            <td style="width: 10px">&nbsp;</td>
            <td colspan=2><?php echo '<a href="http://jigsaw.w3.org/css-validator/validator?uri='.currentURL().'">'; ?><img style="border:0;width:44px;height:15px" src="figures/vcss.gif" alt="Valid CSS" ></a></td>
         </tr>
      </table>
   </div>
</body>
</html>
